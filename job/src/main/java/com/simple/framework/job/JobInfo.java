package com.simple.framework.job;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JobInfo {

    private int group;
    private String desc;
    private String cron;
    private String jobName;
    private String params;
}
