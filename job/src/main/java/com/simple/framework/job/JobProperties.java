package com.simple.framework.job;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import lombok.Data;

@Data
public class JobProperties {
    @Inject
    @Named("job.admin.addresses")
    private String adminAddresses;
    @Inject
    @Named("job.executor.appname")
    private String appName;

    @Inject
    @Named("job.accessToken")
    private String accessToken;

    @Inject
    @Named("job.executor.ip")
    private String ip;

    @Inject
    @Named("job.executor.port")
    private int port;

    @Inject
    @Named("job.executor.logRetentionDays")
    private int logRetentionDays;
}
