package com.simple.framework.job;

import cn.hutool.core.collection.CollUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.beans.Transmit;
import com.simple.framework.data.entity.CorporationPower;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.entity.Power;
import com.simple.framework.data.mapper.CorporationPowerMapper;
import com.simple.framework.data.mapper.PowerMapper;
import com.simple.framework.data.service.DataService;
import com.simple.framework.data.service.NodeContext;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
public class BaseMetaJob extends IJobHandler {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    PowerMapper powerMapper;
    @Autowired
    CorporationPowerMapper corporationPowerMapper;
    @Autowired
    public DataService dataService;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Params {
        private String corporationId;
        private List<String> powerList;
    }

    @Override
    @XxlJob("基础元数据下载")
    public void execute() throws Exception {
        String params = XxlJobHelper.getJobParam();
        Params paramsObj = objectMapper.readValue(params, Params.class);
        List<Power> query = new ArrayList<>();
        for (String item : paramsObj.getPowerList()) {
            String powerCode = item.split(":")[1];
            Power power = dataService.queryDataPower(powerCode);
            if (Objects.nonNull(power)) {
                query.add(power);
            }
        }
        if (CollUtil.isNotEmpty(query)) {
            Map<String, Power> powerMap = query.stream().collect(Collectors.toMap(Power::getId, p -> p));
            List<CorporationPower> corporationPowerList = dataService.queryCorporationPower(paramsObj.getCorporationId(), powerMap.keySet());
            Transmit.supplyAsync(corporationPowerList, corporationPower -> {
                MdcUtil.set();
                Power power = powerMap.get(corporationPower.getPowerId());
                String msg = null;
                try {
                    NodeContext context = dataService.context(corporationPower.getId(), false, null, objectMapper.createObjectNode(), Message.Source.JOB);
                    dataService.clear(context);
                } catch (Exception e) {
                    msg = e.getMessage();
                }
                dataService.saveJobLog(MdcUtil.get(), "基础元数据下载", power.getName(), power.getCode(), corporationPower.getId(), msg);
                MdcUtil.clear();
            });
        }
    }
}
