package com.simple.framework.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.beans.Transmit;
import com.simple.framework.data.entity.CorporationPower;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.entity.Meta;
import com.simple.framework.data.mapper.CorporationPowerMapper;
import com.simple.framework.data.mapper.MetaMapper;
import com.simple.framework.data.mapper.PowerMapper;
import com.simple.framework.data.service.DataService;
import com.simple.framework.data.service.NodeContext;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
public class BaseMetaSyncJob extends IJobHandler {
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    PowerMapper powerMapper;
    @Autowired
    MetaMapper meteMapper;
    @Autowired
    CorporationPowerMapper corporationPowerMapper;
    @Autowired
    public DataService dataService;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Params {
        private String corporationId;
        private int limit;
        private Map<String, String> metaMap;
        private String condition;
    }

    @Override
    @XxlJob("基础元数据同步")
    public void execute() throws Exception {
        String params = XxlJobHelper.getJobParam();
        Params paramsObj = objectMapper.readValue(params, Params.class);
        for (Map.Entry<String, String> entry : paramsObj.getMetaMap().entrySet()) {
            String metaName = entry.getKey();
            String powerName = entry.getValue().split(":")[0];
            String powerCode = entry.getValue().split(":")[1];
            CorporationPower corporationPower = dataService.queryPower(paramsObj.getCorporationId(), powerCode);
            if (Objects.nonNull(corporationPower)) {
                List<Meta> metas = dataService.selectTodoMetaList(paramsObj.getCorporationId(), metaName, paramsObj.getCondition(), paramsObj.getLimit());
                Transmit.supplyAsync(metas, meta -> {
                    MdcUtil.set();
                    String msg = null;
                    ObjectNode node = objectMapper.createObjectNode();
                    ObjectNode context = (ObjectNode) meta.getExt().getSource();
                    node.set("_webhookBody", context);
                    try {
                        NodeContext nodeContext = dataService.context(corporationPower.getId(), false, null, node, Message.Source.JOB);
                        meta.setStatus(Meta.Status.DONE);
                        meteMapper.update(meta);
                        dataService.clear(nodeContext);
                    } catch (Exception e) {
                        meta.setStatus(Meta.Status.FAIL);
                        meteMapper.update(meta);
                        msg = e.getMessage();
                    }
                    dataService.saveJobLog(MdcUtil.get(), "基础元数据同步:" + metaName, powerName, powerCode, corporationPower.getId(), msg);
                    MdcUtil.clear();
                });
            }
        }

    }
}
