package com.simple.framework.job;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.CharSequenceUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mybatisflex.core.query.QueryWrapper;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.data.entity.CorporationPower;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.entity.Power;
import com.simple.framework.data.entity.table.Tables;
import com.simple.framework.data.mapper.CorporationPowerMapper;
import com.simple.framework.data.mapper.MessageMapper;
import com.simple.framework.data.mapper.PowerMapper;
import com.simple.framework.data.service.DataService;
import com.simple.framework.data.service.NodeContext;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
public class MessageJob extends IJobHandler {
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    MessageMapper messageMapper;
    @Autowired
    CorporationPowerMapper corporationPowerMapper;
    @Autowired
    PowerMapper powerMapper;
    @Autowired
    public DataService dataService;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Params {
        private String corporationId;
        private int limit;
        private Map<String, String> powerMap;
        private String condition;
        private Map<String, Boolean> order;
    }

    @Override
    @XxlJob("消息推送任务")
    public void execute() throws Exception {
        String params = XxlJobHelper.getJobParam();
        MessageJob.Params paramsObj = objectMapper.readValue(params, MessageJob.Params.class);

        Map<String, String> corporationPowerMap = new HashMap<>();
        Map<String, String> corporationPower2PownerMap = new HashMap<>();
        for (Map.Entry<String, String> entry : paramsObj.getPowerMap().entrySet()) {
            String powerName = entry.getKey().split(":")[0];
            String powerCode = entry.getKey().split(":")[1];
            Power power = powerMapper.selectOneByQuery(new QueryWrapper().where(Tables.POWER.NAME.in(powerName))
                    .and(Tables.POWER.CODE.eq(powerCode)));
            if (Objects.nonNull(power)) {
                CorporationPower corporationPower = corporationPowerMapper.selectOneByQuery(new QueryWrapper().where(Tables.CORPORATION_POWER.CORPORATION_ID.eq(paramsObj.getCorporationId()))
                        .and(Tables.CORPORATION_POWER.POWER_ID.eq(power.getId())));
                if (Objects.nonNull(corporationPower)) {
                    corporationPower2PownerMap.put(corporationPower.getId(), powerCode);
                    powerName = entry.getValue().split(":")[0];
                    powerCode = entry.getValue().split(":")[1];
                    power = powerMapper.selectOneByQuery(new QueryWrapper().where(Tables.POWER.NAME.in(powerName))
                            .and(Tables.POWER.CODE.eq(powerCode)));
                    if (Objects.nonNull(power)) {
                        CorporationPower tCorporationPower = corporationPowerMapper.selectOneByQuery(new QueryWrapper().where(Tables.CORPORATION_POWER.CORPORATION_ID.eq(paramsObj.getCorporationId()))
                                .and(Tables.CORPORATION_POWER.POWER_ID.eq(power.getId())));
                        corporationPowerMap.put(corporationPower.getId(), tCorporationPower.getId());
                    }
                }
            }
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.MESSAGE.CORPORATION_ID.eq(paramsObj.getCorporationId()))
                .and(Tables.MESSAGE.CORPORATION_POWER_ID.in(corporationPowerMap.keySet()));
        if (CharSequenceUtil.isNotEmpty(paramsObj.getCondition())) {
            queryWrapper.and(paramsObj.getCondition());
        } else {
            queryWrapper.and(Tables.MESSAGE.STATUS.in(Message.Status.PROCESSOR, Message.Status.FAIL));
        }
        if (CollUtil.isNotEmpty(paramsObj.getOrder())) {
            paramsObj.getOrder().forEach(queryWrapper::orderBy);
        } else {
            queryWrapper.orderBy(Tables.MESSAGE.UPDATE_TIME, true);
        }
        if (paramsObj.getLimit() > 0) {
            queryWrapper.limit(paramsObj.getLimit());
        }
        List<Message> messages = messageMapper.selectListByQuery(queryWrapper);

        for (Message message : messages) {
            if (corporationPowerMap.containsKey(message.getCorporationPowerId())) {
                MdcUtil.set();
                String corporationPowerId = corporationPowerMap.get(message.getCorporationPowerId());
                String msg = null;
                try {
                    ObjectNode node = (ObjectNode) message.getExt().getData();
                    node.put("_messageAction", message.getAction().getName());
                    node.put("_messagePowerCode", corporationPower2PownerMap.get(message.getCorporationPowerId()));
                    NodeContext context = dataService.context(corporationPowerId, message.getSource() == Message.Source.CALLBACK, message.getPlatform(), node, Message.Source.JOB);
                    message.setStatus(Message.Status.DONE);
                    messageMapper.update(message);
                    dataService.clear(context);
                } catch (Exception e) {
                    message.setStatus(Message.Status.FAIL);
                    messageMapper.update(message);
                    msg = e.getMessage();
                }
                dataService.saveJobLog(MdcUtil.get(), "消息推送任务", corporationPowerId, message.getId(), msg);
                MdcUtil.clear();
            }
        }
    }
}
