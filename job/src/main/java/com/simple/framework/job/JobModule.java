package com.simple.framework.job;

import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.SimpleModule;
import lombok.extern.slf4j.Slf4j;

@StarterModule(value = "Job模块-注册xxl-job执行器", order = Constants.FRAMEWORK_MODULE_ORDER_JOB)
@Slf4j
public class JobModule extends SimpleModule {


    @Override
    protected void configure() {
        bind(JobProperties.class).toInstance(new JobProperties());
    }


}
