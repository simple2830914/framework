package com.simple.framework.job;

import cn.hutool.core.collection.CollUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.data.entity.CorporationPower;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.entity.Meta;
import com.simple.framework.data.mapper.CorporationPowerMapper;
import com.simple.framework.data.mapper.MetaMapper;
import com.simple.framework.data.mapper.PowerMapper;
import com.simple.framework.data.service.DataService;
import com.simple.framework.data.service.NodeContext;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
public class LoadMetaSyncJob extends IJobHandler {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    PowerMapper powerMapper;
    @Autowired
    MetaMapper meteMapper;
    @Autowired
    CorporationPowerMapper corporationPowerMapper;
    @Autowired
    public DataService dataService;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Params {
        private String corporationId;
        private Map<String, String> metaMap;
        private String condition;
    }

    @Override
    @XxlJob("基础元数据全量同步")
    public void execute() throws Exception {
        String params = XxlJobHelper.getJobParam();
        LoadMetaSyncJob.Params paramsObj = objectMapper.readValue(params, LoadMetaSyncJob.Params.class);
        for (Map.Entry<String, String> entry : paramsObj.getMetaMap().entrySet()) {
            String metaName = entry.getKey();
            String powerName = entry.getValue().split(":")[0];
            String powerCode = entry.getValue().split(":")[1];
            CorporationPower corporationPower = dataService.queryPower(paramsObj.getCorporationId(), powerCode);
            if (Objects.nonNull(corporationPower)) {
                List<Meta> metas = dataService.selectMetaListByCondition(paramsObj.getCorporationId(), metaName, paramsObj.getCondition());
                if (CollUtil.isNotEmpty(metas)) {
                    ArrayNode arrayNode = objectMapper.createArrayNode();
                    metas.forEach(s -> arrayNode.add(objectMapper.convertValue(s.getExt(), JsonNode.class)));
                    MdcUtil.set();
                    String msg = null;
                    ObjectNode node = objectMapper.createObjectNode();
                    node.set("_webhookBody", arrayNode);
                    try {
                        NodeContext context = dataService.context(corporationPower.getId(), false, null, node, Message.Source.JOB);
                        dataService.clear(context);
                    } catch (Exception e) {
                        msg = e.getMessage();
                    }
                    dataService.saveJobLog(MdcUtil.get(), "基础元数据全量同步:" + metaName, powerName, powerCode, corporationPower.getId(), msg);
                    MdcUtil.clear();
                }

            }
        }
    }
}
