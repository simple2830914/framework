package com.simple.framework.job;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.CharSequenceUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybatisflex.core.query.QueryWrapper;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.beans.Transmit;
import com.simple.framework.data.entity.CommonLog;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.entity.Meta;
import com.simple.framework.data.entity.MetaConfig;
import com.simple.framework.data.entity.table.Tables;
import com.simple.framework.data.mapper.MetaMapper;
import com.simple.framework.data.service.DataService;
import com.simple.framework.data.service.NodeContext;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
public class BusinessMetaJob extends IJobHandler {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MetaMapper metaMapper;


    @Autowired
    public DataService dataService;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Params {
        private String condition;
        private long offsetMinute;
        private String corporationId;
        private List<Meta.Status> status;
    }

    @Override
    @XxlJob("业务元数据异常补偿")
    public void execute() throws Exception {
        String params = XxlJobHelper.getJobParam();
        Params paramsObj = objectMapper.readValue(params, Params.class);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.META.CORPORATION_ID.eq(paramsObj.getCorporationId()))
                .and(Tables.META.TYPE.eq(MetaConfig.Type.BUSINESS.getName()))
                .and(Tables.META.STATUS.in(paramsObj.getStatus()))
                .and(Tables.META.UPDATE_TIME.gt(System.currentTimeMillis() - paramsObj.offsetMinute * 60 * 1000));
        if (CharSequenceUtil.isNotEmpty(paramsObj.getCondition())) {
            queryWrapper.and(paramsObj.getCondition());
        }

        List<Meta> query = metaMapper.selectListByQuery(queryWrapper);
        if (CollUtil.isNotEmpty(query)) {
            Transmit.supplyAsync(query, meta -> {
                MdcUtil.set();
                Map<String, Object> jobContext = new HashMap<>();
                CommonLog.Job job = null;
                JsonNode callback = meta.getExt().getCallback();
                if (Objects.nonNull(callback) && !callback.isNull() && callback.get("callback").asBoolean()) {
                    try {
                        jobContext.put("type", "callback");
                        String corporationPowerId = meta.getExt().getCallback().get("corporationPowerId").asText();
                        job = CommonLog.Job.builder()
                                .context(jobContext)
                                .jobName("业务元数据异常补偿")
                                .corporationPowerId(corporationPowerId)
                                .dataId(meta.getId())
                                .build();
                        dataService.context(corporationPowerId, true, meta.getExt()
                                .getCallback()
                                .get("platform")
                                .asText(), meta.getExt().getCallback().get("data"), Message.Source.JOB);
                    } catch (Exception e) {
                        jobContext.put("errorMsg", e.getMessage());
                    }
                } else {
                    if (Objects.nonNull(meta.getExt().getSource())) {
                        try {
                            jobContext.put("type", "webhook");
                            String corporationPowerId = meta.getExt().getSource().get("corporationPowerId").asText();
                            job = CommonLog.Job.builder()
                                    .context(jobContext)
                                    .jobName("业务元数据异常补偿")
                                    .corporationPowerId(corporationPowerId)
                                    .dataId(meta.getId())
                                    .build();
                            NodeContext context = dataService.context(corporationPowerId, false, null, meta.getExt()
                                    .getSource()
                                    .get("data"), Message.Source.JOB);
                            dataService.clear(context);
                        } catch (Exception e) {
                            jobContext.put("errorMsg", e.getMessage());
                        }
                    }
                }
                if (Objects.nonNull(job)) {
                    dataService.saveJobLog(MdcUtil.get(), job);
                }
                MdcUtil.clear();
            });
        }
    }
}
