package com.simple.framework.template;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.PathUtil;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jfinal.template.source.FileSourceFactory;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.template.sql.SqlTemplate;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@StarterModule(value = "SQL模板渲染模块", order = Constants.FRAMEWORK_MODULE_ORDER_TEMPLATE)
@Slf4j
public class TemplateModule extends AbstractModule {

    @Inject
    @Named(value = "template.basePath")
    String basePath;

    @Provides
    @Singleton
    public SqlTemplate sqlTemplate() {
        SqlTemplate sqlTemplate = new SqlTemplate("simple");
        sqlTemplate.getEngine().setSourceFactory(new FileSourceFactory());
        sqlTemplate.setBaseSqlTemplatePath(basePath);
        List<String> enjoyList = PathUtil.loopFiles(Paths.get(basePath), pathname -> FileUtil.getSuffix(pathname)
                        .equals("tlp"))
                .stream()
                .map(s -> Paths.get(basePath).relativize(s.toPath()).toString())
                .collect(Collectors.toList());
        enjoyList.forEach(s -> log.info("add template {}", s));
        enjoyList.forEach(sqlTemplate::addSqlTemplate);
        sqlTemplate.parseSqlTemplate();
        return sqlTemplate;
    }
}
