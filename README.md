### 核心设计

#### 基础模型设计

企业功能执行编排模型[Node.java](data%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fdata%2Fentity%2FNode.java)

全局字段模型[Field.java](data%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fdata%2Fentity%2FField.java)

多层渲染原型[Render.java](data%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fdata%2Fentity%2FRender.java)

业务规则检查模型[Rule.java](data%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fdata%2Fentity%2FRule.java)

业务数据原型[Meta.java](data%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fdata%2Fentity%2FMeta.java)

业务数据配置[MetaConfig.java](data%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fdata%2Fentity%2FMetaConfig.java)

消息记录模型[Message.java](data%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fdata%2Fentity%2FMessage.java)

#### 编排执行的入口

1. [DataWebCallbackResource.java](web%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fweb%2Fcontroller%2FDataWebCallbackResource.java) 提供的http://localhost:8899/webHook/{corporationPowerId}
2. [DataWebHookResource.java](web%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fweb%2Fcontroller%2FDataWebHookResource.java) 提供的http://localhost:8899/callback/{platform}/{corporationPowerId}
3. ##execute('{{$.corporationPowerId}}','key','{{$.data}}') 编排中可以通过执行改表达式进行多个编排串联
4. Job定时任务
   [BaseMetaJob.java](job%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fjob%2FBaseMetaJob.java)
   [BaseMetaSyncJob.java](job%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fjob%2FBaseMetaSyncJob.java)
   [BusinessMetaJob.java](job%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fjob%2FBusinessMetaJob.java)
   [LoadMetaSyncJob.java](job%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fjob%2FLoadMetaSyncJob.java)
   [MessageJob.java](job%2Fsrc%2Fmain%2Fjava%2Fcom%2Fsimple%2Fframework%2Fjob%2FMessageJob.java)
### 开发常见的坑

1. JavaApi 方法编写时应考虑传参Null的情况，##xxxx('{{xxx}}')表达试执行是通过java反射实现的，实参为空时无法正确找到对应Method,可以通过形参设置Object类型来解决
2. 程序配置更新时，由于部署的docker已持久化，应该通过Filebrowser来手工修改
3. 目前的node编排为了图省事支持了before和after，推荐只使用before，after可调整到下个Node中的before,这可以方便后续将order属性扩展成Node执行流
4. spring按需注入的问题，非com.simple下的对象，应该配置文件spring.guiceBeans下添加
5. 依赖注入时分清楚google包还jakarta包

```hocon
simple {
  spring {
    package: ["com.simple"]
    guiceBeans: [
      "com.simple",
      "com.typesafe",
      "com.google.inject.Injector",
      "com.fasterxml.jackson.databind.ObjectMapper",
      "org.mybatis.spring.mapper.MapperScannerConfigurer",
      "javax.script.ScriptEngine"
    ]
  }
}
```
#### 运维过程中常用命令
```shell
查看堆外内存
jcmd 7 VM.native_memory summary scale=MB
查看堆内内存
jmap -heap 7
查看gc情况
jstat -gcutil 7 1000
dump查看
jmap -dump:live,format=b,file=/tmp/dump.hprof 7
```
