#!/usr/bin/env bash
# shellcheck disable=SC2155
export version=$(cat version.txt)
export IMG_VERSION=$(echo "${version}" | awk -F '.' '{print $1"."$2"."$3}')