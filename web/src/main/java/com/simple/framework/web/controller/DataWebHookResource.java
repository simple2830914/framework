package com.simple.framework.web.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.simple.framework.annotation.ExecuteTime;
import com.simple.framework.beans.ResponseBean;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.service.DataService;
import com.simple.framework.data.service.NodeContext;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Path("/webhook")
@Slf4j
public class DataWebHookResource extends SimpleResource {

    @Inject
    DataService dataService;

    @POST
    @Path("/{corporationPowerId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ExecuteTime
    public ResponseBean autoPower(JsonNode body,
                                  @PathParam("corporationPowerId") String corporationPowerId,
                                  @Context UriInfo uriInfo,
                                  @Context HttpHeaders httpHeaders,
                                  @Context HttpServletResponse response) {
        try {
            NodeContext context = dataService.context(corporationPowerId, false, null, httpContext(body, uriInfo, httpHeaders), Message.Source.WEBHOOK);
            ResponseBean responseBean = dataService.response(context);
            if (responseBean.isForwarding()) {
                response.sendRedirect(responseBean.getAddress());
            }
            dataService.clear(context);
            return responseBean;
        } catch (Exception e) {
            return ResponseBean.error(e.getMessage());
        }
    }

}
