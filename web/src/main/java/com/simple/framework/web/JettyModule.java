package com.simple.framework.web;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.SimpleModule;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.WebAppContext;


@StarterModule(value = "Jetty容器模块", order = Constants.FRAMEWORK_MODULE_ORDER_JETTY)
@Slf4j
public class JettyModule extends SimpleModule {

    @Inject
    @Named("jetty.name")
    private String name;
    @Inject
    @Named("jetty.host")
    private String host;
    @Inject
    @Named("jetty.port")
    private int port;
    @Inject
    @Named("jetty.basePath")
    String basePath;

    @Override
    protected void configure() {
        bind(WebAppContext.class).toInstance(webAppContext());
    }

    public WebAppContext webAppContext() {
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setName(name);
        connector.setHost(host);
        connector.setPort(port);
        server.addConnector(connector);
        WebAppContext webAppContext = new WebAppContext();
        webAppContext.setServer(server);
        webAppContext.setResourceBase("/");
        webAppContext.setContextPath(basePath);
        return webAppContext;
    }
}
