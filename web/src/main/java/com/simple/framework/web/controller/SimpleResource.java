package com.simple.framework.web.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.UriInfo;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SimpleResource {

    @Inject
    ObjectMapper objectMapper;

    public JsonNode httpContext(JsonNode body, UriInfo uriInfo, HttpHeaders httpHeaders) {
        body = Optional.ofNullable(body).orElse(objectMapper.createObjectNode());
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        MultivaluedMap<String, String> headerParams = httpHeaders.getRequestHeaders();
        ObjectNode node = objectMapper.createObjectNode();
        node.set("_webhookGet", objectMapper.convertValue(queryParams, JsonNode.class));
        node.set("_webhookHeader", objectMapper.convertValue(headerParams, JsonNode.class));
        node.set("_webhookBody", body);
        return node;
    }
}
