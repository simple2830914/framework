package com.simple.framework.web;

import com.simple.framework.annotation.Starter;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.Application;
import com.simple.framework.boot.Order;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.HandlerWrapper;
import org.eclipse.jetty.webapp.WebAppContext;

import java.util.Arrays;


@Slf4j
@Starter(value = "内嵌Jetty启动器", async = false)
@Order(Constants.FRAMEWORK_STARTER_ORDER_JETTY)
public class JettyStarter extends Application {

    @Inject
    private WebAppContext webAppContext;

    private void setHandler(HandlerWrapper handlerWrapper, Handler handlerToAdd) {
        Handler currentInnerHandler = handlerWrapper.getHandler();
        if (currentInnerHandler == null) {
            handlerWrapper.setHandler(handlerToAdd);
        } else if (currentInnerHandler instanceof HandlerCollection) {
            ((HandlerCollection) currentInnerHandler).addHandler(handlerToAdd);
        } else if (currentInnerHandler instanceof HandlerWrapper) {
            setHandler((HandlerWrapper) currentInnerHandler, handlerToAdd);
        } else {
            HandlerList handlerList = new HandlerList();
            handlerList.addHandler(currentInnerHandler);
            handlerList.addHandler(handlerToAdd);
            handlerWrapper.setHandler(handlerWrapper);
        }

    }

    @Override
    public void doStart() throws Exception {
        log.info("Starting embedded jetty server:{} - basePath:{}", Arrays.toString(webAppContext.getServer()
                .getConnectors()), webAppContext.getContextPath());
        setHandler(webAppContext.getServer(), webAppContext);
        webAppContext.getServer().start();
    }

    @Override
    public void doStop() throws Exception {
        webAppContext.stop();
        log.info("Embedded jetty server stopped");
    }

}