package com.simple.framework.boot;

import com.google.inject.AbstractModule;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleModule extends AbstractModule {

    @Override
    protected void configure() {
        try {
            configureWithExcept();
        } catch (Exception e) {
            log.info("注入异常", e);
        }
    }

    public void configureWithExcept() {

    }
}
