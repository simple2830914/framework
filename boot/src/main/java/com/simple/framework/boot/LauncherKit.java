package com.simple.framework.boot;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ReflectUtil;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.simple.framework.annotation.Starter;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.MdcUtil;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigRenderOptions;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sun.misc.Unsafe;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Slf4j
public class LauncherKit {

    @Getter
    @Setter
    private static Injector injector;
    @Getter
    @Setter
    private static Injector rootInjector;
    @Getter
    private static Config globalConfig;
    @Getter
    private static ClassGraphReflector reflector;

    private static void printBanner() {
        log.info("\n{}", IoUtil.readUtf8(ResourceUtil.getStream("banner.txt")));
    }

    private static Config loadConfig() {
        String configPath = System.getProperty("config.file");
        Assert.notBlank(configPath, "未指定config.file路径");
        File configFile = new File(configPath);
        Assert.isTrue(configFile.exists(), "未知路径:{}", configFile);
        Config defaultConfig = ConfigFactory.load();
        globalConfig = ConfigFactory.parseFile(configFile).withFallback(defaultConfig).resolve().getConfig("simple");
        log.info("config:\n{}", globalConfig.root().render(ConfigRenderOptions.concise().setFormatted(true)));
        return globalConfig;
    }

    private static void scanClass() {
        List<String> packages = globalConfig.getStringList("scan.package");
        reflector = new ClassGraphReflector();
        reflector.scan(packages.toArray(new String[]{}));
    }

    public static void refresh(Injector injector) {
        LauncherKit.injector = injector;
    }

    private static Injector createInjector() {
        List<Class<?>> modules = reflector.getSubClassWithAnnotation(AbstractModule.class, StarterModule.class);
        injector = Guice.createInjector(new BaseModule());
        modules.sort(Comparator.comparingInt(s -> s.getAnnotation(StarterModule.class).order()));
        for (Class<?> s : modules) {
            try {
                AbstractModule module;
                log.info("Guice install:{}", s.getAnnotation(StarterModule.class).value());
                module = (AbstractModule) injector.getInstance(s);
                injector = injector.createChildInjector(module);
            } catch (Exception e) {
                log.error("guice注入异常 - {} ", s.getAnnotation(StarterModule.class).value(), e);
            }
        }
        return injector;
    }

    private static void runApplications() throws Exception {
        List<Class<?>> applicationClasses = reflector.getSubClassWithAnnotation(Application.class, Starter.class);
        List<Application> applications = new ArrayList<>();
        for (Class<?> clazz : applicationClasses) {
            Object object = injector.getInstance(clazz);
            if (Objects.nonNull(object)) {
                applications.add((Application) object);
            }
        }
        applications.sort(Comparator.comparing(s -> s.getClass().getAnnotation(Order.class).value()));
        for (Application application : applications) {
            log.info("Starter启动 - {} - {}", application.getClass(), application.getClass()
                    .getAnnotation(Starter.class)
                    .value());
            boolean async = application.getClass().getAnnotation(Starter.class).async();
            application.setAsync(async);
            application.start();
        }
        log.info("{} 启动完成", LauncherKit.class.getName());
    }

    @SneakyThrows
    public static void startUp() {
        disableWarning();
        printBanner();
        MdcUtil.set();
        loadConfig();
        scanClass();
        createInjector();
        runApplications();
    }

    public static void disableWarning() {
        try {
            System.setProperty("nashorn.args", "--no-deprecation-warning");
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            ReflectUtil.setAccessible(theUnsafe);
            Unsafe u = (Unsafe) theUnsafe.get(null);
            Class<?> cls = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field logger = cls.getDeclaredField("logger");
            u.putObjectVolatile(cls, u.staticFieldOffset(logger), null);
        } catch (Exception e) {
            // ignore
        }
    }

    public static void main(String[] args) {
        startUp();
    }

}
