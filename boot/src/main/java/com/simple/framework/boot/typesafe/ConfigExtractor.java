package com.simple.framework.boot.typesafe;

import com.typesafe.config.Config;


interface ConfigExtractor {

    Object extractValue(Config config, String path);

    Class<?>[] getMatchingClasses();
}
