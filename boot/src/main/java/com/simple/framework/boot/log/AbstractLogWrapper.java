package com.simple.framework.boot.log;

import lombok.Getter;
import org.slf4j.MDC;

import java.util.Map;

public class AbstractLogWrapper<T> {
    @Getter
    private final T job;
    private final Map<String, String> context;

    public AbstractLogWrapper(T t) {
        this.job = t;
        this.context = MDC.getCopyOfContextMap();
    }

    public void setLogContext() {
        if (this.context != null) {
            MDC.setContextMap(this.context);
        }
    }

    public void clearLogContext() {
        MDC.clear();
    }

}