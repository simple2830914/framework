package com.simple.framework.boot;

import cn.hutool.core.thread.ThreadUtil;
import com.google.inject.Inject;
import com.simple.framework.boot.log.LogRunnable;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Slf4j
public abstract class Application implements LifeCycle, App {

    private final AtomicReference<Status> status = new AtomicReference<>(Status.STOPPED);
    private final CopyOnWriteArrayList<Listener> listeners = new CopyOnWriteArrayList<>();
    @Setter
    private boolean async;
    @Inject
    private Worker worker;

    @Override
    public void doStart() throws Exception {

    }

    @Override
    public void doStop() throws Exception {

    }

    @Override
    public void registerShutdownHook() {
//        log.info("registerShutdownHook - {}", this.getClass().getName());
        Thread shutdownHook = new Thread(new LogRunnable(() -> {
            try {
                Application.this.stop();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }));
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }

    @Override
    public boolean setStarting() {
        if (status.compareAndSet(Status.STOPPED, Status.STARTING)) {
            for (Listener listener : listeners) {
                listener.lifeCycleStarting(this);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean setStarted() {
        if (status.compareAndSet(Status.STARTING, Status.STARTED)) {
            for (Listener listener : listeners) {
                listener.lifeCycleStarted(this);
            }
            return true;
        }
        return false;

    }


    @Override
    public boolean setStopping() {
        if (status.compareAndSet(Status.STARTING, Status.STOPPING) || status.compareAndSet(Status.STARTED, Status.STOPPING)) {
            for (Listener listener : listeners) {
                listener.lifeCycleStopping(this);
            }
            return true;
        }
        return false;
    }

    @Override
    public void setStopped() {
        status.set(Status.STOPPED);
        for (Listener listener : listeners) {
            listener.lifeCycleStopped(this);
        }
    }

    @Override
    public void setFailed(Throwable th) {
        status.lazySet(Status.FAILED);
        for (Listener listener : listeners) {
            listener.lifeCycleFailure(this, th);
        }
    }

    @Override
    public void start() throws Exception {
        try {
            if (isStopped() && setStarting()) {
                worker.register(this.getClass().getName(), this);
                if (async) {
                    ThreadUtil.execAsync(new LogRunnable(() -> {
                        try {
                            doStart();
                        } catch (Exception e) {
                            log.error("启动异常", e);
                            throw new RuntimeException(e);
                        }
                    }));
                } else {
                    try {
                        doStart();
                    } catch (Exception e) {
                        log.error("启动异常", e);
                        throw new RuntimeException(e);
                    }
                }
                while (true) {
                    if (setStarted()) {
                        registerShutdownHook();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            setFailed(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() throws Exception {
        try {
            if (isRunning() && setStopping()) {
                worker.unregister(this.getClass().getName());
                doStop();
                setStopped();
            }
        } catch (Exception e) {
            setFailed(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isRunning() {
        return status.get() == Status.STARTED || status.get() == Status.STARTING;
    }

    @Override
    public boolean isStarted() {
        return status.get() == Status.STARTED;
    }

    @Override
    public boolean isStarting() {
        return status.get() == Status.STARTING;
    }

    @Override
    public boolean isStopping() {
        return status.get() == Status.STOPPING;
    }

    @Override
    public boolean isStopped() {
        return status.get() == Status.STOPPED;
    }

    @Override
    public boolean isFailed() {
        return status.get() == Status.FAILED;
    }

    @Override
    public void addLifeCycleListener(Listener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeLifeCycleListener(Listener listener) {
        listeners.remove(listener);
    }


}
