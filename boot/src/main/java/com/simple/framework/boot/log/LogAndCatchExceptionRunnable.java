package com.simple.framework.boot.log;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogAndCatchExceptionRunnable extends AbstractLogWrapper<Runnable> implements Runnable {


    public LogAndCatchExceptionRunnable(Runnable runnable) {
        super(runnable);
    }

    @Override
    public void run() {
        // 把主线程上下文复到子线程
        this.setLogContext();
        try {
            getJob().run();
        } catch (Exception e) { // Catch所有异常阻止其继续传播
            log.error(e.getMessage(), e);
        } finally {
            // 恢复主线程上下文
            this.clearLogContext();
        }
    }
}