package com.simple.framework.boot.typesafe;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.Reflector;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigObject;
import com.typesafe.config.ConfigValueType;

import java.lang.reflect.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@StarterModule(value = "全局配置config注入模块", order = Constants.FRAMEWORK_MODULE_ORDER_CONFIG)
public class ConfigModule extends AbstractModule {

    @Inject
    private Config config;
    @Inject
    private Reflector reflections;

    @Override
    protected void configure() {
        Set<Constructor<Class<?>>> annotatedConstructors = reflections.getConstructorsAnnotatedWith(Named.class);
        for (Constructor<Class<?>> c : annotatedConstructors) {
            Parameter[] params = c.getParameters();
            bindParameters(params);
        }

        Set<Method> annotatedMethods = reflections.getMethodsAnnotatedWith(Named.class);
        for (Method m : annotatedMethods) {
            Parameter[] params = m.getParameters();
            bindParameters(params);
        }

        Set<Field> annotatedFields = reflections.getFieldsAnnotatedWith(Named.class);
        for (Field f : annotatedFields) {
            Named annotation = f.getAnnotation(Named.class);
            bindValue(f.getType(), f.getAnnotatedType().getType(), annotation);
        }
    }

    private void bindParameters(Parameter[] params) {
        for (Parameter p : params) {
            if (p.isAnnotationPresent(Named.class)) {
                Named annotation = p.getAnnotation(Named.class);
                bindValue(p.getType(), p.getAnnotatedType().getType(), annotation);
            }
        }
    }

    private void bindValue(Class<?> paramClass, Type paramType, Named annotation) {
        @SuppressWarnings("unchecked") Key<Object> key = (Key<Object>) Key.get(paramType, annotation);
        String configPath = annotation.value();
        Object configValue = getConfigValue(paramClass, paramType, configPath);
        bind(key).toInstance(configValue);
    }

    private Object getConfigValue(Class<?> paramClass, Type paramType, String path) {
        Optional<Object> extractedValue = ConfigExtractors.extractConfigValue(config, paramClass, path);
        if (extractedValue.isPresent()) {
            return extractedValue.get();
        }

        com.typesafe.config.ConfigValue configValue = config.getValue(path);
        ConfigValueType valueType = configValue.valueType();
        if (valueType.equals(ConfigValueType.OBJECT) && Map.class.isAssignableFrom(paramClass)) {
            ConfigObject object = config.getObject(path);
            return object.unwrapped();
        } else if (valueType.equals(ConfigValueType.OBJECT)) {
            return ConfigBeanFactory.create(config.getConfig(path), paramClass);
        } else if (valueType.equals(ConfigValueType.LIST) && List.class.isAssignableFrom(paramClass)) {
            Type listType = ((ParameterizedType) paramType).getActualTypeArguments()[0];

            Optional<List<?>> extractedListValue = ListExtractors.extractConfigListValue(config, listType, path);

            if (extractedListValue.isPresent()) {
                return extractedListValue.get();
            } else {
                List<? extends Config> configList = config.getConfigList(path);
                return configList.stream()
                        .map(cfg -> ConfigBeanFactory.create(cfg, (Class<?>) listType))
                        .collect(Collectors.toList());
            }
        }

        throw new RuntimeException("Cannot obtain config value for " + paramType + " at path: " + path);
    }
}