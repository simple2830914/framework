package com.simple.framework.boot;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.typesafe.config.Config;

public class BaseModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Config.class).toInstance(LauncherKit.getGlobalConfig());
        bind(Reflector.class).toInstance(LauncherKit.getReflector());
        bind(Worker.class).toInstance(new Worker());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        bind(ObjectMapper.class).toInstance(objectMapper);

    }
}
