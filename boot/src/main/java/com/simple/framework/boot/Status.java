package com.simple.framework.boot;


import lombok.Getter;

@Getter
public enum Status {
    FAILED(-1, "FAILED"),
    STOPPED(0, "STOPPED"),
    STARTING(1, "STARTING"),
    STARTED(2, "STARTED"),
    STOPPING(3, "STOPPING"),
    UNKNOWN(999, "UNKNOWN");

    private final int order;
    private final String name;

    Status(int order, String name) {
        this.order = order;
        this.name = name;
    }

    public static Status of(String name) {
        try {
            return Status.valueOf(name);
        } catch (Exception e) {
            return Status.UNKNOWN;
        }
    }
}