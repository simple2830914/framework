package com.simple.framework.boot;


public interface App {

    void doStart() throws Exception;

    void doStop() throws Exception;

    boolean setStarted();

    boolean setStarting();

    boolean setStopping();

    void setStopped();

    void setFailed(Throwable th);

    void registerShutdownHook();
}
