package com.simple.framework.boot;


public interface LifeCycle {

    void start() throws Exception;

    void stop() throws Exception;

    boolean isRunning();

    boolean isStarted();

    boolean isStarting();

    boolean isStopping();

    boolean isStopped();

    boolean isFailed();

    void addLifeCycleListener(Listener listener);

    void removeLifeCycleListener(Listener listener);

}
