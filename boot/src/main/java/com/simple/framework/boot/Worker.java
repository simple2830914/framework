package com.simple.framework.boot;

import cn.hutool.core.lang.Assert;
import com.google.inject.Singleton;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class Worker {
    private static final Map<String, Application> map = new ConcurrentHashMap<>();

    public Application get(String name) {
        return map.get(name);
    }

    public void register(String name, Application application) {
        Assert.isFalse(map.containsKey(name), "application {} must be Singleton");
        map.put(name, application);
    }

    public void unregister(String name, Application application) {
        map.remove(name, application);
    }

    public void unregister(String name) {
        map.remove(name);
    }
}
