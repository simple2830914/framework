package com.simple.framework.boot.log;

public class LogRunnable extends AbstractLogWrapper<Runnable> implements Runnable {
    public LogRunnable(Runnable runnable) {
        super(runnable);
    }

    @Override
    public void run() {
        // 把主线程上下文复到子线程
        this.setLogContext();
        try {
            getJob().run();
        } finally {
            // 恢复主线程上下文
            this.clearLogContext();
        }
    }
}