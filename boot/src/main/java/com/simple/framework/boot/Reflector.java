package com.simple.framework.boot;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

public interface Reflector {

    Set<Constructor<Class<?>>> getConstructorsAnnotatedWith(Class<? extends Annotation> annotation);

    Set<Method> getMethodsAnnotatedWith(Class<? extends Annotation> annotation);

    Set<Field> getFieldsAnnotatedWith(Class<? extends Annotation> annotation);

    List<Class<?>> getClassesWithAnnotation(Class<? extends Annotation> annotation);

    List<Class<?>> getSubClassWithAnnotation(Class<?> superClass, Class<? extends Annotation> annotation);

    List<Class<?>> getImplClassWithAnnotation(Class<?> interfaceClass, Class<? extends Annotation> annotation);

    List<Class<?>> getImplClass(Class<?> interfaceClass);

    List<Class<?>> getSubClassWithAnnotations(Class<? extends Annotation> annotation, Class<?> superClass, Class<? extends Annotation>... annotations);
}
