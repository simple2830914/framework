package com.simple.framework.boot;

import io.github.classgraph.*;
import lombok.Data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class ClassGraphReflector implements Reflector {

    private ScanResult scanResult;

    public void scan(String... packages) {
        scanResult = new ClassGraph().enableAllInfo().enableSystemJarsAndModules().acceptPackages(packages).scan();
    }

    @Override
    public Set<Constructor<Class<?>>> getConstructorsAnnotatedWith(Class<? extends Annotation> annotation) {
        ClassInfoList classInfos = scanResult.getClassesWithAnnotation(annotation);
        Set<Constructor<Class<?>>> set = new HashSet<>();
        classInfos.forEach(s -> {
            MethodInfoList methodInfos = s.getMethodAndConstructorInfo()
                    .filter(MethodInfo::isConstructor)
                    .filter(methodInfo -> methodInfo.hasAnnotation(annotation));
            methodInfos.forEach(methodInfo -> set.add((Constructor<Class<?>>) methodInfo.loadClassAndGetConstructor()));
        });
        return set;
    }

    @Override
    public Set<Method> getMethodsAnnotatedWith(Class<? extends Annotation> annotation) {
        ClassInfoList classInfos = scanResult.getClassesWithMethodAnnotation(annotation);
        Set<Method> set = new HashSet<>();
        classInfos.forEach(s -> {
            MethodInfoList methodInfos = s.getMethodInfo().filter(methodInfo -> methodInfo.hasAnnotation(annotation));
            methodInfos.forEach(methodInfo -> set.add(methodInfo.loadClassAndGetMethod()));
        });
        return set;
    }

    @Override
    public Set<Field> getFieldsAnnotatedWith(Class<? extends Annotation> annotation) {
        ClassInfoList classInfos = scanResult.getClassesWithFieldAnnotation(annotation);
        Set<Field> set = new HashSet<>();
        classInfos.forEach(s -> {
            FieldInfoList fieldInfos = s.getFieldInfo().filter(fieldInfo -> fieldInfo.hasAnnotation(annotation));
            fieldInfos.forEach(fieldInfo -> set.add(fieldInfo.loadClassAndGetField()));
        });
        return set;
    }

    @Override
    public List<Class<?>> getClassesWithAnnotation(Class<? extends Annotation> annotation) {
        return scanResult.getClassesWithAnnotation(annotation).loadClasses();
    }

    @Override
    public List<Class<?>> getSubClassWithAnnotation(Class<?> superClass, Class<? extends Annotation> annotation) {
        return scanResult.getClassesWithAnnotation(annotation)
                .filter(s -> s.extendsSuperclass(superClass))
                .loadClasses();
    }

    @SafeVarargs
    @Override
    public final List<Class<?>> getSubClassWithAnnotations(Class<? extends Annotation> annotation, Class<?> superClass, Class<? extends Annotation>... annotations) {
        return scanResult.getClassesWithAnnotation(annotation)
                .filter(s -> s.extendsSuperclass(superClass))
                .filter(s -> Arrays.stream(annotations).anyMatch(s::hasAnnotation))
                .loadClasses();
    }

    @Override
    public List<Class<?>> getImplClassWithAnnotation(Class<?> interfaceClass, Class<? extends Annotation> annotation) {
        return scanResult.getClassesWithAnnotation(annotation)
                .filter(s -> s.implementsInterface(interfaceClass))
                .loadClasses();
    }

    @Override
    public List<Class<?>> getImplClass(Class<?> interfaceClass) {
        return scanResult.getClassesImplementing(interfaceClass).loadClasses();
    }
}
