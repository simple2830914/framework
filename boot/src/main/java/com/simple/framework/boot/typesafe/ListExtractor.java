package com.simple.framework.boot.typesafe;

import com.typesafe.config.Config;

import java.lang.reflect.Type;
import java.util.List;


public interface ListExtractor {

    List<?> extractListValue(Config config, String path);

    Type getMatchingParameterizedType();
}


