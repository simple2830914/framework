package com.simple.framework.data.gen;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenTable {

    private String tableName;
    private List<String> indexes;
    private String comment;
    private List<GenField> fields;
    private String prop;


}
