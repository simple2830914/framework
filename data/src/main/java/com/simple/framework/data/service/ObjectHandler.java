package com.simple.framework.data.service;

import com.simple.framework.data.AbstractPlatformApi;

public interface ObjectHandler {

    Object handle(Object object, NodeRender render, AbstractPlatformApi service, String methodName, Object[] params);

}
