package com.simple.framework.data.entity;

import com.fasterxml.jackson.databind.JsonNode;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Unique;
import com.simple.framework.data.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@Table(value = "user_corporation")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("租户用户关系表")
public class UserCorporation extends BaseEntity<JsonNode> {
    @Column
    @Comment("用户ID")
    @Unique
    private String userId;
    @Column
    @Comment("租户ID")
    @Unique
    private String corporationId;
}
