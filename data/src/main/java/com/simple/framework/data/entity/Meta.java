package com.simple.framework.data.entity;

import com.fasterxml.jackson.databind.JsonNode;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.EnumValue;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Text;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@Table(value = "meta")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("元数据表")
public class Meta extends BaseEntity<Meta.Ext> {

    @Column
    @Comment("元数据配置Id")
    private String configId;

    @Column
    @Comment("企业Id")
    private String corporationId;

    @Column
    @Comment("元数据名称")
    private String entityName;

    @Column
    @Comment("元数据类型")
    private MetaConfig.Type type;

    @Column
    @Comment("来源traceId")
    private String traceId;

    @Column
    @Comment("数据标识")
    private String dataId;

    @Column
    @Comment("数据出口标识")
    private String outId;

    @Column
    @Comment("处理状态")
    private Status status;

    @Column
    @Comment("处理描述")
    @Text
    private String message;

    @Column
    @Comment("数据旧MD5值")
    private String oldMd5;

    @Column
    @Comment("数据MD5值")
    private String md5;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {
        private JsonNode source;
        private JsonNode local;
        private JsonNode callback;
    }

    @Getter
    public enum Status {
        NONE("none", "无需处理"),
        PREPARATION("preparation", "准备中"),
        PROCESSOR("processor", "处理中"),
        DONE("done", "处理完成"),
        FAIL("fail", "处理失败");
        @EnumValue
        private final String name;
        private final String remark;

        Status(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }

    }
}
