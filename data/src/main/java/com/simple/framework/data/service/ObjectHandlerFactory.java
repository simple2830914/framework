package com.simple.framework.data.service;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class ObjectHandlerFactory {

    @Autowired
    private ApplicationContext applicationContext;

    public ObjectHandler getHandler(Object object) {
        try {
            return applicationContext.getBean(StrUtil.lowerFirst(object.getClass().getSimpleName()) + "Handler", ObjectHandler.class);
        } catch (Exception e) {
            return null;
        }
    }

}
