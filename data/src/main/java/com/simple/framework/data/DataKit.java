package com.simple.framework.data;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ReflectUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.simple.framework.annotation.ApiPlatform;
import com.simple.framework.annotation.Master;
import com.simple.framework.annotation.MasterGroup;
import com.simple.framework.data.entity.CommonLog;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

@Slf4j
public class DataKit {


    public static Client client() {
        ClientConfig config = new ClientConfig();
        config.property(ClientProperties.CONNECT_TIMEOUT, 60000);
        config.property(ClientProperties.READ_TIMEOUT, 60000);
        return ClientBuilder.newClient(config).register(MultiPartFeature.class);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <T> T doMethod(Object obj, String methodName, Object... args) {
        Method method = ReflectUtil.getMethodOfObj(obj, methodName, args);
        try {
            if (method == null) {
                method = ReflectUtil.getMethodByName(obj.getClass(), methodName);
                if (method == null) {
                    throw new RuntimeException("Method not found: " + methodName);
                }
                return (T) method.invoke(obj, (Object) args);
            }
            return (T) method.invoke(obj, args);
        } catch (Exception e) {
            if (e instanceof InvocationTargetException) {
                InvocationTargetException ex = (InvocationTargetException) e;
                log.error("执行方法{}异常", methodName, e);
                throw new RuntimeException(ex.getTargetException().getMessage());
            } else {
                log.error("执行方法{}异常", methodName, e);
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    public static String getParamsMasterGroup(Object obj, String methodName, Object... args) {
        Method method = ReflectUtil.getMethodOfObj(obj, methodName, args);
        StringBuilder name = new StringBuilder();
        if (Objects.nonNull(method)) {
            Parameter[] parameters = method.getParameters();
            int index = 0;
            for (Parameter parameter : parameters) {
                if (parameter.isAnnotationPresent(MasterGroup.class)) {
                    name.append(":").append(args[index]);
                }
                index++;
            }
        }
        return name.toString();
    }

    public static String getMethodMasterName(Object obj, String methodName, Object... args) {
        Method method = ReflectUtil.getMethodOfObj(obj, methodName, args);
        if (Objects.isNull(method)) {
            return null;
        }
        String entityName = getEntityNameFromApiPlatform(obj.getClass());
        if (Objects.nonNull(entityName)) {
            return getEntityNameFromMaster(method, entityName);
        }
        return null;
    }

    public static String getMethodMasterName(Method method) {
        String entityName = getEntityNameFromApiPlatform(method.getDeclaringClass());
        if (Objects.nonNull(entityName)) {
            return getEntityNameFromMaster(method, entityName);
        }
        return null;
    }

    public static String getEntityNameFromApiPlatform(Class<?> obj) {
        ApiPlatform apiPlatform = obj.getAnnotation(ApiPlatform.class);
        return Objects.nonNull(apiPlatform) && CharSequenceUtil.isNotEmpty(apiPlatform.name()) ? apiPlatform.name() : null;
    }

    public static String getEntityNameFromMaster(Method method, String entityName) {
        Master master = method.getAnnotation(Master.class);
        return Objects.nonNull(master) && CharSequenceUtil.isNotEmpty(master.value()) ? entityName + master.value() : null;
    }

    public static String printStackTrace(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return "\r\n" + sw + "\r\n";
    }

    public static boolean hasMethod(Object obj, String methodName, Object... args) {
        Method method = ReflectUtil.getMethodOfObj(obj, methodName, args);
        if (method == null) {
            method = ReflectUtil.getMethodByName(obj.getClass(), methodName);
        }
        return method != null;
    }

    public static JsonNode rest(RestRequest request) {
        if (request == null) {
            return null;
        }
        if (Objects.nonNull(request.getRequestHandler())) {
            request.getRequestHandler().accept(request);
        }
        Client client;
        if (Objects.nonNull(request.getClientHandler())) {
            client = request.getClientHandler().get();
        } else {
            client = client();
        }
        AtomicReference<WebTarget> webTarget = new AtomicReference<>(client.target(request.getHost())
                .path(request.getApi()));
        if (Objects.nonNull(request.getQueryParam())) {
            request.getQueryParam().forEach((k, v) -> v.forEach(m -> webTarget.set(webTarget.get().queryParam(k, m))));
        }
        Invocation.Builder builder = webTarget.get().request().headers(request.getHeaders());
        Response response = builder.method(request.getMethod(), request.getEntity());
        response.bufferEntity();
        if (Objects.nonNull(request.getStatusHandler())) {
            request.getStatusHandler().accept(response, response.getStatus());
        }
        Function<Response, JsonNode> responseHandler = s -> {
            try {
                return s.readEntity(JsonNode.class);
            } catch (Exception e) {
                String error = s.readEntity(String.class);
                throw new RuntimeException(error);
            }
        };
        if (Objects.nonNull(request.getResponseHandler())) {
            responseHandler = request.getResponseHandler();
        }
        return responseHandler.apply(response);
    }

    public static CommonLog.Http toLogBean(RestRequest request) {
        StringBuilder params = new StringBuilder();
        if (CollUtil.isNotEmpty(request.getQueryParam())) {
            params.append("?");
            for (Map.Entry<String, List<Object>> entry : request.getQueryParam().entrySet()) {
                for (Object s : entry.getValue()) {
                    params.append(entry.getKey())
                            .append("=")
                            .append(URLEncoder.encode(s.toString(), StandardCharsets.UTF_8))
                            .append("&");
                }
            }
        }
        Object body = request.getEntity();
        if (body != null) {
            if (request.getEntity().getEntity() instanceof FormDataMultiPart) {
                FormDataMultiPart form = (FormDataMultiPart) request.getEntity().getEntity();
                Map<String, Object> map = new HashMap<>();
                form.getFields().forEach((k, v) -> map.put(k, v.get(0).isSimple() ? v.get(0).getValue() : v.get(0)
                        .getEntity()));
                body = map;
            } else {
                body = request.getEntity().getEntity();
            }

        }
        return CommonLog.Http.builder()
                .url(request.getHost() + request.getApi() + params)
                .method(request.getMethod())
                .params(body)
                .build();
    }
}
