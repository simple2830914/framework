package com.simple.framework.data;

import com.google.inject.Inject;
import com.simple.framework.annotation.Master;
import com.simple.framework.annotation.Starter;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.Application;
import com.simple.framework.boot.Order;
import com.simple.framework.boot.Reflector;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.Set;

@Slf4j
@Starter("Meta查找器：查找平台主数据项")
@Order(Constants.FRAMEWORK_STARTER_ORDER_GEN)
public class MetaStarter extends Application {
    @Inject
    Reflector reflector;

    @Override
    public void doStart() throws Exception {
        Set<Method> methods = reflector.getMethodsAnnotatedWith(Master.class);
        for (Method method : methods) {
            String entityName = DataKit.getMethodMasterName(method);
            log.info("平台主数据项：{}", entityName);
        }
    }
}
