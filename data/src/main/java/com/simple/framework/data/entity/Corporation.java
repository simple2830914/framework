package com.simple.framework.data.entity;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Unique;
import com.simple.framework.data.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@Table(value = "corporation")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("租户信息表")
public class Corporation extends BaseEntity<ObjectNode> {

    @Column
    @Comment("租户名称")
    @Unique
    private String name;

    @Column
    @Comment("租户管理员ID")
    private String ownerId;

}
