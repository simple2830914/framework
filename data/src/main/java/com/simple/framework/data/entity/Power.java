package com.simple.framework.data.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.EnumValue;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Unique;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@Table(value = "power")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("功能信息表")
public class Power extends BaseEntity<Power.Ext> {

    @Column
    @Comment("功能名称")
    @Unique
    private String name;

    @Column
    @Comment("功能描述")
    private String description;

    @Column
    @Comment("功能编码")
    @Unique
    private String code;

    @Column
    @Comment("功能类型")
    private Type type;

    @Getter
    public enum Type {
        NORMAL("normal", "普通类型"),
        DATA("data", "基础数据");
        @EnumValue
        private final String name;
        private final String remark;

        Type(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {
        @Comment("功能全局字段")
        private List<Field> config;
    }

}
