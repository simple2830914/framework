package com.simple.framework.data;

import com.mybatisflex.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum Platform {
    JAVA("java", "内置函数"),
    CHECK("check", "内置函数rule执行");

    @EnumValue
    private final String name;
    private final String remark;

    Platform(String name, String remark) {
        this.name = name;
        this.remark = remark;
    }

}