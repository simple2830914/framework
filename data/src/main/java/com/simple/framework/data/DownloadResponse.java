package com.simple.framework.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class DownloadResponse {
    private String fileId;
    private String corporationPowerId;
    private String corporationId;
    private String url;
    private String fileType;
    private String savePath;
    private String fileName;
}
