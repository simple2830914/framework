package com.simple.framework.data.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.boot.log.LogRunnable;
import com.simple.framework.data.AbstractPlatformApi;
import com.simple.framework.data.CallbackResponse;
import com.simple.framework.data.Platform;
import com.simple.framework.data.entity.*;
import com.simple.framework.data.mapper.MetaMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.simple.framework.data.service.NodeRender.JSON_PATH_SUB;

@Component
@Scope("prototype")
public class NodeExecutor {

    @Autowired
    DataService dataService;

    @Autowired
    MetaMapper metaMapper;

    @Autowired
    ObjectMapper objectMapper;

    @Getter
    @Setter
    NodeContext context;

    @Getter
    @Setter
    NodeRender nodeRender;

    @SneakyThrows
    public Node renderNode(Node node, int i, List<Node> nodes) {
        String newNode = objectMapper.writeValueAsString(node);
        newNode = newNode.replace("::_NODE_NAME", "$." + node.getName());
        newNode = newNode.replace("::_TRACEID", MdcUtil.get());
        if (i > 0) {
            newNode = newNode.replace("::_PRE_NODE_NAME", "$." + nodes.get(i - 1).getName());
        }
        return objectMapper.readValue(newNode, Node.class);
    }

    private void updateCache(String key, Object object) {
        context.getCache().put(key, object);
        JSONObject jsonObject = (JSONObject) context.getCacheJson();
        jsonObject.set(key, object);
    }

    public boolean flows() {
        List<Node> nodes = context.getConfigs();
        for (int i = 0; i < nodes.size(); i++) {
            Node node = renderNode(nodes.get(i), i, nodes);
            renderProps(node);
            boolean isAsync = Boolean.TRUE.equals(node.getAsync());
            boolean isLast = i == nodes.size() - 1;
            if (isAsync) {
                ThreadUtil.execAsync(new LogRunnable(() -> {
                    if (checkRules(node)) {
                        renderExecute(node, isAsync, isLast);
                    } else {
                        renderError(node, "校验规则不通过", true);
                    }
                }));
            } else {
                if (!checkRules(node)) {
                    renderError(node, "校验规则不通过", true);
                    return true;
                }
                renderExecute(node, isAsync, isLast);
            }
        }
        return false;
    }

    public void renderLast(Node node, boolean isAsync, boolean isLast) {
        if (isLast) {
            lastNode(node, isAsync);
            successMeta();
            renderCallback();
        }
    }

    public void renderExecute(Node node, boolean isAsync, boolean isLast) {
        try {
            Set<String> metaProps = renderMeta(node);
            if (context.isCallback()) {
                saveMetaByOutId(metaProps, isAsync);
            } else {
                saveMeta(metaProps, isAsync);
            }
            renderBefore(node);
            renderFields(node);
            renderAfters(node);
            renderMessage(node);
            renderLast(node, isAsync, isLast);
        } catch (Exception e) {
            renderError(node, ExceptionUtil.getSimpleMessage(e), false);
            throw new RuntimeException("配置渲染异常:" + e.getMessage());
        }
    }

    @SneakyThrows
    private void renderMessage(Node node) {
        if (Objects.nonNull(node.getExt()) && node.getExt().isOpenMessage()) {
            Message message = objectMapper.readValue(JSONUtil.toJsonStr(context.getCache()
                    .get("_messageBean")), Message.class);
            ObjectNode result = objectMapper.createObjectNode();
            result.put("success", true);
            result.put("msg", "接收成功");
            message.getExt().setResult(result);
            dataService.saveMessage(message);
        }
    }

    @SneakyThrows
    private void renderErrorMessage(Node node, String msg) {
        if (Objects.nonNull(node.getExt()) && node.getExt().isOpenMessage()) {
            Message message = objectMapper.readValue(JSONUtil.toJsonStr(context.getCache()
                    .get("_messageBean")), Message.class);
            ObjectNode result = objectMapper.createObjectNode();
            result.put("success", false);
            result.put("msg", msg);
            message.getExt().setResult(result);
            dataService.saveMessage(message);
        }
    }

    private void renderData(Object k, Object v, Map<Object, Object> valueMap) {
        Object key = nodeRender.renderExpression((String) k, Platform.JAVA, true);
        Object value = v;
        if (v instanceof String) {
            value = nodeRender.renderExpression((String) v, Platform.JAVA, true);
        } else if (v instanceof List) {
            List<Object> objects = (List<Object>) v;
            List<Object> newValues = new ArrayList<>();
            for (Object m : objects) {
                if (m instanceof String) {
                    newValues.add(nodeRender.renderExpression((String) m, Platform.JAVA, true));
                } else {
                    newValues.add(m);
                }
            }
            value = newValues;
        } else if (v instanceof Map) {
            Map<Object, Object> map = (Map<Object, Object>) v;
            Map<Object, Object> newValues = new HashMap<>();
            for (Map.Entry<Object, Object> entry : map.entrySet()) {
                renderData(entry.getKey(), entry.getValue(), newValues);
            }
            value = newValues;
        }
        valueMap.put(key, value);
    }

    private void renderProps(Node node) {
        if (Objects.nonNull(node.getExt()) && CollUtil.isNotEmpty(node.getExt().getProps())) {
            Map<Object, Object> props = new HashMap<>();
            try {
                node.getExt().getProps().forEach((k, v) -> renderData(k, v, props));
            } catch (Exception e) {
                throw new RuntimeException("Props渲染异常:" + e.getMessage());
            }
            saveBusinessLog(node, "Props渲染", "", "完成", props);
            updateCache("props", props);
        }
    }

    private boolean renderRuleCondition(String conditionExpression) {
        Object condition = nodeRender.renderCondition(conditionExpression, Platform.CHECK, true);
        if (!(condition instanceof Boolean)) {
            return false;
        }
        return (boolean) condition;
    }

    private boolean checkRules(Node config) {
        List<Rule> rules = new ArrayList<>();
        if (Objects.nonNull(config.getExt())) {
            rules = Optional.ofNullable(config.getExt().getRules()).orElse(new ArrayList<>());
        }
        for (Rule rule : rules) {
            Object obj;
            try {
                if (CharSequenceUtil.isNotEmpty(rule.getCondition()) && (!renderRuleCondition(rule.getCondition()))) {
                    continue;
                }
                saveBusinessLog(config, "Rule检查", rule.getName(), "", null);
                AbstractPlatformApi service = nodeRender.getService(rule.getPlatform());
                obj = nodeRender.renderExpression(rule.getExpression(), service, true);
                Assert.isTrue(obj instanceof Boolean, "返回格式非Boolean类型");
                if (rule.isOperate()) {
                    setCache(rule.getKey(), obj, service);
                } else {
                    assert obj instanceof Boolean;
                    Assert.isTrue((Boolean) obj, "检查结果不满足:{}", obj);
                }
            } catch (Exception e) {
                saveBusinessLogException(config, "Rule检查", rule.getName(), rule.getExpression(), ExceptionUtil.stacktraceToString(e));
                if (rule.isWarning()) {
                    saveBusinessLog(config, "Render渲染", rule.getName(), "跳过异常", e.getMessage());
                    return false;
                } else {
                    renderError(config, "Rule检查异常:" + e.getMessage(), false);
                    throw new RuntimeException("Rule检查异常:" + e.getMessage());
                }
            }
        }
        return true;
    }


    private void renderError(Node config, String msg, boolean isRuleCheck) {
        failMeta(isRuleCheck ? "Rule检查异常:" + msg : msg, isRuleCheck);
        if (isRuleCheck && !config.getExt().isMessageFilterRule()) {
            renderErrorMessage(config, msg);
        }
        if (!isRuleCheck) {
            try {
                Map<Integer, Render> renderMap = new HashMap<>();
                if (Objects.nonNull(config.getExt())) {
                    renderMap = Optional.ofNullable(config.getExt().getError()).orElse(new HashMap<>());
                }
                List<Render> renders = sortRenderMap(renderMap);
                updateCache("_errorMsg", CharSequenceUtil.format("{}执行异常:{}", config.getName(), msg));
                renders("Error监听", config, renders);
            } catch (Exception ex) {
                throw new RuntimeException("Error监听执行异常");
            }
        }
        failedCallback();
    }

    private void renderAfters(Node config) {
        Map<Integer, Render> renderMap = new HashMap<>();
        if (Objects.nonNull(config.getExt())) {
            renderMap = Optional.ofNullable(config.getExt().getAfter()).orElse(new HashMap<>());
        }
        List<Render> renders = sortRenderMap(renderMap);
        renders("After渲染", config, renders);
    }

    private void renderBefore(Node config) {
        Map<Integer, Render> renderMap = new HashMap<>();
        if (Objects.nonNull(config.getExt())) {
            renderMap = Optional.ofNullable(config.getExt().getBefore()).orElse(new HashMap<>());
        }
        List<Render> renders = sortRenderMap(renderMap);
        context.getConfigRender().put(config.getId(), renders);
        renders("Render渲染", config, renders);
    }

    private void renderFields(Node config) {
        if (Objects.nonNull(config.getExt()) && CollUtil.isNotEmpty(config.getExt().getFields())) {
            Map<String, Object> fieldValue = nodeRender.renderField(config.getExt()
                    .getFields(), context.getCacheJson(), true);
            saveBusinessLog(config, "Field渲染", "", "完成", fieldValue);
            updateCache(config.getName(), fieldValue);
        }
    }

    private void renders(String title, Node config, List<Render> renders) {
        for (Render render : renders) {
            Object obj;
            saveBusinessLog(config, title, render.getName(), "", null);
            try {
                AbstractPlatformApi service = nodeRender.getService(render.getPlatform());
                if (CharSequenceUtil.isNotEmpty(render.getCondition())) {
                    Object condition = nodeRender.renderCondition(render.getCondition(), Platform.CHECK, true);
                    if (!(condition instanceof Boolean)) {
                        saveBusinessLog(config, title, render.getName(), "执行条件不满足", condition);
                        continue;
                    }
                    boolean flag = (boolean) condition;
                    if (!flag) {
                        saveBusinessLog(config, title, render.getName(), "执行条件不满足", false);
                        continue;
                    }
                }
                obj = nodeRender.renderExpression(render.getExpression(), service, true);

                if (render.isOperate()) {
                    obj = null;
                } else {
                    setCache(render.getKey(), obj, service);
                }
            } catch (Exception e) {
                e.printStackTrace();
                saveBusinessLogException(config, title, render.getName(), render.getExpression(), ExceptionUtil.stacktraceToString(e));
                if (render.isWarning()) {
                    saveBusinessLog(config, title, render.getName(), "跳过异常", e.getMessage());
                    continue;
                } else {
                    throw new RuntimeException(title + "执行异常:" + e.getMessage());
                }
            }
            saveBusinessLog(config, title, render.getName(), "完成", obj);
        }
    }

    private List<Render> sortRenderMap(Map<Integer, Render> renderMap) {
        List<Render> sortRender = new ArrayList<>();
        renderMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(x -> sortRender.add(x.getValue()));
        return sortRender;
    }

    @SuppressWarnings("unchecked")
    private Set<String> renderMeta(Node config) {
        if (Objects.nonNull(config.getExt()) && CollUtil.isNotEmpty(config.getExt().getMeta())) {
            Map<String, Object> metaValue = (Map<String, Object>) context.getCache()
                    .getOrDefault("meta", new HashMap<>());
            Set<String> indexKeys = config.getExt().getMeta().keySet();
            config.getExt().getMeta().forEach((k, v) -> {
                Map<String, Object> fieldValue = nodeRender.renderField(v, context.getCacheJson(), true);
                saveBusinessLog(config, "Meta渲染", k, "完成", fieldValue);
                Map<String, Object> oValue = (Map<String, Object>) metaValue.getOrDefault(k, new HashMap<>());
                oValue.putAll(fieldValue);
                metaValue.put(k, oValue);
            });
            updateCache("meta", metaValue);
            return indexKeys;
        }
        return CollUtil.newHashSet();
    }

    private void lastNode(Node s, boolean isAsync) {
        if (isAsync) {
            context.setAsync(true);
        } else {
            Node.Type type = s.getType();
            if (type == Node.Type.FORWARDING || type == Node.Type.RESPONSE) {
                if (type == Node.Type.FORWARDING) {
                    context.setForwarding(true);
                }
                if (Boolean.TRUE.equals(s.getCustomResponse())) {
                    context.setCustomResponse(true);
                    context.setMediaType(s.getMediaType());
                }
                context.setResponsePath(s.getExpression());
            }
        }
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    private void saveMetaByOutId(Set<String> keys, boolean isAsync) {
        Map<String, Object> meta = (Map<String, Object>) context.getCache().getOrDefault("meta", new HashMap<>());
        JSON json = JSONUtil.parse(objectMapper.writeValueAsString(meta));
        Map<String, Object> metaValue = (Map<String, Object>) context.getCache()
                .getOrDefault("metaObj", new HashMap<>());
        for (String k : keys) {
            String outId = json.getByPath(JSON_PATH_SUB + k + ".outId", String.class);
            Map<String, Object> logContext = new HashMap<>();
            logContext.put("key", k);
            logContext.put("outId", outId);
            Meta metaObj = dataService.selectMetaOutOne(context.getCorporationId(), k, outId);
            JsonNode local = objectMapper.convertValue(meta.get(k), JsonNode.class);
            ObjectNode callback = objectMapper.createObjectNode();
            callback.put("callback", true);
            callback.put("platform", context.getPlatform());
            callback.put("corporationPowerId", context.getCorporationPowerId());
            callback.set("data", context.getHttpContext());
            if (Objects.isNull(metaObj)) {
                logContext.put("action", "save");
                dataService.saveOutMeta(context.getCorporationId(), k, outId, callback, local);
            } else {
                logContext.put("action", "update");
                dataService.updateOutMeta(metaObj.getId(), outId, callback, local, isAsync ? null : Meta.Status.PROCESSOR);
            }
            metaObj = dataService.selectMetaOutOne(context.getCorporationId(), k, outId);
            Assert.isTrue(Objects.nonNull(metaObj), "未开通 meta:" + k);
            logContext.put("meta", metaObj);
            dataService.saveBusinessLog(MdcUtil.get(), context.getCorporationPowerId(), "meta", "saveMetaByOutId", logContext);
            metaValue.put(k, JSONUtil.parse(objectMapper.writeValueAsString(metaObj)));
        }
        updateCache("metaObj", metaValue);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    private void saveMeta(Set<String> keys, boolean isAsync) {
        Map<String, Object> meta = (Map<String, Object>) context.getCache().getOrDefault("meta", new HashMap<>());
        JSON json = JSONUtil.parse(objectMapper.writeValueAsString(meta));
        Map<String, Object> metaValue = (Map<String, Object>) context.getCache()
                .getOrDefault("metaObj", new HashMap<>());
        for (String k : keys) {
            String dataId = json.getByPath(JSON_PATH_SUB + k + ".dataId", String.class);
            String outId = json.getByPath(JSON_PATH_SUB + k + ".outId", String.class);

            Map<String, Object> logContext = new HashMap<>();
            logContext.put("key", k);
            logContext.put("dataId", dataId);
            logContext.put("outId", outId);
            Meta metaObj = dataService.selectMetaOne(context.getCorporationId(), k, dataId);
            JsonNode local = objectMapper.convertValue(meta.get(k), JsonNode.class);
            ObjectNode source = objectMapper.createObjectNode();
            source.put("callback", false);
            source.put("corporationPowerId", context.getCorporationPowerId());
            source.set("data", context.getHttpContext());

            if (Objects.isNull(metaObj)) {
                logContext.put("action", "save");
                dataService.saveMeta(context.getCorporationId(), k, dataId, source, local);
            } else {
                logContext.put("action", "update");
                dataService.updateMeta(metaObj.getId(), outId, source, local, isAsync ? null : Meta.Status.PROCESSOR);
            }
            metaObj = dataService.selectMetaOne(context.getCorporationId(), k, dataId);

            logContext.put("meta", metaObj);
            dataService.saveBusinessLog(MdcUtil.get(), context.getCorporationPowerId(), "meta", "saveMeta", logContext);
            if (Objects.nonNull(metaObj)) {
                metaValue.put(k, JSONUtil.parse(objectMapper.writeValueAsString(metaObj)));
            }
        }
        updateCache("metaObj", metaValue);
    }

    public void renderCallback() {
        if (context.isCallback()) {
            CallbackResponse response = context.getCallbackResponse();
            if (!response.isStatus()) {
                errorMessage();
            } else {
                updateCache("callbackResponse", response.getResponse());
            }
            updateCache("callbackResult", nodeRender.getService(context.getPlatform())
                    .callbackReturn(response, context.getCache()));
        }
    }

    public void failedCallback() {
        if (context.isCallback()) {
            CallbackResponse response = context.getCallbackResponse();
            response.setStatus(false);
            errorMessage();
            updateCache("callbackResult", nodeRender.getService(context.getPlatform())
                    .callbackReturn(response, context.getCache()));
        }
    }

    @SneakyThrows
    public void errorMessage() {
        Message message = objectMapper.readValue(JSONUtil.toJsonStr(context.getCache()
                .get("_messageBean")), Message.class);
        dataService.errorMessage(message);
    }


    @SuppressWarnings("unchecked")
    @SneakyThrows
    private void failMeta(String msg, boolean isCheck) {
        Map<String, Object> meta = (Map<String, Object>) context.getCache().getOrDefault("metaObj", new HashMap<>());
        JSON json = JSONUtil.parse(objectMapper.writeValueAsString(meta));
        meta.forEach((k, v) -> {
            String id = json.getByPath(JSON_PATH_SUB + k + ".id", String.class);
            Map<String, Object> logContext = new HashMap<>();
            logContext.put("key", k);
            logContext.put("id", id);
            logContext.put("action", "fail");
            logContext.put("msg", msg);
            dataService.saveBusinessLog(MdcUtil.get(), context.getCorporationPowerId(), "meta", "failMeta", logContext);
            if (isCheck) {
                dataService.metaNoneById(id, msg);
            } else {
                dataService.metaFailById(id, msg);
            }
        });
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    private void successMeta() {
        Map<String, Object> meta = (Map<String, Object>) context.getCache().getOrDefault("metaObj", new HashMap<>());
        JSON json = JSONUtil.parse(objectMapper.writeValueAsString(meta));
        meta.forEach((k, v) -> {
            String id = json.getByPath(JSON_PATH_SUB + k + ".id", String.class);
            dataService.metaDoneById(id, "操作完成");
        });
    }


    @SneakyThrows
    private void setCache(String key, Object obj, AbstractPlatformApi service) {
        if (CharSequenceUtil.isNotEmpty(key)) {
            boolean serverUpdate = key.startsWith(JSON_PATH_SUB.concat("corporation")) || key.equals("corporation");
            if (key.startsWith(JSON_PATH_SUB)) {
                context.getCacheJson().putByPath(key, obj);
                context.getCache().putAll(context.getCacheJson().toBean(new TypeReference<Map<String, Object>>() {
                }));
            } else {
                JSONObject object = (JSONObject) context.getCacheJson();
                if (obj == null) {
                    object.remove(key);
                    context.getCache().remove(key);
                } else {
                    context.getCache().put(key, obj);
                    object.set(key, obj);
                }
            }
            if (serverUpdate) {
                context.setCorporation(JSONUtil.parse(objectMapper.writeValueAsString(context.getCache()
                        .get("corporation"))));
                service.setCorporation(context.getCorporation());
            }
        }
    }

    private void saveBusinessLog(Node config, String title, String name, String step, Object context) {
        dataService.saveBusinessLog(MdcUtil.get(), config.getCorporationPowerId(), "WEBHOOK", CharSequenceUtil.format("{}{}:【{}{}{}】{}", Boolean.TRUE.equals(config.getAsync()) ? "异步" : "", title, config.getName(), CharSequenceUtil.isEmpty(name) ? "" : ".", name, step), context);
    }

    private void saveBusinessLogException(Node config, String title, String name, String expression, Object context) {
        dataService.saveBusinessLog(MdcUtil.get(), config.getCorporationPowerId(), "WEBHOOK", CharSequenceUtil.format("{}{}:【{}.{}】执行异常 - expression【{}】", Boolean.TRUE.equals(config.getAsync()) ? "异步" : "", title, config.getName(), name, expression), context);

    }

}
