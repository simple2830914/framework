package com.simple.framework.data.gen;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ReflectUtil;
import com.google.inject.Inject;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.datasource.DataSourceKey;
import com.mybatisflex.core.row.Db;
import com.mybatisflex.core.row.Row;
import com.simple.framework.annotation.*;
import com.simple.framework.beans.Constants;
import com.simple.framework.beans.kit.GenericsUtils;
import com.simple.framework.boot.Reflector;
import com.simple.framework.template.sql.SqlTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GenService {

    @Autowired
    @Inject
    GenConfigProperties genConfigProperties;

    @Autowired
    @Inject
    Reflector reflector;

    @Autowired
    @Inject
    private SqlTemplate sqlTemplate;

    public List<String> compare(List<GenTable> source, List<GenTable> target) {
        Map<String, GenTable> targetMap = target.stream().collect(Collectors.toMap(GenTable::getTableName, s -> s));
        List<String> sqlList = new ArrayList<>();
        for (GenTable table : source) {
            String tableName = table.getTableName();
            if (targetMap.containsKey(tableName)) {
                compare(table, targetMap.get(tableName), sqlList);
            } else {
                String sql = sqlTemplate.renderSql(genConfigProperties.createTableTemplate, MapUtil.builder("VALUE", table)
                        .build());
                sqlList.add(sql);
            }
        }
        return sqlList;
    }

    public void compare(GenTable source, GenTable target, List<String> sqlList) {
        if (!source.getComment().equals(target.getComment())) {
            String sql = sqlTemplate.renderSql(genConfigProperties.modifyComment, MapUtil.builder("VALUE", source)
                    .build());
            sqlList.add(sql);
        }
        if (CollUtil.isNotEmpty(source.getIndexes())) {
            for (String index : source.getIndexes()) {
                if (!target.getIndexes().contains(index)) {
                    String sql = sqlTemplate.renderSql(genConfigProperties.createIndex, source.getTableName(), index);
                    sqlList.add(sql);
                }
            }
        }
        Map<String, GenField> targetMap = target.getFields()
                .stream()
                .collect(Collectors.toMap(GenField::getName, s -> s));

        for (GenField field : source.getFields()) {
            if (targetMap.containsKey(field.getName())) {
                if (!compareField(field, targetMap.get(field.getName()))) {
                    field.setTableName(source.getTableName());
                    String sql = sqlTemplate.renderSql(genConfigProperties.modifyField, MapUtil.builder("VALUE", field)
                            .build());
                    sqlList.add(sql);
                }
            } else {
                field.setTableName(source.getTableName());
                String sql = sqlTemplate.renderSql(genConfigProperties.addField, MapUtil.builder("VALUE", field)
                        .build());
                sqlList.add(sql);
            }
        }
    }

    public boolean compareField(GenField source, GenField target) {
        CompareService<GenField> compareService = new CompareService<>();
        List<String> list = compareService.compare(source, target);
        if (!list.isEmpty()) {
            log.info("{} - {}", source.getName(), list);
        }
        return list.isEmpty();
    }


    public List<GenTable> scanGenTable() {
        List<Class<?>> classList = reflector.getClassesWithAnnotation(Table.class);
        List<GenTable> genTables = new ArrayList<>();
        for (Class<?> clazz : classList) {
            List<GenField> gFields = new ArrayList<>();
            Field[] fields = ReflectUtil.getFields(clazz, s -> {
                Column columnAnnotation = s.getAnnotation(Column.class);
                Id idAnnotation = s.getAnnotation(Id.class);
                return Objects.nonNull(columnAnnotation) || Objects.nonNull(idAnnotation);
            });
            List<String> uniqueIndex = new ArrayList<>();
            for (Field field : fields) {
                try {
                    GenField gField = GenField.builder()
                            .props(getColumnProp(field, ""))
                            .comment(getComment(field))
                            .type(getColumnType(field))
                            .build();
                    if (Objects.nonNull(field.getAnnotation(Column.class))) {
                        gField.setName(getColumnName(field));
                    } else {
                        gField.setName(getIdName(field));
                    }
                    if (field.getName().equals("ext")) {
                        gFields.addAll(getGeneratedColumn(GenericsUtils.getSuperClassGenericType(clazz), ""));
                    }
                    if (field.isAnnotationPresent(Unique.class)) {
                        uniqueIndex.add(gField.getName());
                    }
                    gFields.add(gField);
                } catch (Exception e) {
                    log.info("未知异常：{}", field, e);
                    throw new RuntimeException(e);
                }

            }
            String prop = "";
            if (CollUtil.isNotEmpty(uniqueIndex)) {
                Collections.sort(uniqueIndex);
                String name = CharSequenceUtil.join("_", uniqueIndex);
                String value = CharSequenceUtil.join(",", uniqueIndex);
                prop = CharSequenceUtil.format(genConfigProperties.uniqueIndex, name, value);
            }

            @SuppressWarnings("unchecked") Map<String, List<String>> indexesMap = (Map<String, List<String>>) genConfigProperties.genConfig.get("indexes")
                    .unwrapped();
            List<String> indexes = indexesMap.get(getTableName(clazz));
            GenTable table = GenTable.builder()
                    .tableName(getTableName(clazz))
                    .comment(getComment(clazz))
                    .fields(gFields)
                    .indexes(indexes)
                    .prop(prop)
                    .build();
            genTables.add(table);
        }
        return genTables;
    }

    public void createSchema() {
        DataSourceKey.use(Constants.FRAMEWORK_DB_NAME_OTHER, () -> {
            Db.updateBySql(sqlTemplate.renderSql(genConfigProperties.createDataBaseTemplate, genConfigProperties.database));
        });
    }

    public List<GenTable> queryAllGenTable() {
        List<GenTable> genTables = new ArrayList<>();
        List<Row> tableList = Db.selectListBySql(sqlTemplate.renderSql(genConfigProperties.queryAllTablesTemplate, genConfigProperties.database));
        List<Row> fieldList = Db.selectListBySql(sqlTemplate.renderSql(genConfigProperties.queryAllFieldsTemplate, genConfigProperties.database));
        List<Row> indexList = Db.selectListBySql(sqlTemplate.renderSql(genConfigProperties.queryAllIndexesTemplate, genConfigProperties.database));
        for (Row row : tableList) {
            String tableName = getTableName(row);
            List<GenField> tableFields = fieldList.stream()
                    .filter(s -> getTableName(s).equals(tableName))
                    .map(this::row2GenField)
                    .collect(Collectors.toList());
            List<String> tableIndexes = indexList.stream()
                    .filter(s -> getTableName(s).equals(tableName))
                    .map(s -> s.getString("INDEX_NAME"))
                    .collect(Collectors.toList());
            GenTable table = GenTable.builder()
                    .tableName(tableName)
                    .comment(row.getString("TABLE_COMMENT"))
                    .fields(tableFields)
                    .indexes(tableIndexes)
                    .build();
            genTables.add(table);
        }
        return genTables;
    }

    private GenField row2GenField(Row row) {
        StringBuilder stringBuilder = new StringBuilder();

        if (Boolean.TRUE.equals(row.getBoolean("IS_GENERATION"))) {
            stringBuilder.append(" ").append(row.getString("GENERATION_EXPRESSION"));
        }
        if (Boolean.TRUE.equals(row.getBoolean("IS_NULLABLE"))) {
            stringBuilder.append(" ").append(genConfigProperties.notNullProp);
        }
        Object o = row.get("COLUMN_DEFAULT");
        if (Objects.nonNull(o)) {
            if (o instanceof String && row.getString("COLUMN_TYPE").equalsIgnoreCase("bit(1)")) {
                String s = (String) o;
                if (s.equals("b'0'")) {
                    o = false;
                } else if (s.equals("b'1'")) {
                    o = true;
                }
            }
            stringBuilder.append(" ").append(CharSequenceUtil.format(genConfigProperties.defaultProp, o));
        }

        if (Boolean.TRUE.equals(row.getBoolean("IS_PRIMARY"))) {
            stringBuilder.append(" ").append(genConfigProperties.primaryProp);
        }
        return GenField.builder()
                .type(row.getString("COLUMN_TYPE"))
                .name(row.getString("COLUMN_NAME"))
                .comment(row.getString("COLUMN_COMMENT"))
                .props(stringBuilder.toString())
                .build();

    }

    private String getTableName(Row row) {
        return row.getString("TABLE_NAME");
    }


    private List<GenField> getGeneratedColumn(Class<?> clazz, String parentName) {
        List<GenField> gFields = new ArrayList<>();
        Field[] fields = ReflectUtil.getFields(clazz, s -> s.isAnnotationPresent(GeneratedColumn.class) || s.isAnnotationPresent(GeneratedAble.class));
        for (Field field : fields) {
            if (field.isAnnotationPresent(GeneratedColumn.class)) {
                GenField gField = GenField.builder()
                        .name(getColumnName(field))
                        .props(getColumnProp(field, parentName))
                        .comment(getComment(field))
                        .type(getColumnType(field))
                        .build();
                gFields.add(gField);
            }
            if (field.isAnnotationPresent(GeneratedAble.class) && (!isBaseType(field.getType()))) {
                gFields.addAll(getGeneratedColumn(field.getType(), parentName + field.getName() + "."));
            }
        }
        return gFields;
    }

    public static boolean isBaseType(Class<?> className) {
        return className.equals(Integer.class) || className.equals(Byte.class) || className.equals(Long.class) || className.equals(Double.class) || className.equals(Float.class) || className.equals(Character.class) || className.equals(Short.class) || className.equals(Boolean.class);
    }

    @SuppressWarnings("unchecked")
    private String getColumnType(Field field) {
        String javaType = field.getType().getName();
        Length lengthAnnotation = field.getAnnotation(Length.class);
        if (field.getName().equals("ext")) {
            javaType = "ext";
        }
        if (field.isAnnotationPresent(Text.class)) {
            javaType = "text";
        }
        if (field.getType().isEnum()) {
            javaType = String.class.getName();
        }
        Map<String, Object> typeMap = (Map<String, Object>) genConfigProperties.genConfig.get("typeMap").unwrapped();
        Assert.isTrue(typeMap.containsKey(javaType), "存在未配置的映射的类型{}", javaType);
        Map<String, String> typeConfig = (Map<String, String>) typeMap.get(javaType);
        String template = typeConfig.get("type");
        String length = Objects.nonNull(lengthAnnotation) ? lengthAnnotation.value() : typeConfig.getOrDefault("length", "");
        return CharSequenceUtil.format(template, length);
    }

    private String getColumnProp(Field field, String parentName) {
        StringBuilder stringBuilder = new StringBuilder();
        if (field.isAnnotationPresent(GeneratedColumn.class)) {
            stringBuilder.append(" ")
                    .append(CharSequenceUtil.format(genConfigProperties.generatedColumnProp, parentName + field.getName()));
        }

        if (field.isAnnotationPresent(NotNull.class)) {
            stringBuilder.append(" ").append(genConfigProperties.notNullProp);
        }

        if (field.isAnnotationPresent(Default.class)) {
            Default defaultAnnotation = field.getAnnotation(Default.class);
            stringBuilder.append(" ")
                    .append(CharSequenceUtil.format(genConfigProperties.defaultProp, defaultAnnotation.value()));
        }

        if (field.isAnnotationPresent(Primary.class)) {
            stringBuilder.append(" ").append(genConfigProperties.primaryProp);
        }
        return stringBuilder.toString();
    }

    private String getColumnName(Field field) {
        Column columnAnnotation = field.getAnnotation(Column.class);
        if (CharSequenceUtil.isNotEmpty(columnAnnotation.value())) {
            return columnAnnotation.value();
        }
        return CharSequenceUtil.toUnderlineCase(field.getName());
    }

    private String getIdName(Field field) {
        return CharSequenceUtil.toUnderlineCase(field.getName());
    }

    private String getComment(Field field) {
        Comment commentAnnotation = field.getAnnotation(Comment.class);
        String enumComment = "";
        if (field.getType().isEnum()) {
            enumComment = Arrays.toString(field.getType().getEnumConstants());
        }
        return Objects.nonNull(commentAnnotation) ? commentAnnotation.value() + enumComment : "";
    }

    private String getComment(Class<?> clazz) {
        Comment commentAnnotation = clazz.getAnnotation(Comment.class);
        return Objects.nonNull(commentAnnotation) ? commentAnnotation.value() : "";
    }

    private String getTableName(Class<?> clazz) {
        return clazz.getAnnotation(Table.class).value();
    }
}
