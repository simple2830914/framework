package com.simple.framework.data.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.row.Db;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.data.AbstractPlatformApi;
import com.simple.framework.data.DataKit;
import com.simple.framework.data.PageRestRequest;
import com.simple.framework.data.Platform;
import com.simple.framework.data.entity.BaseMeta;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.entity.Meta;
import com.simple.framework.data.entity.MetaConfig;
import com.simple.framework.data.mapper.MessageMapper;
import com.simple.framework.data.mapper.MetaMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.simple.framework.data.entity.Message.Action.*;

@Service
public class PageRestRequestHandler implements ObjectHandler {

    @Autowired
    DataService dataService;

    @Autowired
    RestService restService;

    @Autowired
    ObjectMapper objectMapper;

    @SneakyThrows
    public MetaConfig initPageRequestByConfig(PageRestRequest pageRequest, NodeRender render, String entityName) {
        if (CharSequenceUtil.isNotEmpty(entityName)) {
            MetaConfig metaConfig = dataService.selectMetaConfig(render.getContext().getCorporationId(), entityName);
            Assert.isTrue(Objects.nonNull(metaConfig), "Meta[{}]未找到对应的配置信息", entityName);
            pageRequest.setPageIndex(metaConfig.getExt().getPageIndex());
            pageRequest.setPageLength(metaConfig.getExt().getPageLength());
            pageRequest.setMax(metaConfig.getExt().getMax());
            pageRequest.setPageCursor(metaConfig.getExt().getPageCursor());
            if (CharSequenceUtil.isNotEmpty(metaConfig.getExt()
                    .getSourceTimeExpress()) && Objects.nonNull(metaConfig.getExt().getMaxSourceTime())) {
                pageRequest.setSourceTime(render.executeJava(metaConfig.getExt()
                        .getSourceTimeExpress(), JSONUtil.parse(objectMapper.writeValueAsString(metaConfig.getExt())), render.getService(Platform.JAVA), false));
            } else {
                pageRequest.setSourceTime(metaConfig.getExt().getMaxSourceTime());
            }
            return metaConfig;
        }
        return null;
    }

    public void saveBaseMeta(MetaConfig metaConfig, NodeRender render, List<JsonNode> queryList, boolean isFirst) {
        List<Meta> saveList = new ArrayList<>();
        List<Meta> updateList = new ArrayList<>();
        List<Object> sourceTimes = new ArrayList<>();
        List<Message> messageList = new ArrayList<>();
        if (isFirst && metaConfig.getExt().isFullDownload()) {
            dataService.disableMetaByConfigId(metaConfig.getCorporationId(), metaConfig.getId());
        }
        for (JsonNode meta : queryList) {
            try {
                JSON json = JSONUtil.parse(objectMapper.writeValueAsString(meta));
                Map<String, Object> local = render.renderField(metaConfig.getExt().getFields(), json, false);
                JsonNode localNode = objectMapper.convertValue(local, JsonNode.class);
                BaseMeta baseMeta = objectMapper.convertValue(local, BaseMeta.class);
                if (!metaConfig.getExt().isMd5WithSourceTime()) {
                    local.remove("sourceTime");
                }
                sourceTimes.add(baseMeta.getSourceTime());
                String md5 = SecureUtil.md5(objectMapper.writeValueAsString(local));
                ObjectNode node = objectMapper.createObjectNode();
                node.set("_webhookBody", meta);
                node.set("_localNode", localNode);
                if (CharSequenceUtil.isNotEmpty(baseMeta.getDataId())) {
                    Message message;
                    Meta queryMeta = dataService.selectMetaOne(render.getContext()
                            .getCorporationId(), metaConfig.getEntityName(), baseMeta.getDataId());
                    if (Objects.isNull(queryMeta)) {
                        Meta saveMeta = Meta.builder()
                                .corporationId(render.getContext().getCorporationId())
                                .entityName(metaConfig.getEntityName())
                                .dataId(baseMeta.getDataId())
                                .outId(baseMeta.getOutId())
                                .active(baseMeta.isActive())
                                .configId(metaConfig.getId())
                                .traceId(MdcUtil.get())
                                .type(metaConfig.getType())
                                .md5(md5)
                                .message("")
                                .status(Meta.Status.PREPARATION)
                                .ext(Meta.Ext.builder().source(meta).local(localNode).build())
                                .build();
                        if (baseMeta.isActive()) {
                            message = Message.builder()
                                    .corporationId(metaConfig.getCorporationId())
                                    .corporationPowerId(render.getContext().getCorporationPowerId())
                                    .action(ADD)
                                    .traceId(MdcUtil.get())
                                    .status(Message.Status.PROCESSOR)
                                    .source(render.getContext().getSource())
                                    .platform(render.getContext().getPlatform())
                                    .ext(Message.Ext.builder().data(node).build())
                                    .build();
                            messageList.add(message);
                        }
                        saveList.add(saveMeta);
                    } else {
                        Meta updateMeta = Meta.builder()
                                .id(queryMeta.getId())
                                .corporationId(render.getContext()
                                        .getCorporationId())
                                .entityName(metaConfig.getEntityName())
                                .dataId(baseMeta.getDataId())
                                .outId(baseMeta.getOutId() == null ? queryMeta.getOutId() : baseMeta.getOutId())
                                .active(baseMeta.isActive())
                                .configId(metaConfig.getId())
                                .traceId(MdcUtil.get())
                                .type(metaConfig.getType())
                                .md5(md5)
                                .version(queryMeta.getVersion())
                                .oldMd5(queryMeta.getMd5())
                                .message(queryMeta.getMd5().equals(md5) ? queryMeta.getMessage() : "")
                                .status(queryMeta.getMd5()
                                        .equals(md5) ? queryMeta.getStatus() : Meta.Status.PREPARATION)
                                .ext(Meta.Ext.builder().source(meta).local(localNode).build())
                                .build();
                        if (!queryMeta.getMd5().equals(md5)) {
                            message = Message.builder()
                                    .corporationId(metaConfig.getCorporationId())
                                    .corporationPowerId(render.getContext().getCorporationPowerId())
                                    .action(UPDATE)
                                    .traceId(MdcUtil.get())
                                    .status(Message.Status.PROCESSOR)
                                    .source(render.getContext().getSource())
                                    .platform(render.getContext().getPlatform())
                                    .ext(Message.Ext.builder().data(node).build())
                                    .build();
                            if (Boolean.TRUE.equals(queryMeta.getActive()) && !baseMeta.isActive()) {
                                Message statusMessage = Message.builder()
                                        .corporationId(metaConfig.getCorporationId())
                                        .corporationPowerId(render.getContext().getCorporationPowerId())
                                        .action(DISABLE)
                                        .traceId(MdcUtil.get())
                                        .status(Message.Status.PROCESSOR)
                                        .source(render.getContext().getSource())
                                        .platform(render.getContext().getPlatform())
                                        .ext(Message.Ext.builder().data(node).build())
                                        .build();
                                messageList.add(statusMessage);
                            }
                            if (Boolean.TRUE.equals(!queryMeta.getActive()) && baseMeta.isActive()) {
                                Message statusMessage = Message.builder()
                                        .corporationId(metaConfig.getCorporationId())
                                        .corporationPowerId(render.getContext().getCorporationPowerId())
                                        .action(ENABLE)
                                        .traceId(MdcUtil.get())
                                        .status(Message.Status.PROCESSOR)
                                        .source(render.getContext().getSource())
                                        .platform(render.getContext().getPlatform())
                                        .ext(Message.Ext.builder().data(node).build())
                                        .build();
                                messageList.add(statusMessage);
                            }
                            messageList.add(message);
                        }
                        updateList.add(updateMeta);
                    }

                } else {
                    throw new RuntimeException("dataId不能为空");
                }
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        if (!messageList.isEmpty() && metaConfig.getExt().isEventOpen()) {
            int[] rss = Db.executeBatch(messageList, 1000, MessageMapper.class, BaseMapper::insert);
            if (Arrays.stream(rss).anyMatch(r -> r == 0)) {
                throw new RuntimeException("乐观锁原因message入库失败");
            }
        }

        if (!saveList.isEmpty()) {
            int[] rss = Db.executeBatch(saveList, 1000, MetaMapper.class, BaseMapper::insert);
            if (Arrays.stream(rss).anyMatch(r -> r == 0)) {
                throw new RuntimeException("乐观锁原因新增失败");
            }
        }
        if (!updateList.isEmpty()) {
            int[] rss = Db.executeBatch(updateList, 1000, MetaMapper.class, BaseMapper::update);
            if (Arrays.stream(rss).anyMatch(r -> r == 0)) {
                throw new RuntimeException("乐观锁原因更新失败");
            }
        }
        List<Object> sourceTimeList = sourceTimes.stream().filter(Objects::nonNull).collect(Collectors.toList());
        if (!sourceTimeList.isEmpty()) {
            if (metaConfig.getExt().getSourceTimeType().equals("Text")) {
                CollUtil.sort(sourceTimeList, Comparator.comparing(o -> ((String) o)));
                metaConfig.getExt().setMaxSourceTime(sourceTimeList.get(sourceTimeList.size() - 1));
            } else if (metaConfig.getExt().getSourceTimeType().equals("Number")) {
                if (sourceTimeList.get(0) instanceof Double) {
                    CollUtil.sort(sourceTimeList, Comparator.comparingDouble(o -> ((Number) o).doubleValue()));
                } else if (sourceTimeList.get(0) instanceof Long) {
                    CollUtil.sort(sourceTimeList, Comparator.comparingDouble(o -> ((Number) o).longValue()));

                } else if (sourceTimeList.get(0) instanceof Float) {
                    CollUtil.sort(sourceTimeList, Comparator.comparingDouble(o -> ((Number) o).floatValue()));

                } else if (sourceTimeList.get(0) instanceof Integer) {
                    CollUtil.sort(sourceTimeList, Comparator.comparingDouble(o -> ((Number) o).intValue()));
                }
                metaConfig.getExt().setMaxSourceTime(sourceTimeList.get(sourceTimeList.size() - 1));
            }
            dataService.saveOrUpdateMetaConfig(metaConfig);
        }
    }

    @Override
    public Object handle(Object obj, NodeRender render, AbstractPlatformApi service, String methodName, Object[] params) {
        PageRestRequest pageRequest = (PageRestRequest) obj;
        String entityName = DataKit.getMethodMasterName(service, methodName, params);
        entityName += DataKit.getParamsMasterGroup(service, methodName, params);
        MetaConfig metaConfig = initPageRequestByConfig(pageRequest, render, entityName);
        Db.tx(() -> {
            restService.autoPage(pageRequest, (isFirst, nodeList) -> saveBaseMeta(metaConfig, render, nodeList, isFirst));
            return true;
        });
        return obj;
    }
}
