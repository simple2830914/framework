package com.simple.framework.data;

import com.simple.framework.beans.Function2;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Data
@Builder
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class CyclePageRestRequest {
    List<Map<String, Object>> data;
    Function<Map<String, Object>, PageRestRequest> cycleHandler;
    Function2<Object, String, String> metaNameHandler;
    String metaName;
    boolean openMessage;
}
