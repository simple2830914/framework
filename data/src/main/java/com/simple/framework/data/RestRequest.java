package com.simple.framework.data;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Data
@SuperBuilder(toBuilder = true)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class RestRequest implements Serializable {
    private String api;
    private String host;
    private MultivaluedMap<String, Object> queryParam;
    private MultivaluedMap<String, Object> headers;
    private String method;
    private Entity<?> entity;
    private Consumer<RestRequest> requestHandler;
    private BiConsumer<Response, Integer> statusHandler;
    private Function<Response, JsonNode> responseHandler;
    private Supplier<Client> clientHandler;
    private Consumer<JsonNode> resultHandler;
}
