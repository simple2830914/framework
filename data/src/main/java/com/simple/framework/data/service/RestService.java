package com.simple.framework.data.service;

import cn.hutool.core.lang.Pair;
import cn.hutool.core.text.CharSequenceUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.data.DataKit;
import com.simple.framework.data.PageRestRequest;
import com.simple.framework.data.RestRequest;
import com.simple.framework.data.entity.CommonLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

@Service
@Slf4j
public class RestService {
    @Autowired
    DataService dataService;

    public Pair<List<JsonNode>, JsonNode> list(PageRestRequest request) {
        List<JsonNode> list;
        JsonNode response = executeRest(request, false);
        if (Objects.nonNull(request.getPageResultHandler())) {
            list = request.getPageResultHandler().apply(request, response);
        } else {
            list = Collections.singletonList(response);
        }
        return Pair.of(list, response);
    }

    public void autoPage(PageRestRequest pageRequest, BiConsumer<Boolean, List<JsonNode>> consumer) {
        int pageStart = Objects.isNull(pageRequest.getPageIndex()) ? 0 : pageRequest.getPageIndex();
        int pageCursor = Objects.isNull(pageRequest.getPageCursor()) ? 0 : pageRequest.getPageCursor();
        for (int i = pageStart; i < pageRequest.getMax(); i++) {
            pageRequest.setPageIndex(i);
            if (Objects.isNull(pageRequest.getCursorCondition())) {
                pageRequest.setPageCursor(pageCursor + pageRequest.getPageIndex() * pageRequest.getPageLength());
            }
            if (Objects.nonNull(pageRequest.getEach())) {
                pageRequest.getEach().accept(pageRequest);
            }
            Pair<List<JsonNode>, JsonNode> pair = list(pageRequest);
            List<JsonNode> queryList = pair.getKey();
            if (!queryList.isEmpty() && Objects.nonNull(consumer)) {
                consumer.accept(i == pageStart, queryList);
            }
            if (Objects.nonNull(pageRequest.getCursorCondition())) {
                pageRequest.getCursorCondition().accept(pageRequest, pair.getValue());
            }
            if (Objects.nonNull(pageRequest.getBreakCondition())) {
                if (Boolean.TRUE.equals(pageRequest.getBreakCondition().apply(pair.getValue(), pageRequest))) {
                    break;
                }
            } else {
                if (queryList.isEmpty() || queryList.size() < pageRequest.getPageLength()) {
                    break;
                }
            }
        }
    }

    public JsonNode executeRest(RestRequest restRequest, boolean isSaveResponse) {
        CommonLog.Http http = DataKit.toLogBean(restRequest);
        try {
            JsonNode node = DataKit.rest(restRequest);
            if (Objects.nonNull(restRequest.getResultHandler())) {
                restRequest.getResultHandler().accept(node);
            }
            if (isSaveResponse) {
                http.setResponse(node);
            }
            dataService.saveHttpLog(MdcUtil.get(), http);
            return node;
        } catch (Exception e) {
            http.setMsg(e.getMessage());
            dataService.saveHttpLog(MdcUtil.get(), http);
            throw new RuntimeException(CharSequenceUtil.format("{}调用异常:{}", http.getUrl(), e.getMessage()));
        }
    }
}
