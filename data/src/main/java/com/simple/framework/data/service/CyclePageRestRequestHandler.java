package com.simple.framework.data.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.CharSequenceUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybatisflex.core.row.Db;
import com.simple.framework.data.AbstractPlatformApi;
import com.simple.framework.data.CyclePageRestRequest;
import com.simple.framework.data.PageRestRequest;
import com.simple.framework.data.entity.MetaConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class CyclePageRestRequestHandler implements ObjectHandler {
    @Autowired
    PageRestRequestHandler pageRestRequestHandler;
    @Autowired
    DataService dataService;

    @Autowired
    RestService restService;

    @Autowired
    ObjectMapper objectMapper;


    @Override
    public Object handle(Object obj, NodeRender render, AbstractPlatformApi service, String methodName, Object[] params) {
        CyclePageRestRequest cyclePageRestRequest = (CyclePageRestRequest) obj;
        if (CollUtil.isNotEmpty(cyclePageRestRequest.getData()) && CharSequenceUtil.isNotEmpty(cyclePageRestRequest.getMetaName()) && Objects.nonNull(cyclePageRestRequest.getCycleHandler()) && Objects.nonNull(cyclePageRestRequest.getMetaNameHandler())) {
            AtomicBoolean isFirstData = new AtomicBoolean(true);
            Db.tx(() -> {
                cyclePageRestRequest.getData().forEach(data -> {
                    PageRestRequest pageRestRequest = cyclePageRestRequest.getCycleHandler().apply(data);
                    String entityName = cyclePageRestRequest.getMetaNameHandler()
                            .apply(data, cyclePageRestRequest.getMetaName());
                    MetaConfig metaConfig = pageRestRequestHandler.initPageRequestByConfig(pageRestRequest, render, entityName);
                    boolean isGroup = !entityName.equals(cyclePageRestRequest.getMetaName());
                    if (isGroup) {
                        restService.autoPage(pageRestRequest, (isFirst, nodeList) -> pageRestRequestHandler.saveBaseMeta(metaConfig, render, nodeList, isFirst));
                    } else {
                        restService.autoPage(pageRestRequest, (isFirst, nodeList) -> pageRestRequestHandler.saveBaseMeta(metaConfig, render, nodeList, isFirstData.get() && isFirst));
                    }
                    if (cyclePageRestRequest.isOpenMessage()) {
                        service.pushWebHookMessage(objectMapper.convertValue(data, JsonNode.class));
                    }
                    isFirstData.set(false);
                });
                return true;
            });
        }
        return obj;
    }
}
