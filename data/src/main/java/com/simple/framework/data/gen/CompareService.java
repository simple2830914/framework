package com.simple.framework.data.gen;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 使用须知： <br>
 * （1）该工具类主要用于两个同类对象的属性值比较； <br>
 * （2）使用本工具类前，请将对应的类属性上打上 @Compare("xxx") 注解，其中xxx为字段的表达名称；<br>
 * （3）为了比较灵活，只有打了该注解才会进行比较，不打的字段则不会进行比较 <br>
 * （4）比较后，只会返回有变化的字段，无变化的字符则不返回 <br>
 */
public class CompareService<T> {

    /**
     * 属性比较
     *
     * @param source 源数据对象
     * @param target 目标数据对象
     * @return 对应属性值的比较变化
     */
    public List<String> compare(T source, T target) {
        return compare(source, target, null);
    }


    /**
     * 属性比较
     *
     * @param source              源数据对象
     * @param target              目标数据对象
     * @param ignoreCompareFields 忽略比较的字段
     * @return 对应属性值的比较变化
     */
    public List<String> compare(T source, T target, List<String> ignoreCompareFields) {
        List<String> list;
        if (Objects.isNull(source) && Objects.isNull(target)) {
            return Collections.emptyList();
        }
        Map<String, CompareNode> sourceMap = this.getFiledValueMap(source);
        Map<String, CompareNode> targetMap = this.getFiledValueMap(target);
        if (sourceMap.isEmpty() && targetMap.isEmpty()) {
            return Collections.emptyList();
        }
        // 如果源数据为空，则只显示目标数据，不显示属性变化情况
        if (sourceMap.isEmpty()) {
            return doEmpty(targetMap, ignoreCompareFields);
        }
        list = doCompare(sourceMap, targetMap, ignoreCompareFields);
        return list;
    }

    private List<String> doEmpty(Map<String, CompareNode> targetMap, List<String> ignoreCompareFields) {
        List<String> list = new ArrayList<>();
        Collection<CompareNode> values = targetMap.values();
        for (CompareNode node : values) {
            Object o = Optional.ofNullable(node.getFieldValue()).orElse("");
            if (Objects.nonNull(ignoreCompareFields) && ignoreCompareFields.contains(node.getFieldKey())) {
                continue;
            }
            if (!o.toString().isEmpty()) {
                list.add("[" + node.getFieldName() + "：" + o + "]");
            }
        }
        return list;
    }

    private List<String> doCompare(Map<String, CompareNode> sourceMap, Map<String, CompareNode> targetMap, List<String> ignoreCompareFields) {
        Set<String> keys = sourceMap.keySet();
        List<String> list = new ArrayList<>();
        for (String key : keys) {
            CompareNode sn = sourceMap.get(key);
            CompareNode tn = targetMap.get(key);
            if (Objects.nonNull(ignoreCompareFields) && ignoreCompareFields.contains(sn.getFieldKey())) {
                continue;
            }
            String sv = Optional.ofNullable(sn.getFieldValue()).orElse("").toString();
            String tv = Optional.ofNullable(tn.getFieldValue()).orElse("").toString();
            // 只有两者属性值不一致时, 才显示变化情况
            if (!sv.equals(tv)) {
                list.add(String.format("[%s：%s -> %s]", sn.getFieldName(), sv, tv));
            }
        }
        return list;
    }

    private Map<String, CompareNode> getFiledValueMap(T t) {
        if (Objects.isNull(t)) {
            return Collections.emptyMap();
        }
        Field[] fields = getAllFields(t.getClass());
        if (fields.length == 0) {
            return Collections.emptyMap();
        }
        Map<String, CompareNode> map = new LinkedHashMap<>();
        for (Field field : fields) {
            Compare compareAnnotation = field.getAnnotation(Compare.class);
            ReflectionUtils.makeAccessible(field);
            try {
                String fieldKey = field.getName();
                CompareNode node = new CompareNode();
                node.setFieldKey(fieldKey);
                node.setFieldValue(field.get(t));
                if (Objects.isNull(compareAnnotation)) {
                    node.setFieldName(field.getName());
                } else {
                    node.setFieldName(compareAnnotation.value());
                }
                map.put(field.getName(), node);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public static Field[] getAllFields(Class<?> clazz) {
        List<Field> fieldList = new ArrayList<>();
        while (clazz != null) {
            fieldList.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
            clazz = clazz.getSuperclass();
        }
        Field[] fields = new Field[fieldList.size()];
        return fieldList.toArray(fields);
    }
}