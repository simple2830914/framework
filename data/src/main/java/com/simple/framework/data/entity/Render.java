package com.simple.framework.data.entity;

import com.simple.framework.annotation.Comment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Render {

    @Comment("render名称")
    private String name;

    @Comment("回写的Key")
    private String key;

    @Comment("render描述")
    private String remark;

    @Comment("渲染表达式：取值表达：$.a,模板取值：{{$.a}}的值,api 方法调用取值：##getToken('{{$.a}}')")
    private String expression;

    @Comment("执行条件")
    private String condition;

    @Comment("标识是否需要返回结果")
    private boolean operate;

    @Comment("配置调用的平台的方法")
    private String platform;

    @Comment("是否忽略异常")
    private boolean warning;


}
