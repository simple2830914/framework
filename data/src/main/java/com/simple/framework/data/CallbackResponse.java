package com.simple.framework.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.function.Function;

@Data
@Builder
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class CallbackResponse {
    private Object response;
    private boolean status;
    private Function<Object, Object> failedHandler;
    private Function<Object, Object> successHandler;
}
