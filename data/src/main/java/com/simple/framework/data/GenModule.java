package com.simple.framework.data;

import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.SimpleModule;
import com.simple.framework.data.gen.GenConfigProperties;
import lombok.extern.slf4j.Slf4j;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

@StarterModule(value = "Gen模块-扫描@Table表生成建表语句", order = Constants.FRAMEWORK_MODULE_ORDER_GEN)
@Slf4j
public class GenModule extends SimpleModule {

    @Override
    protected void configure() {
        bind(GenConfigProperties.class).toInstance(new GenConfigProperties());
        bind(ScriptEngine.class).toInstance(new ScriptEngineManager().getEngineByName("JavaScript"));
    }
}
