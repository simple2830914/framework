package com.simple.framework.data.entity;

import com.mybatisflex.annotation.EnumValue;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Field {

    private String name;
    private String label;
    private String remark;
    private Type type;
    private String platform;
    private String condition;
    private String expression;
    private Object defaultValue;
    private boolean closeLog;
    private List<Field> fields;

    @Getter
    public enum Type {
        STRING("string", "文本类型"),
        BOOLEAN("boolean", "布尔类型"),
        FILE("file", "文件类型"),
        INT("int", "整数类型"),
        LONG("long", "长整型"),
        DECIMAL("decimal", "金额类型"),
        LIST("list", "集合类型"),
        FORM("form", "表单类型"),
        FORMVALUE("formValue", "表单值类型"),
        LISTOBJECT("listObj", "单集合"),
        JSON("json", "json类型"),
        REF("ref", "子集类型"),
        FOREACH("foreach", "集合子集类型");

        @EnumValue
        private final String name;
        private final String remark;

        Type(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }

    }

}
