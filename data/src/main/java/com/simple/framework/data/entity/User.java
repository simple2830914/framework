package com.simple.framework.data.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.GeneratedColumn;
import com.simple.framework.annotation.NotNull;
import com.simple.framework.annotation.Unique;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@Table(value = "user")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("用户信息表")
public class User extends BaseEntity<User.Ext> {

    @Column
    @Comment("用户账号")
    @Unique
    private String name;
    @Column
    @Comment("用户密码")
    private String password;
    @Column
    @Comment("用户姓名")
    private String nickName;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {

        @NotNull
        @GeneratedColumn
        @Column
        @Comment("用户手机号")
        private String phone;

        @NotNull
        @GeneratedColumn
        @Column
        @Comment("用户邮件地址")
        private String email;

    }
}
