package com.simple.framework.data;

import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.beans.ResponseBean;
import com.simple.framework.data.entity.CommonLog;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.service.DataService;
import com.simple.framework.data.service.NodeContext;
import com.simple.framework.data.service.RestService;
import com.simple.framework.redis.RedisService;
import com.simple.framework.template.sql.SqlTemplate;
import com.typesafe.config.Config;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static com.simple.framework.data.entity.Message.Action.NONE;

@Data
public abstract class AbstractPlatformApi implements ICallBack {

    @Autowired
    public ObjectMapper objectMapper;

    @Autowired
    public DataService dataService;

    @Autowired
    public RestService restService;

    @Autowired
    public RedisService redisService;

    @Autowired
    public Config config;

    @Autowired
    private SqlTemplate sqlTemplate;

    public String corporationPowerId;

    public String corporationId;

    public JsonNode httpContext;

    public JSON corporation;

    private String platform;

    private Message.Source source;


    @SneakyThrows
    public Object getConfig(JSON config, String path) {
        return config.getByPath(path);
    }

    public String getStr(JSON config, String path) {
        return config.getByPath(path, String.class);
    }


    public <T> T getCorporation(String path) {
        //noinspection unchecked
        return (T) corporation.getByPath(path);
    }

    public byte[] getCorporationByte(String path) {
        String s = (String) corporation.getByPath(path);
        return s.getBytes(StandardCharsets.UTF_8);
    }

    public String getParameter(JSON config, String name) {
        return config.getByPath(CharSequenceUtil.format("$._webhookGet.{}[0]", name), String.class);
    }

    public String getData(JSON config, String name) {
        return config.getByPath(CharSequenceUtil.format("$._webhookBody.{}", name), String.class);
    }

    public String getHeader(JSON config, String name) {
        return config.getByPath(CharSequenceUtil.format("$._webhookHeader.{}[0]", name), String.class);
    }

    public String getFile(String fileName) {
        String root = config.getString("data.connector.file");
        return CharSequenceUtil.format("{}/{}/{}", root, getCorporationId(), fileName);
    }

    public String getDownloadFile(String fileId, String fileType) {
        String root = config.getString("data.connector.file");
        return CharSequenceUtil.format("{}/{}/{}/{}.{}", root, getCorporationId(), getCorporationPowerId(), fileId, fileType);
    }

    @SneakyThrows
    public ByteArrayInputStream bufferStream(InputStream in) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) > -1) {
            out.write(buffer, 0, len);
        }
        out.flush();
        in.close();
        return new ByteArrayInputStream(out.toByteArray());
    }

    @SneakyThrows
    public DownloadResponse saveFile(String url, int delayTime, String fileId, String defaultType) {
        try {

            Thread.sleep(delayTime);

            InputStream inputStream = new URL(url).openStream();
            ByteArrayInputStream byteArrayInputStream = bufferStream(inputStream);
            String fileType = FileTypeUtil.getType(byteArrayInputStream);
            if (CharSequenceUtil.isEmpty(fileType)) {
                fileType = defaultType;
            }
            String path = getDownloadFile(fileId, fileType);
            String fileName = fileId + "." + fileType;
            byteArrayInputStream.reset();
            IoUtil.copy(byteArrayInputStream, FileUtil.getOutputStream(path));
            dataService.saveFileLog(MdcUtil.get(), CommonLog.File.builder()
                    .workerName("文件下载")
                    .fileId(fileId)
                    .fileName(fileName)
                    .fileType(fileType)
                    .url(url)
                    .savePath(path)
                    .build());
            return DownloadResponse.builder()
                    .fileType(fileType)
                    .fileId(fileId)
                    .savePath(path)
                    .url(url)
                    .fileName(fileName)
                    .corporationId(corporationId)
                    .corporationPowerId(corporationPowerId)
                    .build();
        } catch (Exception e) {
            dataService.saveFileLog(MdcUtil.get(), CommonLog.File.builder()
                    .workerName("文件下载")
                    .fileId(fileId)
                    .url(url)
                    .msg(e.getMessage())
                    .build());
            throw new RuntimeException(fileId + "下载异常");
        }
    }

    @Override
    public CallbackResponse callback(JSON params) {
        return null;
    }

    public Object getRelation(JSONObject object, String key) {
        return object.get(key);
    }

    public ResponseBean execute(String corporationPowerId, boolean callback, String platform, JSON param) throws JsonProcessingException {
        JsonNode node = objectMapper.readValue(param.toBean(String.class), JsonNode.class);
        NodeContext context = dataService.context(corporationPowerId, callback, platform, node, Message.Source.CALLBACK);
        return dataService.response(context);
    }

    @SneakyThrows
    public void pushMessage(JSON param) {
        JsonNode node = objectMapper.readValue(param.toBean(String.class), JsonNode.class);
        Message message = Message.builder()
                .corporationId(getCorporationId())
                .corporationPowerId(getCorporationPowerId())
                .action(NONE)
                .traceId(MdcUtil.get())
                .status(Message.Status.PROCESSOR)
                .source(getSource())
                .platform(getPlatform())
                .ext(Message.Ext.builder().data(node).build())
                .build();
        dataService.saveMessage(message);
    }

    @SneakyThrows
    public void pushMessage(JsonNode node) {
        Message message = Message.builder()
                .corporationId(getCorporationId())
                .corporationPowerId(getCorporationPowerId())
                .action(NONE)
                .traceId(MdcUtil.get())
                .status(Message.Status.PROCESSOR)
                .source(getSource())
                .platform(getPlatform())
                .ext(Message.Ext.builder().data(node).build())
                .build();
        dataService.saveMessage(message);
    }

    @SneakyThrows
    public void pushWebHookMessage(JsonNode node) {
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.set("_webhookBody", node);
        Message message = Message.builder()
                .corporationId(getCorporationId())
                .corporationPowerId(getCorporationPowerId())
                .action(NONE)
                .traceId(MdcUtil.get())
                .status(Message.Status.PROCESSOR)
                .source(getSource())
                .platform(getPlatform())
                .ext(Message.Ext.builder().data(objectNode).build())
                .build();
        dataService.saveMessage(message);
    }

    @SneakyThrows
    public void pushMessage(String platform, String source, JSON param) {
        JsonNode node = objectMapper.readValue(param.toBean(String.class), JsonNode.class);
        Message message = Message.builder()
                .corporationId(getCorporationId())
                .corporationPowerId(getCorporationPowerId())
                .action(NONE)
                .traceId(MdcUtil.get())
                .status(Message.Status.PROCESSOR)
                .source(Message.Source.valueOf(source))
                .platform(platform)
                .ext(Message.Ext.builder().data(node).build())
                .build();
        dataService.saveMessage(message);
    }

    public JsonNode executeRest(RestRequest restRequest, boolean isSaveResponse) {
        return restService.executeRest(restRequest, isSaveResponse);
    }

    public List<JsonNode> executePageRest(PageRestRequest pageRequest) {
        List<JsonNode> nodeList = new ArrayList<>();
        restService.autoPage(pageRequest, (isFirst, queryList) -> nodeList.addAll(queryList));
        return nodeList;
    }
}
