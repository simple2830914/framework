package com.simple.framework.data.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.row.Db;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.beans.ResponseBean;
import com.simple.framework.data.AbstractPlatformApi;
import com.simple.framework.data.CallbackResponse;
import com.simple.framework.data.entity.*;
import com.simple.framework.data.entity.table.Tables;
import com.simple.framework.data.mapper.*;
import jakarta.ws.rs.core.MediaType;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.simple.framework.data.entity.Message.Action.NONE;
import static com.simple.framework.data.service.NodeRender.JSON_PATH_SUB;

@Service
@Slf4j
public class DataService implements IDataService {

    @Autowired
    CommonLogMapper commonLogMapper;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CorporationPowerMapper corporationPowerMapper;

    @Autowired
    NodeMapper configMapper;

    @Autowired
    CorporationMapper corporationMapper;

    @Autowired
    MenuMapper menuMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    PowerMapper powerMapper;

    @Autowired
    NodeMapper nodeMapper;

    @Autowired
    UserCorporationMapper userCorporationMapper;


    @Autowired
    MetaConfigMapper metaConfigMapper;

    @Autowired
    MetaMapper metaMapper;

    @Autowired
    MessageMapper messageMapper;

    @Override
    public boolean saveSqlLog(String traceId, String sourceSql, String targetSql, String schema, long executeTime) {
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.SQL)
                .ext(CommonLog.Ext.builder()
                        .sql(CommonLog.Sql.builder()
                                .sourceSQL(sourceSql)
                                .targetSQL(targetSql)
                                .schemaName(schema)
                                .executeTime(executeTime)
                                .build())
                        .build())
                .build());
        return result > 0;
    }


    @Override
    public boolean saveSqlLog(String traceId, String sql, String schema, long executeTime) {
        return saveSqlLog(traceId, sql, sql, schema, executeTime);
    }

    @Override
    public boolean saveHttpLog(String traceId, String url, String method, Object params, String msg, Object response) {
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.HTTP)
                .ext(CommonLog.Ext.builder()
                        .http(CommonLog.Http.builder()
                                .url(url)
                                .method(method)
                                .params(params)
                                .msg(msg)
                                .response(response)
                                .build())
                        .build())
                .build());
        return result > 0;
    }

    @Override
    public boolean saveHttpLog(String traceId, CommonLog.Http http) {
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.HTTP)
                .ext(CommonLog.Ext.builder().http(http).build())
                .build());
        return result > 0;
    }

    @Override
    public boolean saveFileLog(String traceId, CommonLog.File file) {
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.FILE)
                .ext(CommonLog.Ext.builder().file(file).build())
                .build());
        return result > 0;
    }

    @Override
    public boolean saveJobLog(String traceId, CommonLog.Job job) {
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.JOB)
                .ext(CommonLog.Ext.builder().job(job).build())
                .build());
        return result > 0;
    }

    public boolean saveJobLog(String traceId, String jobName, String powerName, String powerCode, String corporationPowerId, String msg) {
        Map<String, Object> context = new HashMap<>();
        context.put("errorMsg", msg);
        CommonLog.Job job = CommonLog.Job.builder()
                .context(context)
                .jobName(jobName + ":" + powerName + ":" + powerCode)
                .corporationPowerId(corporationPowerId)
                .build();
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.JOB)
                .ext(CommonLog.Ext.builder().job(job).build())
                .build());
        return result > 0;
    }

    public boolean saveJobLog(String traceId, String jobName, String corporationPowerId, String dataId, String msg) {
        Map<String, Object> context = new HashMap<>();
        context.put("errorMsg", msg);
        CommonLog.Job job = CommonLog.Job.builder()
                .context(context)
                .jobName(jobName)
                .dataId(dataId)
                .corporationPowerId(corporationPowerId)
                .build();
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.JOB)
                .ext(CommonLog.Ext.builder().job(job).build())
                .build());
        return result > 0;
    }

    public boolean saveBusinessLog(String traceId, String businessId, String businessName, String stepRemark, Object context) {
        int result = commonLogMapper.insert(CommonLog.builder()
                .traceId(traceId)
                .type(CommonLog.Type.BUSINESS)
                .ext(CommonLog.Ext.builder()
                        .business(CommonLog.Business.builder()
                                .businessId(businessId)
                                .businessName(businessName)
                                .businessStep(stepRemark)
                                .context(context)
                                .build())
                        .build())
                .build());
        return result > 0;
    }

    @Override
    public boolean saveCorporation(Corporation corporation) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.CORPORATION.NAME.eq(corporation.getName()));
        Corporation query = corporationMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            corporation.setId(query.getId());
            return false;
        } else {
            return corporationMapper.insert(corporation) > 0;
        }
    }

    @Override
    public void saveMessage(Message message) {
        messageMapper.insert(message);
    }

    @Override
    public void errorMessage(Message bean) {
        Message message = Message.builder().status(Message.Status.ERROR).build();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.MESSAGE.CORPORATION_ID.eq(bean.getCorporationId()))
                .and(Tables.MESSAGE.CORPORATION_POWER_ID.eq(bean.getCorporationPowerId()))
                .and(Tables.MESSAGE.TRACE_ID.eq(bean.getTraceId()))
                .and(Tables.MESSAGE.SOURCE.eq(bean.getSource()))
                .and(Tables.MESSAGE.PLATFORM.eq(bean.getPlatform()))
        ;
        messageMapper.updateByQuery(message, queryWrapper);
    }

    @Override
    public boolean insertOrUpdateCorporation(Corporation corporation) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (CharSequenceUtil.isNotEmpty(corporation.getId())) {
            queryWrapper.where(Tables.CORPORATION.ID.eq(corporation.getId()));
        } else {
            queryWrapper.where(Tables.CORPORATION.NAME.eq(corporation.getName()));
        }
        Corporation query = corporationMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            corporation.setId(query.getId());
            corporation.setVersion(query.getVersion());
            return corporationMapper.update(corporation) > 0;
        } else {
            return corporationMapper.insert(corporation) > 0;
        }
    }

    @Override
    public boolean saveUser(User user) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.USER.NAME.eq(user.getName()));
        User query = userMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            user.setId(query.getId());
            return false;
        } else {
            return userMapper.insert(user) > 0;
        }
    }

    @Override
    public boolean savePower(Power power) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.POWER.CODE.eq(power.getCode()));
        Power query = powerMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            power.setId(query.getId());
            return false;
        } else {
            return powerMapper.insert(power) > 0;
        }
    }

    @Override
    public boolean saveOrUpdatePower(Power power) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.POWER.CODE.eq(power.getCode()));
        Power query = powerMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            power.setId(query.getId());
            power.setVersion(query.getVersion());
            return powerMapper.update(power) > 0;
        } else {
            return powerMapper.insert(power) > 0;
        }
    }

    @Override
    public void openDataPower(String powerName, String powerCode, String corporationId, String ownerId, int days, ObjectNode config) {
        openPower(Power.Type.DATA, powerName, powerCode, corporationId, ownerId, days, config);
    }

    @Override
    public void openPower(String powerName, String powerCode, String corporationId, String ownerId, int days, ObjectNode config) {
        openPower(Power.Type.NORMAL, powerName, powerCode, corporationId, ownerId, days, config);
    }

    private void openPower(Power.Type type, String powerName, String powerCode, String corporationId, String ownerId, int days, ObjectNode config) {
        Power power = Power.builder()
                .name(powerName)
                .code(powerCode)
                .id(SecureUtil.md5(powerCode))
                .type(type)
                .ext(Power.Ext.builder().build())
                .build();
        saveOrUpdatePower(power);
        CorporationPower corporationPower = CorporationPower.builder()
                .corporationId(corporationId)
                .powerId(power.getId())
                .id(SecureUtil.md5(corporationId + ":" + power.getId()))
                .ownerId(ownerId)
                .expirateDate(DateUtil.offsetDay(new Date(), days))
                .ext(CorporationPower.Ext.builder().config(config).build())
                .build();
        saveOrUpdateCorporationPower(corporationPower);
    }

    @Override
    public Power queryPower(String powerCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.POWER.CODE.eq(powerCode));
        return powerMapper.selectOneByQuery(queryWrapper);
    }

    @Override
    public Power queryDataPower(String powerCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.POWER.CODE.eq(powerCode)).and(Tables.POWER.TYPE.eq(Power.Type.DATA));
        return powerMapper.selectOneByQuery(queryWrapper);
    }

    public List<CorporationPower> queryCorporationPower(String corporationId, Set<String> powerIds) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.CORPORATION_POWER.POWER_ID.in(powerIds))
                .and(Tables.CORPORATION_POWER.CORPORATION_ID.eq(corporationId));
        return corporationPowerMapper.selectListByQuery(queryWrapper);
    }

    @Override
    public void disableMetaByConfigId(String corporationId, String configId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.META.CORPORATION_ID.eq(corporationId)).and(Tables.META.CONFIG_ID.eq(configId));
        metaMapper.updateByQuery(Meta.builder().active(false).build(), queryWrapper);
    }

    @Override
    public CorporationPower queryPower(String corporationId, String powerCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.POWER.CODE.eq(powerCode));
        Power power = powerMapper.selectOneByQuery(queryWrapper);

        queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.CORPORATION_POWER.CORPORATION_ID.eq(corporationId))
                .and(Tables.CORPORATION_POWER.POWER_ID.eq(power.getId()));
        return corporationPowerMapper.selectOneByQuery(queryWrapper);
    }

    @Override
    public boolean saveMenu(Menu menu) {
        return menuMapper.insert(menu) > 0;
    }

    @Override
    public boolean saveOrUpdateMetaConfig(MetaConfig metaConfig) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.META_CONFIG.CORPORATION_ID.eq(metaConfig.getCorporationId()))
                .and(Tables.META_CONFIG.ENTITY_NAME.eq(metaConfig.getEntityName()));
        MetaConfig query = metaConfigMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            metaConfig.setId(query.getId());
            metaConfig.setVersion(query.getVersion());
            return metaConfigMapper.update(metaConfig) > 0;
        } else {
            return metaConfigMapper.insert(metaConfig) > 0;
        }
    }

    @Override
    public boolean openMeta(String corporationId, String metaName, MetaConfig.Ext config) {
        MetaConfig metaConfig = MetaConfig.builder()
                .corporationId(corporationId)
                .entityName(metaName)
                .id(SecureUtil.md5(corporationId + ":" + metaName))
                .type(MetaConfig.Type.BASE)
                .ext(config)
                .build();
        return saveOrUpdateMetaConfig(metaConfig);
    }

    @Override
    public boolean openMeta(String corporationId, String metaName) {
        MetaConfig metaConfig = MetaConfig.builder()
                .corporationId(corporationId)
                .entityName(metaName)
                .id(SecureUtil.md5(corporationId + ":" + metaName))
                .type(MetaConfig.Type.BUSINESS)
                .build();
        return saveOrUpdateMetaConfig(metaConfig);
    }

    @Override
    public boolean saveOrUpdateMeta(Meta meta) {
        if (CharSequenceUtil.isNotEmpty(meta.getId())) {
            Meta query = metaMapper.selectOneById(meta.getId());
            if (Objects.nonNull(query)) {
                meta.setId(query.getId());
                meta.setVersion(query.getVersion());
                return metaMapper.update(meta) > 0;
            } else {
                return metaMapper.insert(meta) > 0;
            }
        } else {
            return metaMapper.insert(meta) > 0;
        }
    }


    @SneakyThrows
    @Override
    public Meta saveMeta(String corporationId, String entityName, String dataId, JsonNode source, JsonNode local) {
        MetaConfig metaConfig = metaConfigMapper.selectOneByQuery(new QueryWrapper().where(Tables.META_CONFIG.CORPORATION_ID.eq(corporationId))
                .and(Tables.META_CONFIG.ENTITY_NAME.eq(entityName))
                .and(Tables.META_CONFIG.ACTIVE.eq(true)));
        if (Objects.nonNull(metaConfig)) {
            Meta meta = Meta.builder()
                    .dataId(dataId)
                    .traceId(MdcUtil.get())
                    .configId(metaConfig.getId())
                    .entityName(entityName)
                    .corporationId(corporationId)
                    .type(metaConfig.getType())
                    .status(Meta.Status.PREPARATION)
                    .ext(Meta.Ext.builder().source(source).local(local).build())
                    .build();
            saveOrUpdateMeta(meta);
            if (CharSequenceUtil.isNotEmpty(meta.getId())) {
                return metaMapper.selectOneById(meta.getId());
            }
        }
        return null;
    }

    @SneakyThrows
    @Override
    public Meta saveOutMeta(String corporationId, String entityName, String outId, JsonNode callback, JsonNode local) {
        MetaConfig metaConfig = metaConfigMapper.selectOneByQuery(new QueryWrapper().where(Tables.META_CONFIG.CORPORATION_ID.eq(corporationId))
                .and(Tables.META_CONFIG.ENTITY_NAME.eq(entityName))
                .and(Tables.META_CONFIG.ACTIVE.eq(true)));
        if (Objects.nonNull(metaConfig)) {
            Meta meta = Meta.builder()
                    .traceId(MdcUtil.get())
                    .configId(metaConfig.getId())
                    .entityName(entityName)
                    .corporationId(corporationId)
                    .type(metaConfig.getType())
                    .status(Meta.Status.PREPARATION)
                    .outId(outId)
                    .ext(Meta.Ext.builder().callback(callback).local(local).build())
                    .build();
            saveOrUpdateMeta(meta);
            if (CharSequenceUtil.isNotEmpty(meta.getId())) {
                return metaMapper.selectOneById(meta.getId());
            }
        }
        return null;
    }


    @SneakyThrows
    @Override
    public boolean updateOutMeta(String id, String outId, JsonNode callback, JsonNode local, Meta.Status status) {
        return Db.txWithResult(() -> {
            Meta query = metaMapper.selectOneByQuery(new QueryWrapper().where(Tables.META.ID.eq(id)).forUpdate());
            JsonNode oLocal = query.getExt().getLocal();
            ObjectNode nLocal = objectMapper.createObjectNode();
            if (Objects.nonNull(oLocal)) {
                forEach(oLocal, (k, v) -> nLocal.set((String) k, v));
            }
            if (Objects.nonNull(local)) {
                forEach(local, (k, v) -> nLocal.set((String) k, v));
            }
            Meta meta = Meta.builder()
                    .traceId(MdcUtil.get())
                    .status(status)
                    .message(null)
                    .outId(outId == null ? query.getOutId() : outId)
                    .ext(Meta.Ext.builder()
                            .source(query.getExt().getSource())
                            .callback(callback == null ? query.getExt().getCallback() : callback)
                            .local(nLocal)
                            .build())
                    .build();
            meta.setId(query.getId());
            meta.setVersion(query.getVersion());
            return metaMapper.update(meta) > 0;
        });
    }

    @SneakyThrows
    @Override
    public boolean updateMeta(String id, String outId, JsonNode source, JsonNode local, Meta.Status status) {
        return Db.txWithResult(() -> {
            Meta query = metaMapper.selectOneByQuery(new QueryWrapper().where(Tables.META.ID.eq(id)).forUpdate());
            Meta meta = Meta.builder()
                    .traceId(MdcUtil.get())
                    .status(status)
                    .message(null)
                    .outId(outId == null ? query.getOutId() : outId)
                    .ext(Meta.Ext.builder()
                            .source(source == null ? query.getExt().getSource() : source)
                            .local(local == null ? query.getExt().getLocal() : local)
                            .build())
                    .build();
            meta.setId(query.getId());
            meta.setVersion(query.getVersion());
            return metaMapper.update(meta) > 0;
        });

    }


    @Override
    public void metaFailById(String id, String msg) {
        Db.tx(() -> {
            metaMapper.selectOneByQuery(new QueryWrapper().where(Tables.META.ID.eq(id)).forUpdate());
            metaMapper.updateByQuery(Meta.builder()
                    .status(Meta.Status.FAIL)
                    .message(msg)
                    .build(), true, new QueryWrapper().where(Tables.META.ID.eq(id)));
            return true;
        });
    }

    @Override
    public void metaNoneById(String id, String msg) {
        Db.tx(() -> {
            metaMapper.selectOneByQuery(new QueryWrapper().where(Tables.META.ID.eq(id)).forUpdate());
            metaMapper.updateByQuery(Meta.builder()
                    .status(Meta.Status.NONE)
                    .message(msg)
                    .build(), true, new QueryWrapper().where(Tables.META.ID.eq(id)));
            return true;
        });
    }


    @Override
    public void metaDoneById(String id, String msg) {
        Db.tx(() -> {
            metaMapper.selectOneByQuery(new QueryWrapper().where(Tables.META.ID.eq(id)).forUpdate());
            metaMapper.updateByQuery(Meta.builder()
                    .status(Meta.Status.DONE)
                    .message(msg)
                    .build(), true, new QueryWrapper().where(Tables.META.ID.eq(id)));
            return true;
        });
    }


    @Override
    public Meta selectMetaOne(String corporationId, String entityName, String dataId) {
        List<Meta> metas = selectMetaList(corporationId, entityName, dataId);
        return CollUtil.isNotEmpty(metas) ? metas.get(0) : null;
    }

    @Override
    public Meta selectMetaOutOne(String corporationId, String entityName, String outId) {
        List<Meta> metas = selectMetaOutList(corporationId, entityName, outId);
        return CollUtil.isNotEmpty(metas) ? metas.get(0) : null;
    }

    @Override
    public List<Meta> selectMetaOutList(String corporationId, String entityName, String outId) {
        return metaMapper.selectListByQuery(new QueryWrapper().where(Tables.META.CORPORATION_ID.eq(corporationId))
                .and(Tables.META.ENTITY_NAME.eq(entityName))
                .and(Tables.META.OUT_ID.eq(outId)));
    }

    @Override
    public List<Meta> selectMetaList(String corporationId, String entityName, String dataId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.META.CORPORATION_ID.eq(corporationId))
                .and(Tables.META.ENTITY_NAME.eq(entityName))
                .and(Tables.META.DATA_ID.eq(dataId));
        return metaMapper.selectListByQuery(queryWrapper);
    }

    @Override
    public List<Meta> selectTodoMetaList(String corporationId, String metaName, String condition, int limit) {
        QueryWrapper queryWrapper = new QueryWrapper().where(Tables.META.ENTITY_NAME.eq(metaName))
                .and(Tables.META.CORPORATION_ID.eq(corporationId));
        if (CharSequenceUtil.isNotEmpty(condition)) {
            queryWrapper.and(condition);
        } else {
            queryWrapper.and(Tables.META.STATUS.in(Meta.Status.PREPARATION, Meta.Status.FAIL));
        }
        return metaMapper.selectListByQuery(queryWrapper.orderBy(Tables.META.UPDATE_TIME, true).limit(limit));
    }

    @Override
    public List<Meta> selectMetaListByCondition(String corporationId, String metaName, String condition) {
        QueryWrapper queryWrapper = new QueryWrapper().where(Tables.META.ENTITY_NAME.eq(metaName))
                .and(Tables.META.CORPORATION_ID.eq(corporationId));
        if (CharSequenceUtil.isNotEmpty(condition)) {
            queryWrapper.and(condition);
        }
        return metaMapper.selectListByQuery(queryWrapper.orderBy(Tables.META.UPDATE_TIME, true));
    }

    @Override
    public MetaConfig selectMetaConfig(String corporationId, String entityName) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.META_CONFIG.CORPORATION_ID.eq(corporationId))
                .and(Tables.META_CONFIG.ENTITY_NAME.eq(entityName));
        return metaConfigMapper.selectOneByQuery(queryWrapper);
    }

    @Override
    public boolean openCorporationPower(CorporationPower corporationPower) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.CORPORATION_POWER.CORPORATION_ID.eq(corporationPower.getCorporationId()))
                .and(Tables.CORPORATION_POWER.POWER_ID.eq(corporationPower.getPowerId()));
        CorporationPower query = corporationPowerMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            corporationPower.setId(query.getId());
            return false;
        } else {
            return corporationPowerMapper.insert(corporationPower) > 0;
        }
    }

    @Override
    public boolean saveOrUpdateCorporationPower(CorporationPower corporationPower) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.CORPORATION_POWER.CORPORATION_ID.eq(corporationPower.getCorporationId()))
                .and(Tables.CORPORATION_POWER.POWER_ID.eq(corporationPower.getPowerId()));
        CorporationPower query = corporationPowerMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            corporationPower.setId(query.getId());
            corporationPower.setVersion(query.getVersion());
            return corporationPowerMapper.update(corporationPower) > 0;
        } else {
            return corporationPowerMapper.insert(corporationPower) > 0;
        }
    }

    @Override
    public boolean savaCorporationPowerNode(Node node) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.NODE.NAME.eq(node.getName()))
                .and(Tables.NODE.CORPORATION_POWER_ID.eq(node.getCorporationPowerId()));
        Node query = nodeMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            node.setId(query.getId());
            return false;
        } else {
            return nodeMapper.insert(node) > 0;
        }
    }

    @Override
    public boolean savaOrUpdateCorporationPowerNode(Node node) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.NODE.NAME.eq(node.getName()))
                .and(Tables.NODE.CORPORATION_POWER_ID.eq(node.getCorporationPowerId()));
        Node query = nodeMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            node.setId(query.getId());
            node.setVersion(query.getVersion());
            return nodeMapper.update(node) > 0;
        } else {
            return nodeMapper.insert(node) > 0;
        }
    }

    public boolean addNode(String corporationId, String powerCode, String nodeName, int order, Node.Type type, Node.Ext ext) {
        CorporationPower power = queryPower(corporationId, powerCode);
        Node node = Node.builder()
                .corporationPowerId(power.getId())
                .name(nodeName)
                .type(type)
                .order(order)
                .ext(ext)
                .build();
        return savaOrUpdateCorporationPowerNode(node);
    }

    public boolean addProcessorNode(String corporationId, String powerCode, String nodeName, int order, Node.Ext ext) {
        return addNode(corporationId, powerCode, nodeName, order, Node.Type.PROCESSOR, ext);
    }

    public boolean addResponseNode(String corporationId, String powerCode, String nodeName, int order, boolean custom, MediaType mediaType, String expression, Node.Ext ext) {
        CorporationPower power = queryPower(corporationId, powerCode);
        Node node = Node.builder()
                .corporationPowerId(power.getId())
                .name(nodeName)
                .type(Node.Type.RESPONSE)
                .customResponse(custom)
                .mediaType(mediaType.toString())
                .expression(expression)
                .order(order)
                .ext(ext)
                .build();
        return savaOrUpdateCorporationPowerNode(node);
    }


    @Override
    public boolean joinCorporation(String userId, String corporationId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.where(Tables.USER_CORPORATION.CORPORATION_ID.eq(corporationId))
                .and(Tables.USER_CORPORATION.USER_ID.eq(userId));
        UserCorporation query = userCorporationMapper.selectOneByQuery(queryWrapper);
        if (Objects.nonNull(query)) {
            return false;
        } else {
            return userCorporationMapper.insert(UserCorporation.builder()
                    .userId(userId)
                    .corporationId(corporationId)
                    .build()) > 0;
        }
    }

    @SneakyThrows
    @Override
    public List<Field> convertJson2Fields(String json) {
        List<Field> fields = new ArrayList<>();
        ObjectNode node = objectMapper.readValue(json, ObjectNode.class);
        node2Field(node, fields);
        return fields;
    }

    @SneakyThrows
    @Override
    public List<Field> convertJson2FormFields(String json) {
        List<Field> fields = new ArrayList<>();
        List<Field> formFields = new ArrayList<>();
        ArrayNode node = objectMapper.readValue(json, ArrayNode.class);
        Field formField = Field.builder()
                .type(Field.Type.FORM)
                .label("form")
                .name("form")
                .remark("form")
                .expression("")
                .fields(formFields)
                .build();
        forEach(node, (k, v) -> {
            List<Field> sFields = new ArrayList<>();
            node2Field((ObjectNode) v, sFields, true);
            Field sField = Field.builder().type(Field.Type.FORMVALUE).fields(sFields).build();
            formFields.add(sField);
        });
        fields.add(formField);
        return fields;
    }

    @SneakyThrows
    public List<Field> convertJson2FormFields(String json, String nameField, Function<Field, Boolean> filter) {
        List<Field> fields = new ArrayList<>();
        List<Field> formFields = new ArrayList<>();
        ArrayNode node = objectMapper.readValue(json, ArrayNode.class);
        Field formField = Field.builder()
                .type(Field.Type.FORM)
                .label("form")
                .name("form")
                .remark("form")
                .expression("")
                .fields(formFields)
                .build();
        forEach(node, (k, v) -> {
            List<Field> sFields = new ArrayList<>();
            node2Field((ObjectNode) v, sFields, true);
            String name = "";
            for (Field s : sFields) {
                if (s.getName().equals(nameField)) {
                    name = ((JsonNode) s.getDefaultValue()).asText();
                }
            }
            if (Objects.nonNull(filter)) {
                sFields = sFields.stream().filter(filter::apply).collect(Collectors.toList());
            }
            Field sField = Field.builder().name(name).type(Field.Type.FORMVALUE).fields(sFields).build();

            formFields.add(sField);
        });
        fields.add(formField);
        return fields;
    }

    private void node2Field(ObjectNode node, List<Field> fields) {
        node2Field(node, fields, false);
    }

    private void node2Field(ObjectNode node, List<Field> fields, boolean useValue) {
        forEach(node, (k, v) -> {
            String key = (String) k;
            Field field = Field.builder()
                    .label(key)
                    .name(key)
                    .remark(key)
                    .expression("")
                    .defaultValue(useValue ? v : null)
                    .build();
            if (v.isValueNode()) {
                if (v.isTextual()) {
                    field.setType(Field.Type.STRING);
                    fields.add(field);
                } else if (v.isInt() || v.isBigInteger()) {
                    field.setType(Field.Type.INT);
                    fields.add(field);
                } else if (v.isLong()) {
                    field.setType(Field.Type.LONG);
                    fields.add(field);
                } else if (v.isBoolean()) {
                    field.setType(Field.Type.BOOLEAN);
                    fields.add(field);
                } else if (v.isDouble() || v.isBigDecimal()) {
                    field.setType(Field.Type.DECIMAL);
                    fields.add(field);
                }
            } else if (v.isObject()) {
                List<Field> fieldList = new ArrayList<>();
                node2Field((ObjectNode) v, fieldList);
                field.setType(Field.Type.REF);
                field.setFields(fieldList);
                fields.add(field);
            } else if (v.isArray()) {
                array2Field(key, (ArrayNode) v, fields);
            }
        });
    }

    private void array2Field(String key, ArrayNode node, List<Field> fields) {
        Field field = Field.builder().label(key).name(key).remark(key).build();
        AtomicBoolean isBaseType = new AtomicBoolean(true);
        forEach(node, (k, v) -> {
            if (v.isObject() || v.isArray()) {
                isBaseType.set(false);
            }
        });
        if (isBaseType.get()) {
            field.setType(Field.Type.LIST);
            fields.add(field);
        } else {
            List<Field> fieldList = new ArrayList<>();
            node2Field((ObjectNode) node.get(0), fieldList);
            field.setType(Field.Type.FOREACH);
            field.setFields(fieldList);
            fields.add(field);
        }
    }

    public void forEach(JsonNode node, BiConsumer<Object, JsonNode> consumer) {
        if (node.isArray()) {
            int size = node.size();
            for (int m = 0; m < size; m++) {
                consumer.accept(m, node.get(m));
            }
        } else if (node.isObject()) {
            Iterator<Map.Entry<String, JsonNode>> it = node.fields();
            while (it.hasNext()) {
                Map.Entry<String, JsonNode> entry = it.next();
                consumer.accept(entry.getKey(), entry.getValue());
            }
        }
    }


    @Override
    public NodeContext context(String corporationPowerId, boolean callback, String platform, JsonNode httpContext, Message.Source source) {
        try {
            saveBusinessLog(MdcUtil.get(), corporationPowerId, "WEBHOOK", "初始化Context", httpContext);
            CorporationPower corporationPower = corporationPowerMapper.selectOneById(corporationPowerId);
            Assert.notNull(corporationPower, "功能【{}】未开通", corporationPowerId);
            Assert.isTrue(corporationPower.getExpirateDate()
                    .getTime() > System.currentTimeMillis(), "功能【{}】已过期：{}", corporationPowerId, corporationPower.getExpirateDate());
            QueryWrapper query = new QueryWrapper();
            query.where(Tables.NODE.CORPORATION_POWER_ID.eq(corporationPowerId)).orderBy(Tables.NODE.ORDER, true);
            List<Node> powerConfigs = configMapper.selectListByQuery(query);
            Assert.isTrue(CollUtil.isNotEmpty(powerConfigs), "功能【{}】无对应配置", corporationPowerId);
            ObjectNode corporationConfig = corporationPower.getExt().getConfig();
            String appEnv = Optional.ofNullable(System.getenv("appEnv")).orElse(System.getProperty("appEnv"));
            corporationConfig = (ObjectNode) corporationConfig.get(appEnv);

            NodeContext context = NodeContext.builder()
                    .configs(powerConfigs)
                    .corporation(JSONUtil.parse(objectMapper.writeValueAsString(corporationConfig)))
                    .build();
            context.setCorporationPowerId(corporationPowerId);
            context.setCorporationId(corporationPower.getCorporationId());
            context.setCallback(callback);
            context.setPlatform(platform);
            context.setSource(source);

            if (callback) {
                AbstractPlatformApi service = SpringUtil.getBean(platform + "Api", AbstractPlatformApi.class);
                service.setCorporationPowerId(context.getCorporationPowerId());
                service.setCorporationId(context.getCorporationId());
                service.setHttpContext(context.getHttpContext());
                service.setCorporation(context.getCorporation());
                service.setPlatform(context.getPlatform());
                service.setSource(context.getSource());

                CallbackResponse callbackResponse = service.callback(JSONUtil.parse(objectMapper.writeValueAsString(httpContext)));
                context.setCallbackResponse(callbackResponse);
                context.getCache()
                        .put("callback", JSONUtil.parse(objectMapper.writeValueAsString(callbackResponse.getResponse())));
            }
            context.getCache().put("corporation", corporationConfig);
            context.getCache().put("corporationPowerId", corporationPowerId);
            context.getCache().put("corporationId", corporationPower.getCorporationId());
            context.getCache().put("traceId", MdcUtil.get());
            context.getCache().put("data", httpContext.path("_webhookBody"));
            context.getCache().put("_webhookGet", httpContext.path("_webhookGet"));
            context.getCache().put("_webhookHeader", httpContext.path("_webhookHeader"));
            Message message = Message.builder()
                    .corporationId(corporationPower.getCorporationId())
                    .corporationPowerId(corporationPowerId)
                    .action(NONE)
                    .traceId(MdcUtil.get())
                    .status(Message.Status.PROCESSOR)
                    .source(source)
                    .platform(platform)
                    .ext(Message.Ext.builder().data(httpContext).build())
                    .build();
            context.getCache().put("_messageBean", JSONUtil.parse(objectMapper.writeValueAsString(message)));
            context.getCache().put("_messageAction", httpContext.path("_messageAction"));
            context.getCache().put("_messagePowerCode", httpContext.path("_messagePowerCode"));
            context.setHttpContext(httpContext);
            context.setCacheJson(JSONUtil.parse(objectMapper.writeValueAsString(context.getCache())));

            NodeExecutor nodeExecutor = SpringUtil.getBean(NodeExecutor.class);
            nodeExecutor.setContext(context);

            NodeRender nodeRender = SpringUtil.getBean(NodeRender.class);
            nodeRender.setContext(context);

            nodeExecutor.setNodeRender(nodeRender);
            context.setFilter(nodeExecutor.flows());

            saveBusinessLog(MdcUtil.get(), corporationPowerId, "WEBHOOK", "初始化Context:完成", context);
            return context;
        } catch (Exception e) {
            saveBusinessLog(MdcUtil.get(), corporationPowerId, "WEBHOOK", CharSequenceUtil.format("初始化Context:执行异常"), ExceptionUtil.stacktraceToString(e));
            throw new RuntimeException(e.getMessage());
        }
    }

    public void clear(Object object) {
        if (object instanceof NodeContext) {
            NodeContext context = (NodeContext) object;
            context.getCache().clear();
            context.setCorporation(null);
            context.setCacheJson(null);
            context.setCallbackResponse(null);
            context = null;
        }
    }

    @SneakyThrows
    public ResponseBean response(NodeContext context) {
        if (context.isFilter()) {
            return ResponseBean.ok("请求过滤");
        }
        if (!context.isAsync()) {
            Object object;
            if (CharSequenceUtil.isNotEmpty(context.getResponsePath())) {
                if (context.getResponsePath().startsWith(JSON_PATH_SUB)) {
                    object = context.getCacheJson().getByPath(context.getResponsePath());
                } else {
                    object = context.getCache().get(context.getResponsePath());
                }
                if (context.isForwarding()) {
                    return ResponseBean.forwarding((String) object);
                } else {
                    ResponseBean bean = ResponseBean.of(object);
                    bean.setCustomResponse(context.isCustomResponse());
                    bean.setMediaType(context.getMediaType());
                    return bean;
                }
            }
        }
        return ResponseBean.ok();
    }

}
