package com.simple.framework.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.simple.framework.beans.Function2;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class PageRestRequest extends RestRequest {
    private Integer pageIndex;
    private int pageLength;
    private Integer pageCursor;
    private int max;
    private Object sourceTime;
    private Consumer<PageRestRequest> each;
    private Function2<PageRestRequest, JsonNode, List<JsonNode>> pageResultHandler;
    private Function2<JsonNode, PageRestRequest, Boolean> breakCondition;
    private BiConsumer<PageRestRequest, JsonNode> cursorCondition;
}
