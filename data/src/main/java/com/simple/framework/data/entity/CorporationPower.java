package com.simple.framework.data.entity;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Unique;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Data
@SuperBuilder
@Table(value = "corporation_power")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("租户功能表")
public class CorporationPower extends BaseEntity<CorporationPower.Ext> {

    @Column
    @Comment("租户ID")
    @Unique
    private String corporationId;

    @Column
    @Comment("功能ID")
    @Unique
    private String powerId;

    @Column
    @Comment("负责人ID")
    private String ownerId;

    @Column(onInsertValue = "now()")
    @Comment("开通时间")
    private Date openTime;

    @Column
    @Comment("过期时间")
    private Date expirateDate;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {

        @Comment("功能全局字段值")
        private ObjectNode config;

    }
}
