package com.simple.framework.data.gen;

import lombok.Getter;

@Getter
public class CompareNode {

    /**
     * 字段
     */
    private String fieldKey;

    /**
     * 字段值
     */
    private Object fieldValue;

    /**
     * 字段名称
     */
    private String fieldName;

    public CompareNode setFieldKey(String fieldKey) {
        this.fieldKey = fieldKey;
        return this;
    }

    public CompareNode setFieldValue(Object fieldValue) {
        this.fieldValue = fieldValue;
        return this;
    }

    public CompareNode setFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }
}