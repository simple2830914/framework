package com.simple.framework.data.entity;

import com.fasterxml.jackson.databind.JsonNode;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.EnumValue;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@Table(value = "message")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("消息记录表")
public class Message extends BaseEntity<Message.Ext> {

    @Column
    @Comment("企业功能Id")
    private String corporationPowerId;

    @Column
    @Comment("租户ID")
    private String corporationId;

    @Column
    @Comment("traceId")
    private String traceId;

    @Column
    @Comment("来源id")
    private String sourceId;

    @Column
    @Comment("来源类型")
    private Source source;

    @Column
    @Comment("平台标识")
    private String platform;

    @Column
    @Comment("消息动作类型")
    private Action action;

    @Column
    @Comment("处理状态")
    private Status status;

    @Getter
    public enum Action {
        NONE("none", "无"), ADD("add", "新增"), UPDATE("update", "更新"), DELETE("delete", "删除"), ENABLE("enable", "启用"), DISABLE("disable", "禁用");
        @EnumValue
        private final String name;
        private final String remark;

        Action(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }

    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {
        private JsonNode data;
        private JsonNode result;
    }

    @Getter
    public enum Status {
        NONE("none", "无需处理"),
        PROCESSOR("processor", "处理中"),
        DONE("done", "处理完成"),
        ERROR("error", "处理异常"),
        FAIL("fail", "处理失败");
        @EnumValue
        private final String name;
        private final String remark;

        Status(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }

    }

    @Getter
    public enum Source {
        WEBHOOK("webhook", "请求"), CALLBACK("callback", "回调"), MANUAL("manual", "手工处理"), JOB("job", "定时任务");
        @EnumValue
        private final String name;
        private final String remark;

        Source(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }

    }
}
