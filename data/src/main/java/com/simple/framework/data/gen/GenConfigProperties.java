package com.simple.framework.data.gen;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.typesafe.config.ConfigObject;

public class GenConfigProperties {
    @Inject
    @Named("data.gen")
    ConfigObject genConfig;
    @Inject
    @Named("data.gen.database")
    String database;
    @Inject
    @Named("data.gen.template.createTable")
    String createTableTemplate;
    @Inject
    @Named("data.gen.template.createDataBase")
    String createDataBaseTemplate;
    @Inject
    @Named("data.gen.template.queryAllTables")
    String queryAllTablesTemplate;
    @Inject
    @Named("data.gen.template.queryAllFields")
    String queryAllFieldsTemplate;
    @Inject
    @Named("data.gen.template.queryAllIndexes")
    String queryAllIndexesTemplate;

    @Inject
    @Named("data.gen.template.modifyComment")
    String modifyComment;

    @Inject
    @Named("data.gen.template.modifyField")
    String modifyField;

    @Inject
    @Named("data.gen.template.addField")
    String addField;

    @Inject
    @Named("data.gen.template.createIndex")
    String createIndex;

    @Inject
    @Named("data.gen.prop.unique")
    String uniqueIndex;
    @Inject
    @Named("data.gen.prop.primary")
    String primaryProp;
    @Inject
    @Named("data.gen.prop.notNull")
    String notNullProp;
    @Inject
    @Named("data.gen.prop.default")
    String defaultProp;
    @Inject
    @Named("data.gen.prop.generatedColumn")
    String generatedColumnProp;
}
