package com.simple.framework.data.service;

import cn.hutool.json.JSON;
import com.fasterxml.jackson.databind.JsonNode;
import com.simple.framework.data.CallbackResponse;
import com.simple.framework.data.entity.Message;
import com.simple.framework.data.entity.MetaConfig;
import com.simple.framework.data.entity.Node;
import com.simple.framework.data.entity.Render;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Builder
@Data
public class NodeContext {

    private String corporationPowerId;

    private String corporationId;

    private JsonNode httpContext;

    private List<Node> configs;

    @Builder.Default
    private Map<String, MetaConfig> metaConfigMap = new ConcurrentHashMap<>();

    private JSON corporation;

    private boolean filter;

    @Builder.Default
    private Map<String, List<Render>> configRender = new ConcurrentHashMap<>();

    @Builder.Default
    private Map<String, Object> cache = new ConcurrentHashMap<>();

    private JSON cacheJson;

    @Builder.Default
    private boolean isForwarding = false;

    @Builder.Default
    private boolean isCustomResponse = false;

    private String mediaType;

    @Builder.Default
    private boolean isAsync = false;

    private String responsePath;

    private boolean callback;

    private String platform;

    private CallbackResponse callbackResponse;

    private Message.Source source;


}
