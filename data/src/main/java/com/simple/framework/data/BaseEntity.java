package com.simple.framework.data;


import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.core.handler.JacksonTypeHandler;
import com.mybatisflex.core.keygen.KeyGenerators;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Default;
import com.simple.framework.annotation.NotNull;
import com.simple.framework.annotation.Primary;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.ibatis.type.JdbcType;

import java.util.Date;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class BaseEntity<T> {

    @Primary
    @NotNull
    @Comment("数据主键-flexId算法生成")
    @Id(keyType = KeyType.Generator, value = KeyGenerators.flexId)
    private String id;

    @NotNull
    @Default("0")
    @Column(version = true)
    @Comment("数据版本号")
    private Long version;

    @NotNull
    @Default("false")
    @Column(isLogicDelete = true, onInsertValue = "false")
    @Comment("逻辑删除标识")
    private Boolean isDelete;

    @NotNull
    @Default("true")
    @Comment("启用标识")
    @Builder.Default
    private Boolean active=true;

    @NotNull
    @Default("now()")
    @Comment("数据入库时间")
    @Column(onInsertValue = "now()")
    private Date createTime;

    @NotNull
    @Default("now()")
    @Comment("数据最后更新时间")
    @Column(onInsertValue = "now()", onUpdateValue = "now()")
    private Date updateTime;

    @Column(jdbcType = JdbcType.JAVA_OBJECT, typeHandler = JacksonTypeHandler.class)
    @Comment("扩展信息")
    private T ext;
}
