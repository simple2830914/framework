package com.simple.framework.data.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.EnumValue;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Unique;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@Table(value = "meta_config")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("元数据配置表")
public class MetaConfig extends BaseEntity<MetaConfig.Ext> {

    @Column
    @Comment("企业Id")
    @Unique
    private String corporationId;

    @Column
    @Comment("元数据名称")
    @Unique
    private String entityName;

    @Column
    @Comment("元数据类型")
    private Type type;


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {
        @Comment("字段配置")
        private List<Field> fields;
        @Comment("数据请求的最大次数")
        private int max;
        @Comment("数据请求翻页的大小")
        private int pageLength;
        @Comment("当前页数")
        private Integer pageIndex;
        @Comment("翻页开始位置")
        private Integer pageCursor;
        @Comment("sourceTime是否有效")
        private boolean md5WithSourceTime;
        @Comment("最大更新时间")
        private Object maxSourceTime;
        @Comment("最大更新时间类型")
        private String sourceTimeType;
        @Comment("是否全量下载")
        private boolean fullDownload;
        @Comment("sourceTime接口传值表达式")
        private String sourceTimeExpress;
        @Comment("是否开启入库事件")
        private boolean eventOpen;
    }


    @Getter
    public enum Type {
        BASE("base", "基础数据"), BUSINESS("business", "业务数据");
        @EnumValue
        private final String name;
        private final String remark;

        Type(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }

    }
}
