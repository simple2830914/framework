package com.simple.framework.data.entity;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.data.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@Table(value = "menu")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("菜单信息表")
public class Menu extends BaseEntity<ObjectNode> {

    @Column
    @Comment("菜单名称")
    private String name;

    @Column
    @Comment("菜单图标")
    private String icon;

    @Column
    @Comment("父级菜单ID")
    private String parentId;

}