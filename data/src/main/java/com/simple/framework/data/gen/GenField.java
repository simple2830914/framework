package com.simple.framework.data.gen;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GenField {
    private String tableName;
    private String name;
    private String type;
    private String comment;
    private String props;
}