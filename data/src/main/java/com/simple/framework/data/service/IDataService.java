package com.simple.framework.data.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.simple.framework.beans.ResponseBean;
import com.simple.framework.data.entity.*;
import jakarta.ws.rs.core.MediaType;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

public interface IDataService {

    boolean saveSqlLog(String traceId, String sourceSql, String targetSql, String schema, long executeTime);

    boolean saveSqlLog(String traceId, String sql, String schema, long executeTime);

    boolean saveHttpLog(String traceId, String url, String method, Object params, String msg, Object response);

    boolean saveHttpLog(String traceId, CommonLog.Http http);

    boolean saveFileLog(String traceId, CommonLog.File file);

    boolean saveJobLog(String traceId, CommonLog.Job job);

    boolean saveJobLog(String traceId, String jobName, String powerName, String powerCode, String corporationPowerId, String msg);

    boolean saveJobLog(String traceId, String jobName, String corporationPowerId, String dataId, String msg);

    boolean saveBusinessLog(String traceId, String businessId, String businessName, String stepRemark, Object context);

    void saveMessage(Message message);

    void errorMessage(Message bean);

    boolean saveCorporation(Corporation corporation);

    boolean insertOrUpdateCorporation(Corporation corporation);

    boolean saveUser(User user);

    boolean savePower(Power power);

    boolean saveOrUpdatePower(Power power);

    void openDataPower(String powerName, String powerCode, String corporationId, String ownerId, int days, ObjectNode config);

    void openPower(String powerName, String powerCode, String corporationId, String ownerId, int days, ObjectNode config);

    Power queryPower(String powerCode);

    Power queryDataPower(String powerCode);

    List<CorporationPower> queryCorporationPower(String corporationId, Set<String> powerIds);

    void disableMetaByConfigId(String corporationId, String configId);

    CorporationPower queryPower(String corporationId, String powerCode);

    boolean saveMenu(Menu menu);

    boolean saveOrUpdateMetaConfig(MetaConfig metaConfig);

    boolean saveOrUpdateMeta(Meta meta);

    boolean openMeta(String corporationId, String metaName, MetaConfig.Ext config);

    boolean openMeta(String corporationId, String metaName);

    boolean openCorporationPower(CorporationPower corporationPower);

    boolean saveOrUpdateCorporationPower(CorporationPower corporationPower);


    boolean savaCorporationPowerNode(Node node);

    boolean savaOrUpdateCorporationPowerNode(Node node);

    boolean addNode(String corporationId, String powerCode, String nodeName, int order, Node.Type type, Node.Ext ext);

    boolean addProcessorNode(String corporationId, String powerCode, String nodeName, int order, Node.Ext ext);

    boolean addResponseNode(String corporationId, String powerCode, String nodeName, int order, boolean custom, MediaType mediaType, String expression, Node.Ext ext);

    boolean joinCorporation(String userId, String corporationId);

    List<Field> convertJson2Fields(String json);

    List<Field> convertJson2FormFields(String json);

    List<Field> convertJson2FormFields(String json, String nameField, Function<Field, Boolean> filter);

    NodeContext context(String corporationPowerId, boolean callback, String platform, JsonNode httpContext, Message.Source source);

    ResponseBean response(NodeContext context);

    void clear(Object object);

    Meta saveMeta(String corporationId, String entityName, String dataId, JsonNode source, JsonNode local);

    Meta saveOutMeta(String corporationId, String entityName, String outId, JsonNode callback, JsonNode local);

    boolean updateOutMeta(String id, String outId, JsonNode callback, JsonNode local, Meta.Status status);

    boolean updateMeta(String id, String outId, JsonNode source, JsonNode local, Meta.Status status);

    void metaFailById(String id, String msg);

    void metaNoneById(String id, String msg);

    void metaDoneById(String id, String msg);

    Meta selectMetaOne(String corporationId, String entityName, String dataId);

    List<Meta> selectMetaList(String corporationId, String entityName, String dataId);

    List<Meta> selectTodoMetaList(String corporationId, String metaName, String condition, int limit);

    List<Meta> selectMetaListByCondition(String corporationId, String metaName, String condition);

    Meta selectMetaOutOne(String corporationId, String entityName, String outId);

    MetaConfig selectMetaConfig(String corporationId, String entityName);

    List<Meta> selectMetaOutList(String corporationId, String entityName, String outId);
}
