package com.simple.framework.data;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.simple.framework.annotation.Starter;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.Application;
import com.simple.framework.boot.Order;
import com.simple.framework.boot.Worker;
import com.simple.framework.data.gen.GenService;
import com.simple.framework.data.gen.GenTable;
import lombok.extern.slf4j.Slf4j;

import java.util.List;


@Slf4j
@Starter("Gen启动器：对比库Entity的定义生成升级SQL")
@Order(Constants.FRAMEWORK_STARTER_ORDER_GEN)
public class GenStarter extends Application {

    @Inject
    GenService genService;

    @Inject
    Worker worker;

    @Inject
    @Named("data.gen.open")
    boolean open;

    @Override
    public void doStart() throws Exception {
        if (open) {
            List<GenTable> scanGenTable = genService.scanGenTable();
            List<GenTable> queryAllGenTable = genService.queryAllGenTable();
            List<String> stringList = genService.compare(scanGenTable, queryAllGenTable);
            for (String sql : stringList) {
                log.info("版本差异：\n{}", sql);
            }
        }
    }
}