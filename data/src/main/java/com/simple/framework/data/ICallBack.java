package com.simple.framework.data;

import cn.hutool.json.JSON;

import java.util.Map;

public interface ICallBack {

    default Object callbackReturn(CallbackResponse callbackResponse, Map<String, Object> cache) {
        return callbackResponse.isStatus() ? callbackResponse.getSuccessHandler()
                .apply(cache) : callbackResponse.getFailedHandler().apply(cache);
    }

    CallbackResponse callback(JSON params);
}
