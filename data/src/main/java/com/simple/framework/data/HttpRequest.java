package com.simple.framework.data;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import javax.net.ssl.HttpsURLConnection;
import java.util.function.BiConsumer;
import java.util.function.Function;

@Data
@SuperBuilder(toBuilder = true)
@Accessors(chain = true)
public class HttpRequest {
    private String api;
    private String host;
    private MultivaluedMap<String, Object> queryParam;
    private MultivaluedMap<String, Object> headers;
    private String method;
    private String entity;
    private BiConsumer<Response, Integer> statusHandler;
    private Function<Response, JsonNode> responseHandler;
    private Function<HttpsURLConnection, HttpsURLConnection> clientHandler;
}
