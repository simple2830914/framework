package com.simple.framework.data.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.EnumValue;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.Default;
import com.simple.framework.annotation.Length;
import com.simple.framework.annotation.Unique;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Map;

@Data
@SuperBuilder
@Table(value = "node")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("企业功能节点表")
public class Node extends BaseEntity<Node.Ext> {

    @Column
    @Comment("企业功能Id")
    @Unique
    private String corporationPowerId;

    @Column
    @Comment("节点名称")
    @Unique
    @Length("20")
    private String name;

    @Column
    @Comment("节点描述")
    @Unique
    private String remark;

    @Column
    @Comment("节点排序")
    private Integer order;

    @Column
    @Comment("异步处理标识")
    @Default("false")
    private Boolean async;

    @Column
    @Comment("是否包装返回类")
    @Default("false")
    private Boolean customResponse;

    @Column
    @Comment("自定义方法的格式类型")
    private String mediaType;

    @Column
    @Comment("节点对应类型")
    private Type type;

    @Column
    @Comment("返回值取值表达式")
    private String expression;

    @Getter
    public enum Type {
        PROCESSOR("processor", "流程中节点"), RESPONSE("response", "接口返回"), FORWARDING("forwarding", "地址跳转");
        @EnumValue
        private final String name;
        private final String remark;

        Type(String name, String remark) {
            this.name = name;
            this.remark = remark;
        }

    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {

        @Comment("开启消息队列")
        private boolean openMessage;

        @Comment("rule校验异常是否入库")
        private boolean messageFilterRule;

        @Comment("meta自动映射入库")
        private Map<String, List<Field>> meta;

        @Comment("执行step0:添加执行检查规则")
        private List<Rule> rules;

        @Comment("执行step1:声明变量")
        private Map<String, Object> props;

        @Comment("执行step2:用取值表达式构建辅助字段或者执行Fields准备动作")
        private Map<Integer, Render> before;

        @Comment("执行step3:用取值表达式进行配置字段渲染")
        private List<Field> fields;

        @Comment("执行step4:字段映射完成后的业务动作")
        private Map<Integer, Render> after;

        @Comment("执行step5:监听错误事件执行")
        private Map<Integer, Render> error;

    }

}
