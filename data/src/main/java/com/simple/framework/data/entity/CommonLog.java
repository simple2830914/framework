package com.simple.framework.data.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.simple.framework.annotation.Comment;
import com.simple.framework.annotation.GeneratedAble;
import com.simple.framework.annotation.GeneratedColumn;
import com.simple.framework.annotation.Length;
import com.simple.framework.data.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@Table(value = "common_log")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Comment("日志表")
public class CommonLog extends BaseEntity<CommonLog.Ext> {

    public enum Type {
        SQL, HTTP, BUSINESS, FILE, JOB
    }

    @Column
    @Comment("traceId")
    private String traceId;

    @Column
    @Comment("日志类型")
    private Type type;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Sql {
        private String sourceSQL;
        private String targetSQL;
        @GeneratedColumn
        @Column
        @Comment("计划执行Schema")
        private String schemaName;
        private long executeTime;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Business {
        @GeneratedColumn
        @Column
        @Comment("业务名称")
        private String businessName;
        @GeneratedColumn
        @Column
        @Comment("业务ID")
        private String businessId;
        @GeneratedColumn
        @Column
        @Comment("业务Step名称")
        @Length("500")
        private String businessStep;
        private Object context;

    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Http {
        private String url;
        private String method;
        private Object params;
        private Object response;
        private String msg;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class File {
        private String workerName;
        private String fileId;
        private String url;
        private String fileType;
        private String savePath;
        private String fileName;
        private String msg;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Job {
        @GeneratedColumn
        @Column
        @Comment("任务名称")
        private String jobName;
        @GeneratedColumn
        @Column
        @Comment("任务执行的数据Id")
        private String dataId;
        @GeneratedColumn
        @Column
        @Comment("任务执行的功能Id")
        private String corporationPowerId;
        private Object context;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ext {
        @GeneratedAble
        Sql sql;

        @GeneratedAble
        Business business;

        @GeneratedAble
        Http http;

        @GeneratedAble
        File file;

        @GeneratedAble
        Job job;
    }
}
