package com.simple.framework.data.service;

import cn.hutool.core.lang.Pair;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrSplitter;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simple.framework.beans.MdcUtil;
import com.simple.framework.data.AbstractPlatformApi;
import com.simple.framework.data.DataKit;
import com.simple.framework.data.Platform;
import com.simple.framework.data.entity.Field;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Scope("prototype")
public class NodeRender {

    @Autowired
    DataService dataService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ScriptEngine scriptEngine;

    @Autowired
    ObjectHandlerFactory objectHandlerFactory;

    @Getter
    @Setter
    NodeContext context;
    public static final String FUNCTION_PARAMS_REGEX = "\\{\\{(\\$.[^\\\"\\'\\s\\{\\}]*)\\}\\}";
    public static final String JSON_PATH_SUB = "$.";
    public static final String CLASS_METHOD_REGEX = "##(\\w+)\\(([^\\n\\r\\(\\)]*)\\)";

    public AbstractPlatformApi getService(Platform platform) {
        return getService(platform.getName());
    }

    public AbstractPlatformApi getService(String platform) {
        AbstractPlatformApi service = SpringUtil.getBean(platform.toLowerCase() + "Api", AbstractPlatformApi.class);
        if (platform.equalsIgnoreCase("JAVA") || platform.equalsIgnoreCase("CHECK")) {
            return service;
        }
        service.setCorporationPowerId(context.getCorporationPowerId());
        service.setCorporationId(context.getCorporationId());
        service.setHttpContext(context.getHttpContext());
        service.setCorporation(context.getCorporation());
        service.setPlatform(context.getPlatform());
        service.setSource(context.getSource());
        return service;
    }


    @SneakyThrows
    public Object executeParams(String expression) {
        return executeParams(expression, context.getCacheJson());
    }

    @SneakyThrows
    public Object executeJava(String expression, AbstractPlatformApi service, boolean saveLog) {
        return executeJava(expression, context.getCacheJson(), service, saveLog);
    }


    public @NonNull Map<String, Object> renderField(List<Field> fields, JSON cache, boolean saveLog) {
        Map<String, Object> object = new HashMap<>();
        for (Field field : fields) {
            try {
                renderField(object, field, cache, saveLog && !field.isCloseLog());
            } catch (Exception e) {
                dataService.saveBusinessLog(MdcUtil.get(), context.getCorporationPowerId(), "WEBHOOK", CharSequenceUtil.format("Field渲染:【{}】执行异常 - expression【{}】", field.getName(), field.getExpression()), e.getMessage());
                throw new RuntimeException(CharSequenceUtil.format("field({})渲染expression[{}]出错 - {} ", field.getName(), field.getExpression(), e.getMessage()));
            }
        }
        return object;
    }

    public boolean renderFieldCondition(Field field, JSON cache, boolean saveLog) {
        if (CharSequenceUtil.isNotEmpty(field.getCondition())) {
            Object condition = renderCondition(field.getCondition(), cache, Platform.CHECK, saveLog);
            if (!(condition instanceof Boolean)) {
                return false;
            }
            return (boolean) condition;
        }
        return true;
    }

    @SneakyThrows
    private void renderField(Map<String, Object> object, Field field, JSON cache, boolean saveLog) {
        if (!renderFieldCondition(field, cache, saveLog)) {
            return;
        }
        Object value = null;
        if (CharSequenceUtil.isNotEmpty(field.getExpression())) {
            AbstractPlatformApi service = getService(Platform.JAVA);
            if (CharSequenceUtil.isNotEmpty(field.getPlatform())) {
                service = getService(field.getPlatform());
            }
            value = renderExpression(field.getExpression(), cache, service, saveLog);
        }
        value = Objects.isNull(value) ? field.getDefaultValue() : value;

        switch (field.getType()) {
            case STRING:
                if (Objects.nonNull(value) && !(value instanceof String)) {
                    value = objectMapper.writeValueAsString(value);
                }
                object.put(field.getName(), value);
                break;
            case JSON:
                if (Objects.nonNull(value)) {
                    value = JSONUtil.parse(value);
                }
                object.put(field.getName(), value);
                break;
            case BOOLEAN:
                try {
                    if (Objects.nonNull(value) && !(value instanceof Boolean)) {
                        value = Boolean.valueOf(String.valueOf(value));
                    }
                } catch (Exception e) {
                    value = null;
                }
                object.put(field.getName(), value);
                break;
            case INT:
                try {
                    if (Objects.nonNull(value) && !(value instanceof Integer)) {
                        value = Integer.valueOf(String.valueOf(value));
                    }
                } catch (Exception e) {
                    value = null;
                }
                object.put(field.getName(), value);
                break;
            case LONG:
                try {
                    if (Objects.nonNull(value) && !(value instanceof Long)) {
                        value = Long.valueOf(String.valueOf(value));
                    }
                } catch (Exception e) {
                    value = null;
                }
                object.put(field.getName(), value);
                break;
            case DECIMAL:
                try {
                    if (Objects.nonNull(value) && !(value instanceof BigDecimal)) {
                        value = new BigDecimal(String.valueOf(value));
                    }
                } catch (Exception e) {
                    value = null;
                }
                object.put(field.getName(), value);
                break;
            case LIST:
                if (Objects.nonNull(value)) {
                    if (value instanceof List) {
                        object.put(field.getName(), value);
                    } else {
                        object.put(field.getName(), List.of(value));
                    }
                }
                break;
            case REF:
                object.put(field.getName(), renderField(field.getFields(), cache, saveLog));
                break;
            case FORM:
                List<Object> formValue = new ArrayList<>();
                for (Field formField : field.getFields()) {
                    if (renderFieldCondition(formField, cache, saveLog)) {
                        if (formField.getType() == Field.Type.FORMVALUE) {
                            formValue.add(renderField(formField.getFields(), cache, saveLog));
                        } else {
                            Map<String, Object> sObject = new HashMap<>();
                            renderField(sObject, formField, cache, saveLog && !formField.isCloseLog());
                            formValue.add(sObject);
                        }
                    }
                }
                object.put(field.getName(), formValue);
                break;
            case LISTOBJECT: {
                List<Map<String, Object>> listValue = new ArrayList<>();
                listValue.add(renderField(field.getFields(), cache, saveLog));
                object.put(field.getName(), listValue);
                break;
            }
            case FOREACH:
                List<Object> list = (List<Object>) value;
                List<Map<String, Object>> listValue = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    JSONObject index = new JSONObject();
                    index.set("currentIndex", i);
                    index.set("currentValue", list.get(i));
                    index.set("super", ((JSONObject) cache).clone());
                    listValue.add(renderField(field.getFields(), index, saveLog));
                    index.clear();
                }
                object.put(field.getName(), listValue);
                break;
            default:
                break;
        }
    }


    @SneakyThrows
    public Object executeJava(String expression, JSON cache, AbstractPlatformApi service, boolean saveLog) {
        Matcher matcher = isContain(expression, CLASS_METHOD_REGEX).getValue();
        String functionName = matcher.group(1);
        String paramName = matcher.group(2);
        return executeJava(paramName, functionName, cache, service, saveLog);
    }

    @SneakyThrows
    public Object executeJava(String paramName, String functionName, AbstractPlatformApi service, boolean saveLog) {
        return executeJava(paramName, functionName, context.getCacheJson(), service, saveLog);
    }

    @SneakyThrows
    public Object executeJava(String paramName, String functionName, JSON cache, AbstractPlatformApi service, boolean saveLog) {
        List<Object> params = getParams(paramName, cache);
        if (saveLog) {
            dataService.saveBusinessLog(MdcUtil.get(), context.getCorporationPowerId(), "executeJava", functionName, params);
        }
        Object object = DataKit.doMethod(service, functionName, params.toArray(new Object[]{}));
        if (Objects.nonNull(object)) {
            ObjectHandler handler = objectHandlerFactory.getHandler(object);
            if (Objects.nonNull(handler)) {
                object = handler.handle(object, this, service, functionName, params.toArray());
            }
            if (object instanceof JsonNode || object instanceof List || object instanceof Map) {
                object = JSONUtil.parse(objectMapper.writeValueAsString(object));
            }
        }
        return object;
    }

    public Object renderExpression(String expression, Platform platform, boolean saveLog) {
        return renderExpression(expression, getService(platform), saveLog);
    }

    @SneakyThrows
    public Object renderCondition(String condition, Platform platform, boolean saveLog) {
        return renderCondition(condition, context.getCacheJson(), platform, saveLog);
    }

    @SneakyThrows
    public Object renderCondition(String condition, JSON cache, Platform platform, boolean saveLog) {
        AbstractPlatformApi service = getService(platform);
        if (Boolean.TRUE.equals(isContain(condition, CLASS_METHOD_REGEX).getKey())) {
            if (isAllContain(matcher(condition, CLASS_METHOD_REGEX))) {
                return executeJava(condition, cache, service, saveLog);
            } else {
                Matcher matcher = matcher(condition, CLASS_METHOD_REGEX);
                StringBuilder sb = new StringBuilder();
                while (matcher.find()) {
                    String functionName = matcher.group(1);
                    String paramName = matcher.group(2);
                    Object object = executeJava(paramName, functionName, service, saveLog);
                    matcher.appendReplacement(sb, object.toString());
                }
                matcher.appendTail(sb);
                Invocable javaScriptEngine = (Invocable) scriptEngine;
                return javaScriptEngine.invokeFunction("eval", sb.toString());
            }
        }
        return condition;
    }

    @SneakyThrows
    public Object renderExpression(String expression, AbstractPlatformApi service, boolean saveLog) {
        if (expression.startsWith(JSON_PATH_SUB)) {
            return executeValue(expression);
        } else {
            if (isAllContain(matcher(expression, CLASS_METHOD_REGEX))) {
                return executeJava(expression, service, saveLog);
            } else if (Boolean.TRUE.equals(isContain(expression, FUNCTION_PARAMS_REGEX).getKey())) {
                return executeParams(expression);
            }
        }
        return expression;
    }

    @SneakyThrows
    private Object renderExpression(String expression, JSON cache, AbstractPlatformApi service, boolean saveLog) {
        if (expression.startsWith(JSON_PATH_SUB)) {
            return executeValue(expression, cache);
        } else {
            if (isAllContain(matcher(expression, CLASS_METHOD_REGEX))) {
                return executeJava(expression, cache, service, saveLog);
            } else if (Boolean.TRUE.equals(isContain(expression, FUNCTION_PARAMS_REGEX).getKey())) {
                return executeParams(expression, cache);
            }
        }
        return expression;
    }

    @SneakyThrows
    private List<Object> getParams(String paramName, JSON cache) {
        List<Object> params = new ArrayList<>();
        Invocable javaScriptEngine = (Invocable) scriptEngine;
        List<String> list = StrSplitter.split(paramName, ",", true, true);
        for (String item : list) {
            try {
                Object param = javaScriptEngine.invokeFunction("eval", item);
                if (param instanceof String) {
                    params.add(executeParams((String) param, cache));
                } else {
                    params.add(param);
                }
            } catch (Exception e) {
                params.add(item.replace("\\'", ""));
            }
        }
        return params;
    }

    public boolean isAllContain(Matcher matcher) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (matcher.find()) {
            matcher.appendReplacement(sb, "");
            i++;
        }
        matcher.appendTail(sb);
        return i == 1 && sb.toString().isEmpty();
    }

    public Pair<Boolean, Matcher> isContain(String template, String regex) {
        Matcher matcher = matcher(template, regex);
        return Pair.of(matcher.find(), matcher);
    }


    @SneakyThrows
    public Object executeParams(String expression, JSON cache) {
        Object param;
        if (isAllContain(matcher(expression, FUNCTION_PARAMS_REGEX))) {
            Matcher matcher = matcher(expression, FUNCTION_PARAMS_REGEX);
            matcher.find();
            String path = matcher.group(1);
            param = executeValue(path, cache);
        } else {
            Matcher matcher = matcher(expression, FUNCTION_PARAMS_REGEX);
            StringBuilder sb = new StringBuilder();
            while (matcher.find()) {
                String path = matcher.group(1);
                Object value = executeValue(path, cache);
                if (Objects.isNull(value)) {
                    value = "";
                }
                matcher.appendReplacement(sb, escapeExprSpecialWord(value.toString()));
            }
            matcher.appendTail(sb);
            param = sb.toString();
        }
        return param;
    }

    /**
     * 特殊字符处理
     *
     * @param keyword
     * @return
     */
    public static String escapeExprSpecialWord(String keyword) {
        if (CharSequenceUtil.isNotBlank(keyword)) {
            String[] fbsArr = {"$"};
            for (String key : fbsArr) {
                if (keyword.contains(key)) {
                    keyword = keyword.replace(key, "\\" + key);
                }
            }
        }
        return keyword;
    }

    public Matcher matcher(String template, String regex) {
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(template);
    }

    @SneakyThrows
    public Object executeValue(String expression) {
        return executeValue(expression, context.getCacheJson());
    }

    public Object executeValue(String expression, JSON cache) {
        return cache.getByPath(expression);
    }


}
