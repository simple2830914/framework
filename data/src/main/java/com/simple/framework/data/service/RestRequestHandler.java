package com.simple.framework.data.service;

import com.simple.framework.data.AbstractPlatformApi;
import com.simple.framework.data.RestRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestRequestHandler implements ObjectHandler {
    @Autowired
    RestService restService;

    @Override
    public Object handle(Object obj, NodeRender render, AbstractPlatformApi service, String methodName, Object[] params) {
        return restService.executeRest((RestRequest) obj, true);
    }
}
