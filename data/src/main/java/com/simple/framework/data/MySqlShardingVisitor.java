package com.simple.framework.data;

import cn.hutool.core.text.CharSequenceUtil;
import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.SQLObject;
import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.ast.statement.MySqlInsertStatement;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.alibaba.druid.sql.visitor.SQLEvalVisitorUtils;
import com.alibaba.druid.util.JdbcConstants;

import java.util.*;


public class MySqlShardingVisitor extends MySqlASTVisitorAdapter {

    public static final String ATTR_TABLE_SOURCE = "tableName";
    public static final String ATTR_ALIAS = "alias";

    private final String filterTable;
    private final String filterColumn;

    public MySqlShardingVisitor(String filterTable, String filterColumn) {
        this.filterTable = filterTable;
        this.filterColumn = filterColumn;
    }

    @Override
    public boolean visit(SQLExprTableSource x) {
        Map<String, SQLTableSource> aliasMap = getAliasMap(x);
        if (aliasMap.isEmpty()) {
            if (x.getAlias() != null) {
                aliasMap.put(x.getAlias(), x);
            }
            if (x.getExpr() instanceof SQLIdentifierExpr) {
                String tableName = ((SQLIdentifierExpr) x.getExpr()).getName();
                aliasMap.put(tableName, x);
            }
        }
        return super.visit(x);
    }

    @Override
    public boolean visit(SQLIdentifierExpr x) {
        SQLTableSource tableSource = getTableSource(x.getName(), x.getParent());
        if (tableSource != null) {
            x.putAttribute(ATTR_TABLE_SOURCE, tableSource);
        }
        return super.visit(x);
    }

    @Override
    public boolean visit(SQLBinaryOpExpr x) {
        String column = getColumn(x.getLeft());
        if (Objects.nonNull(column) && filterColumn.equals(column) && isValue(x.getRight())) {
            SQLTableSource tableSource = getBinaryOpExprLeftOrRightTableSource(x.getLeft());
            String tableName = getTableName(tableSource);
            if (Objects.nonNull(tableSource) && CharSequenceUtil.isNotEmpty(tableName) && filterTable.equals(tableName)) {
                Object value = SQLEvalVisitorUtils.eval(JdbcConstants.MYSQL, x.getRight(), new ArrayList<>());
                ((SQLExprTableSource) tableSource).setExpr(new SQLIdentifierExpr((String) value));
            }
        }
        return super.visit(x);
    }

    @Override
    public boolean visit(MySqlInsertStatement x) {
        String table = x.getTableName().getSimpleName();
        if (!filterTable.equals(table)) {
            return false;
        }
        int columnIndex = -1;
        for (int i = 0; i < x.getColumns().size(); ++i) {
            SQLExpr columnExpr = x.getColumns().get(i);
            if (columnExpr instanceof SQLIdentifierExpr) {
                String columnName = ((SQLIdentifierExpr) columnExpr).getName();
                if (filterColumn.equalsIgnoreCase(columnName)) {
                    columnIndex = i;
                    break;
                }
            }
        }
        if (columnIndex == -1) {
            return false;
        }
        SQLExpr valueExpr = x.getValues().getValues().get(columnIndex);
        Object value = SQLEvalVisitorUtils.eval(null, valueExpr, new ArrayList<>());
        x.setTableName(new SQLIdentifierExpr((String) value));
        return super.visit(x);
    }

    public static String getColumn(SQLExpr x) {
        if (x instanceof SQLPropertyExpr) {
            return ((SQLPropertyExpr) x).getName();
        }
        if (x instanceof SQLIdentifierExpr) {
            return ((SQLIdentifierExpr) x).getName();
        }
        return null;
    }

    public static boolean isValue(SQLExpr x) {
        return x instanceof SQLLiteralExpr || x instanceof SQLVariantRefExpr;
    }

    public static SQLTableSource getBinaryOpExprLeftOrRightTableSource(SQLExpr x) {
        SQLTableSource tableSource = (SQLTableSource) x.getAttribute(ATTR_TABLE_SOURCE);
        if (Objects.isNull(tableSource)) {
            tableSource = getDefaultTableSource(x.getParent());
        }
        if (tableSource instanceof SQLExprTableSource) {
            SQLExpr expr = ((SQLExprTableSource) tableSource).getExpr();
            if (expr instanceof SQLIdentifierExpr) {
                x.putAttribute(ATTR_TABLE_SOURCE, tableSource);
                return tableSource;
            }
        }
        return null;
    }

    public static Map<String, SQLTableSource> getAliasMap(SQLObject x) {
        if (x == null) {
            return Collections.emptyMap();
        }

        if (x instanceof SQLSelectQueryBlock || x instanceof SQLDeleteStatement) {
            Map<String, SQLTableSource> map = (Map<String, SQLTableSource>) x.getAttribute(ATTR_ALIAS);
            if (map == null) {
                map = new HashMap<>();
                x.putAttribute(ATTR_ALIAS, map);
            }
            return map;
        }
        return getAliasMap(x.getParent());
    }

    public static SQLTableSource getTableSource(String name, SQLObject parent) {
        Map<String, SQLTableSource> aliasMap = getAliasMap(parent);
        if (aliasMap == null) {
            return null;
        }
        SQLTableSource tableSource = aliasMap.get(name);
        if (tableSource != null) {
            return tableSource;
        }
        for (Map.Entry<String, SQLTableSource> entry : aliasMap.entrySet()) {
            if (name.equalsIgnoreCase(entry.getKey())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public static String getTableName(SQLTableSource tableSource) {
        if (Objects.nonNull(tableSource) && tableSource instanceof SQLExprTableSource) {
            SQLExpr expr = ((SQLExprTableSource) tableSource).getExpr();
            if (expr instanceof SQLIdentifierExpr) {
                return ((SQLIdentifierExpr) expr).getName();
            }
        }
        return null;
    }

    public static SQLTableSource getDefaultTableSource(SQLObject x) {
        if (x == null) {
            return null;
        }
        if (x instanceof SQLSelectQueryBlock) {
            return ((SQLSelectQueryBlock) x).getFrom();
        }

        if (x instanceof SQLDeleteStatement) {
            return ((SQLDeleteStatement) x).getTableSource();
        }

        if (x instanceof SQLUpdateStatement) {
            return ((SQLUpdateStatement) x).getTableSource();
        }
        return getDefaultTableSource(x.getParent());
    }
}
