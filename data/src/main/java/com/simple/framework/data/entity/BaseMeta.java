package com.simple.framework.data.entity;

import lombok.Data;

@Data
public class BaseMeta {
    private String sourceId;
    private String dataId;
    private String outId;
    private String targetId;
    private Object sourceTime;
    private boolean active;
}
