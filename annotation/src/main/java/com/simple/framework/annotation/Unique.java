package com.simple.framework.annotation;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.*;

@BindingAnnotation
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Unique {
}
