package com.simple.framework.annotation;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.*;

@BindingAnnotation
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Starter {
    String value() default "";

    boolean async() default true;
}
