package com.simple.framework.spring.context;

import com.google.common.base.Preconditions;
import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Names;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;

@Slf4j
public class SpringIntegration {
    private SpringIntegration() {
    }

    public static <T> Provider<T> fromSpring(Class<T> type, String name) {
        return new InjectableSpringProvider<>(type, name);
    }

    public static void bindAll(Binder binder, ListableBeanFactory beanFactory) {
        binder = binder.skipSources(SpringIntegration.class);
        String[] definitionNames = beanFactory.getBeanDefinitionNames();
        for (String name : definitionNames) {
            Class<?> type = beanFactory.getType(name);
            bindBean(binder, beanFactory, name, type);
        }

    }

    static <T> void bindBean(Binder binder, ListableBeanFactory beanFactory, String name, Class<T> type) {
        SpringProvider<T> provider = SpringProvider.newInstance(type, name);
        try {
            provider.initialize(beanFactory);
        } catch (Exception e) {
            binder.addError(e);
            return;
        }
        if (!AbstractModule.class.isAssignableFrom(type)) {
            log.debug("注入guice:{} - {}", name, type.getName());
            if (name.equals(type.getName())) {
                binder.bind(type).toProvider(provider);
            } else {
                binder.bind(type).annotatedWith(Names.named(name)).toProvider(provider);
            }
        }
    }

    static class InjectableSpringProvider<T> extends SpringProvider<T> {
        InjectableSpringProvider(Class<T> type, String name) {
            super(type, name);
        }

        @Override
        @Inject
        void initialize(BeanFactory beanFactory) {
            super.initialize(beanFactory);
        }
    }

    static class SpringProvider<T> implements Provider<T> {
        BeanFactory beanFactory;
        boolean singleton;
        final Class<T> type;
        final String name;
        volatile T instance;

        public SpringProvider(Class<T> type, String name) {
            this.type = Preconditions.checkNotNull(type, "type");
            this.name = Preconditions.checkNotNull(name, "name");
        }

        static <T> SpringProvider<T> newInstance(Class<T> type, String name) {
            return new SpringProvider<>(type, name);
        }

        void initialize(BeanFactory beanFactory) {
            this.beanFactory = beanFactory;
            if (!beanFactory.isTypeMatch(this.name, this.type)) {
                throw new ClassCastException("Spring bean named '" + this.name + "' does not implement " + this.type.getName() + ".");
            } else {
                this.singleton = beanFactory.isSingleton(this.name);
            }
        }

        public T get() {
            return this.singleton ? this.getSingleton() : this.type.cast(this.beanFactory.getBean(this.name));
        }

        private T getSingleton() {
            if (this.instance == null) {
                this.instance = this.type.cast(this.beanFactory.getBean(this.name));
            }

            return this.instance;
        }
    }
}
