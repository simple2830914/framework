package com.simple.framework.spring.context;

import cn.hutool.core.text.CharSequenceUtil;
import com.google.inject.Binding;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.simple.framework.boot.LauncherKit;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.util.*;

@Component
@Slf4j
public class Guice2SpringProcessor implements BeanDefinitionRegistryPostProcessor {

    Injector injector = LauncherKit.getInjector();

    List<String> packages = LauncherKit.getGlobalConfig().getStringList("spring.guiceBeans");

    @Override
    public void postProcessBeanDefinitionRegistry(@NonNull BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        guiceBeanToSpring(getGuiceBinding(injector), beanDefinitionRegistry);
    }


    @Override
    public void postProcessBeanFactory(@NonNull ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        guiceSingleToSpring(getGuiceBinding(injector), configurableListableBeanFactory);
    }

    public void guiceSingleToSpring(Map<Key<?>, Binding<?>> classList, ConfigurableListableBeanFactory configurableListableBeanFactory) {
        classList.forEach((key, value) -> {
            Provider<?> provider = value.getProvider();
            Annotation annotation = value.getKey().getAnnotation();
            String name;
            if (annotation instanceof Named) {
                name = ((Named) annotation).value();
                registerSingleton(name, provider.get(), configurableListableBeanFactory);
            }
        });
    }

    public void guiceBeanToSpring(Map<Key<?>, Binding<?>> classList, BeanDefinitionRegistry beanDefinitionRegistry) {
        classList.forEach((key, value) -> {
            Provider<?> provider = value.getProvider();
            Class<?> clazz = key.getTypeLiteral().getRawType();
            Annotation annotation = value.getKey().getAnnotation();
            String name;
            if (annotation instanceof Named) {
                name = ((Named) annotation).value();
            } else {
                name = CharSequenceUtil.lowerFirst(clazz.getSimpleName());
            }
            if (!isBaseType(clazz) && CharSequenceUtil.startWithAny(clazz.getName(), packages.toArray(new String[]{}))) {
                GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
                beanDefinition.setBeanClass(clazz);
                beanDefinition.setInstanceSupplier(provider::get);
                registerClass(name, clazz, beanDefinition, beanDefinitionRegistry);
            }
        });
    }

    public void registerClass(String name, Class<?> obj, GenericBeanDefinition definition, BeanDefinitionRegistry beanDefinitionRegistry) {
        try {
            beanDefinitionRegistry.registerBeanDefinition(name, definition);
            log.debug("注入spring(Bean):{} - {}", name, obj.getName());
        } catch (Exception var4) {
            log.warn("注入spring(Bean):{} - {} - 失败", name, obj.getName());
        }
    }

    public void registerSingleton(String name, Object obj, ConfigurableListableBeanFactory configurableListableBeanFactory) {
        try {
            configurableListableBeanFactory.registerSingleton(name, obj);
            log.debug("注入spring(Singleton):{} - {}", name, obj.getClass().getName());
        } catch (Exception var4) {
            log.warn("注入spring(Singleton):{} - {} - 失败", name, obj.getClass().getName());
        }

    }

    public Map<Key<?>, Binding<?>> getGuiceBinding(Injector injector) {
        Map<Key<?>, Binding<?>> classList = new HashMap<>();
        List<Injector> list = new ArrayList<>();
        while (true) {
            list.add(injector);
            if (injector.getParent() == null) {
                break;
            }
            injector = injector.getParent();
        }
        Collections.reverse(list);
        for (Injector injector1 : list) {
            classList.putAll(injector1.getAllBindings());
        }
        return classList;
    }

    public static boolean isBaseType(Class<?> className) {
        return className.equals(String.class) || className.equals(Integer.class) || className.equals(Byte.class) || className.equals(Long.class) || className.equals(Double.class) || className.equals(Float.class) || className.equals(Character.class) || className.equals(Short.class) || className.equals(Boolean.class);
    }
}