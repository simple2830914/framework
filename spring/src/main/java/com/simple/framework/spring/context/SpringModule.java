package com.simple.framework.spring.context;

import cn.hutool.extra.spring.SpringUtil;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

@StarterModule(value = "Spring模块", order = Constants.FRAMEWORK_MODULE_ORDER_SPRING)
public class SpringModule extends AbstractModule {


    @Inject
    @Named("spring.package")
    private List<String> packages;

    @Provides
    @Singleton
    public AnnotationConfigApplicationContext applicationContext() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan(packages.toArray(new String[]{}));
        context.register(SpringUtil.class);
        return context;
    }
}