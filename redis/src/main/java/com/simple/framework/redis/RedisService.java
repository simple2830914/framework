package com.simple.framework.redis;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simple.framework.beans.RedisObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.TimeUnit;


@SuppressWarnings("unchecked")
@Component
public class RedisService {

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    ObjectMapper objectMapper;
    //分布式锁过期时间 s  可以根据业务自己调节
    private static final Long LOCK_REDIS_TIMEOUT = 10L;

    /**
     * 加锁
     **/
    public Boolean getLock(String key, Object value) {
        return this.redisTemplate.opsForValue().setIfAbsent(key, value, Duration.ofSeconds(LOCK_REDIS_TIMEOUT));
    }

    public Boolean getLock(String key, String value, Duration duration) {
        return this.redisTemplate.opsForValue().setIfAbsent(key, value, duration);
    }

    /**
     * 释放锁
     **/
    public Long releaseLock(String key, Object value) {
        String luaScript = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
        RedisScript<Long> redisScript = new DefaultRedisScript<>(luaScript, Long.class);
        return this.redisTemplate.execute(redisScript, Collections.singletonList(key), value);
    }


    public boolean expire(final String key, final long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }


    public boolean expire(final String key, final long timeout, final TimeUnit unit) {
        return Boolean.TRUE.equals(redisTemplate.expire(key, timeout, unit));
    }

    public boolean expireAt(final String key, Date date) {
        return Boolean.TRUE.equals(redisTemplate.expireAt(key, date));
    }

    public Long getExpire(final String key, TimeUnit timeUnit) {
        return redisTemplate.getExpire(key, timeUnit);
    }

    public Long getExpire(final String key) {
        return redisTemplate.getExpire(key, TimeUnit.MILLISECONDS);
    }

    public boolean delete(final String key) {
        return Boolean.TRUE.equals(redisTemplate.delete(key));
    }


    public void set(final String key, final Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public void set(final String key, final Object value, final long timeout, final TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, timeout, unit);
    }

    public void setJson(final String key, final Object value) {
        redisTemplate.opsForValue().set(key, new RedisObject(value, true));
    }


    public <T> T get(final String key) {
        return (T) redisTemplate.opsForValue().get(key);
    }

    public <T> T getJson(final String key) {
        Object object = redisTemplate.opsForValue().get(key);
        if (object instanceof RedisObject) {
            object = ((RedisObject) object).getObject();
        }
        try {
            if (object instanceof String) {
                object = objectMapper.readValue((String) object, JsonNode.class);
            } else {
                object = objectMapper.convertValue(object, JsonNode.class);
            }
        } catch (Exception e) {
            return (T) object;
        }
        return (T) object;
    }

}