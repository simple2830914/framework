package com.simple.framework.redis;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import lombok.Data;

import java.util.List;

@Data
public class RedisConfigProperties {

    @Inject
    @Named("redis.deployType")
    private String deployType;

    @Inject
    @Named("redis.host")
    private String host;

    @Inject
    @Named("redis.port")
    private int port;

    @Inject
    @Named("redis.database")
    private int database;

    @Inject
    @Named("redis.username")
    private String username;

    @Inject
    @Named("redis.password")
    private String password;

    @Inject
    @Named("redis.sentinel.master")
    private String master;

    @Inject
    @Named("redis.sentinel.nodes")
    private List<String> nodes;


    @Inject
    @Named("redis.pool.maxIdle")
    private int maxIdle;

    @Inject
    @Named("redis.pool.minIdle")
    private int minIdle;

    @Inject
    @Named("redis.pool.maxWait")
    private int maxWait;

}
