package com.simple.framework.redis;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.simple.framework.beans.RedisObject;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;


public class RedisObjectRedisSerializer<T> implements RedisSerializer<T> {

    public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    private final JavaType javaType;

    private final Converter<Object, byte[]> serializer = new SerializingConverter();
    private final Converter<byte[], Object> deserializer = new DeserializingConverter();

    static boolean isEmpty(@Nullable byte[] data) {
        return (data == null || data.length == 0);
    }

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Creates a new {@link RedisObjectRedisSerializer} for the given target {@link Class}.
     *
     * @param type
     */
    public RedisObjectRedisSerializer(Class<T> type) {
        this.javaType = getJavaType(type);
    }

    /**
     * Creates a new {@link RedisObjectRedisSerializer} for the given target {@link JavaType}.
     *
     * @param javaType
     */
    public RedisObjectRedisSerializer(JavaType javaType) {
        this.javaType = javaType;
    }

    @SuppressWarnings("unchecked")
    public T deserialize(@Nullable byte[] bytes) throws SerializationException {
        if (isEmpty(bytes)) {
            return null;
        }
        try {
            try {
                return (T) this.deserializer.convert(bytes);
            } catch (Exception e) {
                try {
                    RedisObject redisObject = this.objectMapper.readValue(bytes, 0, bytes.length, RedisObject.class);
                    Objects.requireNonNull(redisObject);
                    Objects.requireNonNull(redisObject.getObject());
                    return (T) redisObject;
                } catch (Exception e1) {
                    return this.objectMapper.readValue(bytes, 0, bytes.length, javaType);
                }
            }
        } catch (Exception ex) {
            throw new SerializationException("Could not read JSON: " + ex.getMessage(), ex);
        }
    }

    @Override
    public byte[] serialize(@Nullable Object t) throws SerializationException {
        if (t == null) {
            return new byte[0];
        }
        try {
            byte[] bytes;
            if (t instanceof RedisObject) {
                RedisObject object = (RedisObject) t;
                if (object.getObject() == null) {
                    return new byte[0];
                }
                if (object.isNeedJson()) {
                    bytes = this.objectMapper.writeValueAsBytes(t);
                } else {
                    bytes = serializer.convert(object.getObject());
                }
            } else {
                bytes = serializer.convert(t);
            }
            return bytes;
        } catch (Exception ex) {
            throw new SerializationException("Could not write JSON: " + ex.getMessage(), ex);
        }
    }

    /**
     * Sets the {@code ObjectMapper} for this view. If not set, a default {@link ObjectMapper#ObjectMapper() ObjectMapper}
     * is used.
     * <p>
     * Setting a custom-configured {@code ObjectMapper} is one way to take further control of the JSON serialization
     * process. For example, an extended {@link SerializerFactory} can be configured that provides custom serializers for
     * specific types. The other option for refining the serialization process is to use Jackson's provided annotations on
     * the types to be serialized, in which case a custom-configured ObjectMapper is unnecessary.
     */
    public void setObjectMapper(ObjectMapper objectMapper) {

        Assert.notNull(objectMapper, "'objectMapper' must not be null");
        this.objectMapper = objectMapper;
    }

    /**
     * Returns the Jackson {@link JavaType} for the specific class.
     * <p>
     * Default implementation returns {@link TypeFactory#constructType(java.lang.reflect.Type)}, but this can be
     * overridden in subclasses, to allow for custom generic collection handling. For instance:
     *
     * <pre class="code">
     * protected JavaType getJavaType(Class&lt;?&gt; clazz) {
     * 	if (List.class.isAssignableFrom(clazz)) {
     * 		return TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, MyBean.class);
     *    } else {
     * 		return super.getJavaType(clazz);
     *    }
     * }
     * </pre>
     *
     * @param clazz the class to return the java type for
     * @return the java type
     */
    protected JavaType getJavaType(Class<?> clazz) {
        return TypeFactory.defaultInstance().constructType(clazz);
    }
}
