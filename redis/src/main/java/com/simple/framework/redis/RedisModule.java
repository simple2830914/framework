package com.simple.framework.redis;

import com.google.inject.AbstractModule;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;

@StarterModule(value = "Redis模块", order = Constants.FRAMEWORK_MODULE_ORDER_REDIS)
public class RedisModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(RedisConfigProperties.class).toInstance(new RedisConfigProperties());
    }
}
