package com.simple.framework.redis;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.HashSet;


@Configuration
@Slf4j
public class RedisConfig {

    @Autowired
    RedisConfigProperties redisConfigProperties;

    @Autowired
    ObjectMapper objectMapper;


    @Bean
    public GenericObjectPoolConfig<Object> poolConfig() {
        GenericObjectPoolConfig<Object> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxTotal(redisConfigProperties.getMaxIdle());
        poolConfig.setMinIdle(redisConfigProperties.getMinIdle());
        poolConfig.setMaxWait(Duration.ofMillis(redisConfigProperties.getMaxWait()));
        return poolConfig;
    }

    @Bean
    public RedisSentinelConfiguration redisSentinelConfiguration() {
        return new RedisSentinelConfiguration(redisConfigProperties.getMaster(), new HashSet<>(redisConfigProperties.getNodes()));
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory lettuceConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(lettuceConnectionFactory);

        RedisObjectRedisSerializer<Object> valueSerializer = new RedisObjectRedisSerializer<>(Object.class);
        valueSerializer.setObjectMapper(objectMapper);

        RedisSerializer<?> stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setValueSerializer(valueSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(valueSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    @Bean
    public LettuceConnectionFactory getDefaultLettucePool(RedisSentinelConfiguration redisSentinelConfiguration) {
        LettuceConnectionFactory factory;
        if ("single".equalsIgnoreCase(redisConfigProperties.getDeployType())) {
            RedisStandaloneConfiguration standaloneConfiguration = new RedisStandaloneConfiguration();
            standaloneConfiguration.setHostName(redisConfigProperties.getHost());
            standaloneConfiguration.setPort(redisConfigProperties.getPort());
            standaloneConfiguration.setDatabase(redisConfigProperties.getDatabase());
            standaloneConfiguration.setPassword(redisConfigProperties.getPassword());
            standaloneConfiguration.setUsername(redisConfigProperties.getUsername());
            factory = new LettuceConnectionFactory(standaloneConfiguration);
        } else if ("sentinel".equalsIgnoreCase(redisConfigProperties.getDeployType())) {
            factory = new LettuceConnectionFactory(redisSentinelConfiguration);
        } else {
            throw new RuntimeException("redis deploy type config error !!!, please check redis.properties.", new Throwable());
        }
        return factory;
    }

}