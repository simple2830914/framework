package com.simple.framework.db;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.typesafe.config.ConfigObject;
import lombok.Data;

import java.util.List;

@Data
public class DbConfigProperties {
    @Inject
    @Named("db.master")
    private ConfigObject masterDb;
    @Inject
    @Named("db.druid")
    private ConfigObject druidProp;
    @Inject
    @Named("db.package")
    private List<String> packages;
    @Inject
    @Named("db.druid.proxyFilters")
    private List<String> proxyFilters;
    @Inject
    @Named("db.slaves")
    private List<ConfigObject> slaves;
    @Inject
    @Named("db.others")
    private List<ConfigObject> others;
    @Inject
    @Named("db.balancing")
    private boolean balancing;
}
