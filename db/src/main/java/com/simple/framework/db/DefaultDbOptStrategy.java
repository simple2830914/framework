package com.simple.framework.db;

import cn.hutool.core.text.CharSequenceUtil;
import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.datasource.DataSourceShardingStrategy;
import com.mybatisflex.core.datasource.FlexDataSource;
import com.simple.framework.beans.Constants;

import java.lang.reflect.Method;
import java.util.Set;

public class DefaultDbOptStrategy implements DataSourceShardingStrategy {

    @Override
    public String doSharding(String currentDataSourceKey, Object mapper, Method mapperMethod, Object[] methodArgs) {
        if (!CharSequenceUtil.startWithAny(currentDataSourceKey, Constants.FRAMEWORK_DB_NAME_MASTER, Constants.FRAMEWORK_DB_NAME_SLAVES)) {
            return currentDataSourceKey;
        }
        FlexDataSource dataSource = FlexGlobalConfig.getDefaultConfig().getDataSource();
        if (CharSequenceUtil.startWithAny(mapperMethod.getName(), "insert", "delete", "update")) {
            return Constants.FRAMEWORK_DB_NAME_MASTER;
        }
        Set<String> keys = dataSource.getDataSourceMap().keySet();
        boolean haveSlave = keys.stream().anyMatch(s -> s.startsWith(Constants.FRAMEWORK_DB_NAME_SLAVES));
        return haveSlave ? Constants.FRAMEWORK_DB_NAME_SLAVES + "*" : Constants.FRAMEWORK_DB_NAME_MASTER;
    }

}
