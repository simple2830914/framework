package com.simple.framework.db;

import com.alibaba.druid.pool.DruidDataSource;
import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import com.mybatisflex.core.datasource.DataSourceManager;
import com.mybatisflex.core.datasource.FlexDataSource;
import com.simple.framework.beans.Constants;
import com.typesafe.config.ConfigObject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.List;

@Configuration
@Slf4j
public class MyBatisFlexConfig implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    DbConfigProperties dbConfigProperties;


    public MyBatisFlexConfig() {
        AuditManager.setAuditEnable(false);
        AuditManager.setMessageCollector(auditMessage -> log.info("{},{}ms", auditMessage.getFullSql(), auditMessage.getElapsedTime()));
    }

    @Override
    public void onApplicationEvent(@NonNull ContextRefreshedEvent event) {
        List<ConfigObject> slaves = dbConfigProperties.getSlaves();
        List<ConfigObject> others = dbConfigProperties.getOthers();
        ConfigObject druidProp = dbConfigProperties.getDruidProp();
        List<String> proxyFilters = dbConfigProperties.getProxyFilters();
        FlexDataSource dataSource = FlexGlobalConfig.getDefaultConfig().getDataSource();
        int i = 0;
        for (ConfigObject k : slaves) {
            String order = i == 0 ? "" : String.valueOf(i);
            DruidDataSource slaveDataSource = DbConfig.dataSource(Constants.FRAMEWORK_DB_NAME_SLAVES + order, k.toConfig(), druidProp, proxyFilters);
            dataSource.addDataSource(slaveDataSource.getName(), slaveDataSource);

            i++;
        }
        i = 0;
        for (ConfigObject k : others) {
            String order = i == 0 ? "" : String.valueOf(i);
            DruidDataSource otherDataSource = DbConfig.dataSource(Constants.FRAMEWORK_DB_NAME_OTHER + order, k.toConfig(), druidProp, proxyFilters);
            dataSource.addDataSource(otherDataSource.getName(), otherDataSource);
            i++;
        }
        if (dbConfigProperties.isBalancing()) {
            DataSourceManager.setDataSourceShardingStrategy(new DefaultDbOptStrategy());
        }
    }

}