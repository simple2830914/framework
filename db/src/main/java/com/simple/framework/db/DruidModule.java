package com.simple.framework.db;

import com.alibaba.druid.support.jakarta.StatViewServlet;
import com.alibaba.druid.support.jakarta.WebStatFilter;
import com.google.inject.Inject;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.SimpleModule;
import jakarta.servlet.DispatcherType;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;

import java.util.EnumSet;

@StarterModule(value = "DB模块-注册Druid监控Filter", order = Constants.FRAMEWORK_MODULE_ORDER_DB)
public class DruidModule extends SimpleModule {

    @Inject
    WebAppContext webAppContext;

    @Override
    protected void configure() {
        ServletHolder servlet = new ServletHolder(StatViewServlet.class);
        servlet.setInitParameter("loginUsername", "admin");
        servlet.setInitParameter("loginPassword", "admin");
        FilterHolder filter = new FilterHolder(WebStatFilter.class);
        filter.setInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        webAppContext.addServlet(servlet, "/druid/*");
        webAppContext.addFilter(filter, "/*", EnumSet.allOf(DispatcherType.class));
    }
}
