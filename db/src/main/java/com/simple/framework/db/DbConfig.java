package com.simple.framework.db;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.setting.dialect.Props;
import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;
import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.datasource.FlexDataSource;
import com.mybatisflex.core.mybatis.FlexConfiguration;
import com.mybatisflex.spring.FlexSqlSessionFactoryBean;
import com.mybatisflex.spring.FlexTransactionManager;
import com.mybatisflex.spring.datasource.DataSourceAdvice;
import com.simple.framework.beans.Constants;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.logging.slf4j.Slf4jImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Configuration
@Slf4j
@EnableTransactionManagement
public class DbConfig {

    @Autowired
    DbConfigProperties dbConfigProperties;


    @Bean(name = "transactionManager")
    public @NonNull PlatformTransactionManager annotationDrivenTransactionManager() {
        return new FlexTransactionManager();
    }

    @Bean
    public DataSourceAdvice dataSourceAdvice() {
        return new DataSourceAdvice();
    }


    @Bean
    public DataSource dataSource() {
        return new FlexDataSource(Constants.FRAMEWORK_DB_NAME_MASTER, dataSource(Constants.FRAMEWORK_DB_NAME_MASTER, dbConfigProperties.getMasterDb()
                .toConfig(), dbConfigProperties.getDruidProp(), dbConfigProperties.getProxyFilters()));
    }

    @Bean(name = "sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
        FlexGlobalConfig.getDefaultConfig().setPrintBanner(false);
        SqlSessionFactoryBean factoryBean = new FlexSqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        FlexConfiguration configuration = new FlexConfiguration();
        configuration.setLogImpl(Slf4jImpl.class);
        factoryBean.setConfiguration(configuration);
        return factoryBean.getObject();
    }
//
//    @Bean
//    public MapperScannerConfigurer mapperScannerConfigurer() {
//        MapperScannerConfigurer configurer = new MapperScannerConfigurer();
//        configurer.setBasePackage(CharSequenceUtil.join(",", dbConfigProperties.getPackages()));
//        configurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
//        return configurer;
//    }

    private static Properties getDruidProperty(Config config, ConfigObject druid, String name) {
        Props properties = new Props();
        String url = config.getString("url");

        String username = config.getString("username");
        String password = config.getString("password");
        properties.setProperty("druid.name", name);
        properties.setProperty("druid.url", url);
        properties.setProperty("druid.username", username);
        properties.setProperty("druid.password", password);
        druid.unwrapped().forEach((k, v) -> properties.setProperty(k, String.valueOf(v)));
        return properties;
    }

    public static DruidDataSource dataSource(String name, Config config, ConfigObject druid, List<String> proxyFilters) {
        DruidDataSource dataSource = new DruidDataSource();
        if (CollUtil.isNotEmpty(proxyFilters)) {
            List<Filter> filters = new ArrayList<>();
            for (String s : proxyFilters) {
                try {
                    filters.add((Filter) ClassUtil.loadClass(s).getDeclaredConstructor().newInstance());
                } catch (Exception e) {
                    log.warn("load class error:{} ", s);
                }
            }
            dataSource.setProxyFilters(filters);
        }
        dataSource.configFromPropeties(getDruidProperty(config, druid, name));
        return dataSource;
    }


}
