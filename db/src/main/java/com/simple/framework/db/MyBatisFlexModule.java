package com.simple.framework.db;

import cn.hutool.core.text.CharSequenceUtil;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.SimpleModule;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.mapper.MapperScannerConfigurer;

import java.util.List;

@StarterModule(value = "DB模块-注册MyBatisFlex的SqlSessionFactory,支持读写分离", order = Constants.FRAMEWORK_MODULE_ORDER_DB)
@Slf4j
public class MyBatisFlexModule extends SimpleModule {
    @Inject
    @Named("db.package")
    private List<String> packages;

    @Override
    protected void configure() {
        bind(DbConfigProperties.class).toInstance(new DbConfigProperties());
    }

    @Provides
    @Singleton
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer configurer = new MapperScannerConfigurer();
        configurer.setBasePackage(CharSequenceUtil.join(",", packages));
        configurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        return configurer;
    }
}
