package com.simple.framework.aop;

import com.typesafe.config.Config;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Data
@Component
public class ExecuteTimeAspect {

    @Autowired
    private Config config;
    private long startTime;

    @Pointcut("@annotation(com.simple.framework.annotation.ExecuteTime)")
    private void timeAspect() {

    }

    @Before("timeAspect()")
    public void before(JoinPoint joinPoint) {
        startTime = System.currentTimeMillis();
    }

    @After("timeAspect()")
    public void after(JoinPoint joinPoint) {
        long diff = System.currentTimeMillis() - startTime;
        long slowTime = config.getLong("aop.executeTimeAspect.slowTime");
        if (diff > slowTime) {
            log.info("{} - {}方法执行耗时：{}", joinPoint.getSignature()
                    .getDeclaringType()
                    .getName(), joinPoint.getSignature().getName(), diff);
        }
    }
}