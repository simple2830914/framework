package com.simple.framework.handler;

import com.simple.framework.beans.ResponseBean;
import com.simple.framework.beans.SimpleException;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionUtils {

    public static Response toResponse(Exception e) {
        ResponseBean errorResponse;
        if (e instanceof SimpleException) {
            SimpleException simpleException = (SimpleException) e;
            errorResponse = ResponseBean.error(simpleException.getCode(), simpleException.getMessage());
        } else {
            errorResponse = ResponseBean.error(e.getMessage());
        }
//        log.info("jetty捕获到异常", e);
        return Response.ok().entity(errorResponse).build();
    }

}
