package com.simple.framework.handler;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.text.CharSequenceUtil;
import com.simple.framework.annotation.NamedHandler;
import com.simple.framework.beans.Constants;
import com.simple.framework.beans.ResponseBean;
import jakarta.annotation.Priority;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.lang.annotation.Annotation;


@Provider
@Slf4j
@PreMatching
@Priority(999)
@NamedHandler("全局请求拦截:添加RequestId,未捕获的异常,强制ResponseBean格式返回")
public class RequestIdFilter extends Handler {

    @Override
    public void requestHandler(ContainerRequestContext requestContext) {
        String requestId = requestContext.getHeaderString(Constants.FRAMEWORK_REQUEST_ID);
        if (CharSequenceUtil.isEmpty(requestId)) {
            requestId = UUID.randomUUID().toString();
            requestContext.getHeaders().addFirst(Constants.FRAMEWORK_REQUEST_ID, requestId);
        }
        MDC.put("traceId", requestId);
        if (isOpenLog(requestContext)) {
            log.info("HTTP REQUEST - Path({}):{} ", requestId, requestContext.getUriInfo().getPath());
            log.info("Header({}):{} ", requestId, requestContext.getHeaders());
            log.info("Entity({}):{} ", requestId, getEntityBody(requestContext));
        }
    }

    @SneakyThrows
    @Override
    public void responseHandler(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        String requestId = requestContext.getHeaderString(Constants.FRAMEWORK_REQUEST_ID);
        responseContext.getHeaders().addFirst(Constants.FRAMEWORK_REQUEST_ID, requestId);
        if (isOpenLog(requestContext)) {
            log.info("HTTP RESPONSE - Path({}):{} ", requestId, requestContext.getUriInfo().getPath());
            log.info("Header({}):{} ", requestId, responseContext.getHeaders());
            log.info("Entity({}):{} ", requestId, responseContext.getEntity());
        }
        if (responseContext.getStatus() != Response.Status.OK.getStatusCode()) {
            responseContext.setStatus(Response.Status.OK.getStatusCode());
            responseContext.setEntity(ResponseBean.error(ResponseBean.UNKNOWN_ERROR_MSG), new Annotation[]{}, MediaType.APPLICATION_JSON_TYPE);
        }

        if (responseContext.getEntity() instanceof ResponseBean) {
            ResponseBean responseBean = (ResponseBean) responseContext.getEntity();
            responseBean.setRequestId(requestId);
            if (responseBean.isCustomResponse()) {
                responseContext.setEntity(responseBean.isSingle() ? responseBean.getData()
                        .get(0) : responseBean.getData(), new Annotation[]{}, MediaType.valueOf(responseBean.getMediaType()));
            }
        }
        MDC.remove("traceId");
    }
}
