package com.simple.framework.handler;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Provider
@Slf4j
public class ExceptionHandler implements ExceptionMapper<Exception> {

    @SneakyThrows
    @Override
    public Response toResponse(Exception e) {
        return ExceptionUtils.toResponse(e);
    }

}