package com.simple.framework.handler;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.servlet.GuiceFilter;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.simple.framework.annotation.NamedHandler;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.Reflector;
import com.simple.framework.boot.SimpleModule;
import jakarta.servlet.DispatcherType;
import jakarta.servlet.Filter;
import jakarta.servlet.annotation.WebFilter;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.webapp.WebAppContext;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;


@Slf4j
@StarterModule(value = "支持webFilter的handler模块", order = Constants.FRAMEWORK_MODULE_ORDER_HANDLER)
public class HandlerModule extends SimpleModule {

    @Inject
    private Reflector reflections;

    @Inject
    WebAppContext webAppContext;

    @Override
    protected void configure() {
        addGuiceServlet();
        install(new ServletModule() {
            @SuppressWarnings("unchecked")
            @Override
            protected void configureServlets() {
                StringBuilder sb = new StringBuilder();
                List<Class<?>> filters = reflections.getClassesWithAnnotation(WebFilter.class);
                for (Class<?> clazz : filters) {
                    List<String> urlPatterns = Arrays.asList(clazz.getAnnotation(WebFilter.class).urlPatterns());
                    sb.append(clazz.getName())
                            .append(" - ")
                            .append(clazz.getAnnotation(WebFilter.class).description())
                            .append(" ")
                            .append(urlPatterns)
                            .append("\n");
                    filter(urlPatterns).through((Class<? extends Filter>) clazz);
                }
                filters = reflections.getClassesWithAnnotation(NamedHandler.class);
                for (Class<?> clazz : filters) {
                    sb.append(clazz.getName())
                            .append(" - ")
                            .append(clazz.getAnnotation(NamedHandler.class).value())
                            .append(" ")
                            .append("[*]")
                            .append("\n");
                }
                log.info("Handler:\n{}", sb);
            }
        });
    }

    public void addGuiceServlet() {
        Provider<Injector> provider = binder().getProvider(Injector.class);
        webAppContext.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
        webAppContext.addEventListener(new GuiceServletContextListener() {
            protected Injector getInjector() {
                return provider.get();
            }
        });
    }
}
