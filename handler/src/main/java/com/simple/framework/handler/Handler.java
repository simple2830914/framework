package com.simple.framework.handler;

import cn.hutool.json.JSONUtil;
import com.simple.framework.beans.Constants;
import com.simple.framework.beans.ResponseBean;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.MediaType;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.message.internal.ReaderWriter;

import java.io.*;
import java.nio.charset.StandardCharsets;


@Slf4j
public abstract class Handler implements Filter, ContainerRequestFilter, ContainerResponseFilter {

    @SneakyThrows
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        if (isOpenLog(httpServletRequest)) {
            log.info("{} - access filter {}", httpServletRequest.getRequestURL(), this.getClass()
                    .getAnnotation(WebFilter.class)
                    .filterName());
        }
        handle(httpServletRequest, httpServletResponse);
        chain.doFilter(request, response);
    }

    public void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {

    }

    public void returnHttpError(HttpServletResponse response, ResponseBean data) {
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType(MediaType.APPLICATION_JSON);
        PrintWriter out;
        try {
            out = response.getWriter();
            out.append(JSONUtil.toJsonStr(data));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        requestHandler(containerRequestContext);
    }

    public void requestHandler(ContainerRequestContext containerRequestContext) {

    }

    public void responseHandler(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) {

    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        responseHandler(containerRequestContext, containerResponseContext);
    }

    String getEntityBody(ContainerRequestContext requestContext) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = requestContext.getEntityStream();
        final StringBuilder b = new StringBuilder();
        try {
            ReaderWriter.writeTo(in, out);
            byte[] requestEntity = out.toByteArray();
            if (requestEntity.length != 0) {
                b.append(new String(requestEntity));
            }
            requestContext.setEntityStream(new ByteArrayInputStream(requestEntity));
        } catch (IOException ex) {
            log.error("读取请求体异常", ex);
        }
        return b.toString();
    }

    public boolean isOpenLog(HttpServletRequest httpServletRequest) {
        return Constants.FRAMEWORK_NAME.equals(httpServletRequest.getHeader(Constants.FRAMEWORK_OPEN_FILTER_LOG_FLAG));
    }

    public boolean isOpenLog(ContainerRequestContext containerRequestContext) {
        return Constants.FRAMEWORK_NAME.equals(containerRequestContext.getHeaderString(Constants.FRAMEWORK_OPEN_FILTER_LOG_FLAG));
    }


}

