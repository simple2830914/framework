package com.simple.framework.beans;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SimpleException extends RuntimeException {

    private final String code;

    public SimpleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String code) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
    }

    public SimpleException of(String code, Throwable e) {
        return new SimpleException(e.getMessage(), e, true, true, code);
    }
}
