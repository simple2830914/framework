package com.simple.framework.beans;

@FunctionalInterface
public interface Function2<A, B, C> {
    public C apply(A a, B b);
}