package com.simple.framework.beans;

@FunctionalInterface
public interface Function4<A, B, C, E, F> {
    F invoke(A a, B b, C c, E e);
}