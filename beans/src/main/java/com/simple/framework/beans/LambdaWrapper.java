package com.simple.framework.beans;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author Wbody
 */
public class LambdaWrapper {

    @FunctionalInterface
    public interface ThrowConsumerFunction<T> {

        /**
         * 通用函数
         */
        void accept(T t) throws Exception;

    }

    @FunctionalInterface
    public interface TwoFunction<M, K, R> {

        /**
         * 通用函数
         */
        R apply(M t, K k);

    }

    @FunctionalInterface
    public interface ThrowFunction<T, R> {

        /**
         * 通用函数
         */
        R apply(T t) throws Exception;

    }

    public static <T> void accept(ThrowConsumerFunction<T> function) {
        Consumer<T> a = i -> {
            try {
                function.accept(null);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        };
        a.accept(null);
    }

    public static <T, R> R apply(ThrowFunction<T, R> function) {
        Function<T, R> a = i -> {
            try {
                return function.apply(null);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        };
        return a.apply(null);
    }

}
