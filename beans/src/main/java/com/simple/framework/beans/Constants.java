package com.simple.framework.beans;

public class Constants {
    public static final String FRAMEWORK_NAME = "simple";
    public static final String FRAMEWORK_OPEN_FILTER_LOG_FLAG = "X-Framework";
    public static final String FRAMEWORK_REQUEST_ID = "X-RequestId";


    public static final int FRAMEWORK_STARTER_ORDER_GEN = 2;
    public static final int FRAMEWORK_STARTER_ORDER_JETTY = 1;

    public static final int FRAMEWORK_MODULE_ORDER_JERSEY = 998;
    public static final int FRAMEWORK_MODULE_ORDER_HANDLER = 997;
    public static final int FRAMEWORK_MODULE_ORDER_JOB = 7;
    public static final int FRAMEWORK_MODULE_ORDER_GEN = 6;
    public static final int FRAMEWORK_MODULE_ORDER_DB = 5;
    public static final int FRAMEWORK_MODULE_ORDER_TEMPLATE = 4;
    public static final int FRAMEWORK_MODULE_ORDER_REDIS = 3;
    public static final int FRAMEWORK_MODULE_ORDER_SPRING = 2;
    public static final int FRAMEWORK_MODULE_ORDER_JETTY = 1;
    public static final int FRAMEWORK_MODULE_ORDER_CONFIG = -1;

    public static final String FRAMEWORK_DB_NAME_MASTER = "master";
    public static final String FRAMEWORK_DB_NAME_SLAVES = "slave";
    public static final String FRAMEWORK_DB_NAME_OTHER = "other";


}
