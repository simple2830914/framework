package com.simple.framework.beans;

import cn.hutool.core.codec.Base64;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Supplier;


@Slf4j
public class MdcUtil {

    /**
     * 链路追踪KEY值
     */
    public static final String TRACE_ID = "traceId";

    /**
     * 服务器ID
     */
    private static String SERVER_ID;

    /**
     * 获取服务器ID标识
     *
     * @return
     */
    public static String getServerId() {
        if (Objects.isNull(SERVER_ID)) {
            try {
                String hostAddress = InetAddress.getLocalHost().getHostAddress();
                hostAddress = hostAddress.replace(".", "");
                SERVER_ID = Base64.encode(hostAddress);
            } catch (UnknownHostException e) {
                log.error("\n", e);
            }
        }
        return SERVER_ID;
    }


    /**
     * 设置全局唯一ID
     *
     * @param value ID值
     */
    public static void set(String value) {
        MDC.put(TRACE_ID, Objects.nonNull(value) ? value : UUID.randomUUID().toString().replace("-", ""));
    }

    public static void set() {
        set(null);
    }

    /**
     * 获取ID值
     *
     * @return
     */
    public static String get() {
        return MDC.get(TRACE_ID);
    }


    /**
     * 获取MDC上下文对象
     *
     * @return
     */
    public static Map<String, String> getMap() {
        return MDC.getCopyOfContextMap();
    }

    /**
     * 设置MDC上下文对象
     *
     * @param map 上下文对象
     */
    public static void setMap(Map<String, String> map) {
        if (Objects.nonNull(map)) {
            MDC.setContextMap(map);
        }
    }

    /**
     * 清空MDC
     */
    public static void clear() {
        MDC.clear();
    }

    public static Runnable withMdc(Runnable runnable) {
        Map<String, String> mdc = MDC.getCopyOfContextMap();
        return () -> {
            MDC.setContextMap(mdc);
            runnable.run();
        };
    }

    public static <U> Supplier<U> withMdc(Supplier<U> supplier) {
        Map<String, String> mdc = MDC.getCopyOfContextMap();
        return () -> {
            MDC.setContextMap(mdc);
            return supplier.get();
        };
    }
}