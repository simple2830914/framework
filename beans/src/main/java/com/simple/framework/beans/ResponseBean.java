package com.simple.framework.beans;

import cn.hutool.core.collection.CollUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseBean implements Serializable {

    public static final String SUCCESS_CODE = "200";
    public static final String ERROR_CODE = "500";
    public static final String UNKNOWN_ERROR_CODE = "503";
    public static final String UNKNOWN_ERROR_MSG = "UNKNOWN_ERROR";
    public static final String SUCCESS_MSG = "ok";

    private String statusMsg;
    private String statusCode;
    private boolean success;
    private boolean single;
    private boolean forwarding;
    private String address;
    private boolean customResponse;
    private String mediaType;
    private String requestId;

    @Builder.Default
    private List<Object> data = CollUtil.newArrayList();

    public static ResponseBean ok() {
        return ResponseBean.builder()
                .statusMsg(SUCCESS_MSG)
                .statusCode(SUCCESS_CODE)
                .success(true)
                .single(true)
                .build();
    }

    public static ResponseBean error(String msg) {
        return ResponseBean.builder().statusMsg(msg).statusCode(ERROR_CODE).success(false).single(true).build();
    }

    public static ResponseBean error(String code, String msg) {
        return ResponseBean.builder().statusMsg(msg).statusCode(code).success(false).single(true).build();
    }

    public static ResponseBean errorOf(Object data) {
        return ResponseBean.builder()
                .statusMsg(UNKNOWN_ERROR_MSG)
                .statusCode(UNKNOWN_ERROR_CODE)
                .success(false)
                .single(true)
                .data(data == null ? CollUtil.newArrayList() : CollUtil.newArrayList(data))
                .build();
    }

    public static ResponseBean forwarding(String forwarding) {
        return ResponseBean.builder()
                .statusMsg(SUCCESS_MSG)
                .statusCode(SUCCESS_CODE)
                .success(true)
                .single(true)
                .forwarding(true)
                .address(forwarding)
                .data(CollUtil.newArrayList())
                .build();
    }

    public static ResponseBean of(Object data) {
        return ResponseBean.builder()
                .statusMsg(SUCCESS_MSG)
                .statusCode(SUCCESS_CODE)
                .success(true)
                .single(true)
                .data(data == null ? CollUtil.newArrayList() : CollUtil.newArrayList(data))
                .build();
    }

    public static ResponseBean of(Collection<?> data) {
        return ResponseBean.builder()
                .statusMsg(SUCCESS_MSG)
                .statusCode(SUCCESS_CODE)
                .success(true)
                .single(false)
                .data(data == null ? CollUtil.newArrayList() : new ArrayList<>(data))
                .build();
    }

    public static ResponseBean ok(String statusMsg) {
        return ResponseBean.builder().statusMsg(statusMsg).statusCode(SUCCESS_CODE).success(true).single(true).build();
    }
}
