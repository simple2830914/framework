package com.simple.framework.beans;

import cn.hutool.core.collection.CollUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;

public class Transmit {
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class SyncResult {
        boolean success;
        String msg;
    }

    static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(Runtime.getRuntime()
            .availableProcessors());


    public static <T> void supplyAsync(List<T> node, Consumer<T> function) {
        if (CollUtil.isNotEmpty(node)) {
            CompletableFuture<Void> all = CompletableFuture.allOf(node.stream()
                    .map(m -> CompletableFuture.supplyAsync(() -> {
                        function.accept(m);
                        return m;
                    }, scheduler).orTimeout(60, java.util.concurrent.TimeUnit.SECONDS))
                    .toArray(CompletableFuture[]::new));
            all.join();
        }
    }

    public static <T> void supplyAsyncWithMdc(List<T> node, Consumer<T> function) {
        if (CollUtil.isNotEmpty(node)) {
            CompletableFuture<Void> all = CompletableFuture.allOf(node.stream()
                    .map(m -> CompletableFuture.supplyAsync(MdcUtil.withMdc(() -> {
                        function.accept(m);
                        return m;
                    }), scheduler).orTimeout(60, java.util.concurrent.TimeUnit.SECONDS))
                    .toArray(CompletableFuture[]::new));
            all.join();
        }
    }

    public static <T, K> List<K> supplyAsync(List<T> node, Function<T, K> function) {
        List<K> result = new CopyOnWriteArrayList<>();
        if (CollUtil.isNotEmpty(node)) {
            CompletableFuture<Void> all = CompletableFuture.allOf(node.stream()
                    .map(m -> CompletableFuture.supplyAsync(() -> function.apply(m), scheduler).thenAccept(result::add).orTimeout(60, java.util.concurrent.TimeUnit.SECONDS))
                    .toArray(CompletableFuture[]::new));
            all.join();
        }
        return result;
    }
}
