package com.simple.framework.jersey;

import com.simple.framework.jersey.spring.SpringBridge;
import com.simple.framework.jersey.spring.SpringIntoHK2Bridge;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Slf4j
public class SpringResourceConfig extends ResourceConfig {

    @Inject
    public SpringResourceConfig(ServiceLocator serviceLocator, ServletContext servletContext) {
        super(config(servletContext));
        log.info("安装SpringIntoHK2Bridge");
        installSpringBridge(serviceLocator, servletContext);
    }

    private static ResourceConfig config(ServletContext servletContext) {
        JerseyConfig config = getConfig(servletContext);
        return new ResourceConfig().packages(config.getPackages().toArray(new String[]{}));
    }

    private static AnnotationConfigApplicationContext getContext(ServletContext servletContext) {
        return (AnnotationConfigApplicationContext) servletContext.getAttribute(AnnotationConfigApplicationContext.class.getName());
    }

    private static JerseyConfig getConfig(ServletContext servletContext) {
        return (JerseyConfig) servletContext.getAttribute(JerseyConfig.class.getName());
    }

    public static void installSpringBridge(ServiceLocator serviceLocator, ServletContext servletContext) {
        AnnotationConfigApplicationContext context = getContext(servletContext);
        SpringBridge.getSpringBridge().initializeSpringBridge(serviceLocator);
        SpringIntoHK2Bridge bridge = serviceLocator.getService(SpringIntoHK2Bridge.class);
        bridge.bridgeSpringBeanFactory(serviceLocator, context);
    }
}