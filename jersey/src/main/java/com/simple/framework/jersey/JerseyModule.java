package com.simple.framework.jersey;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.simple.framework.annotation.StarterModule;
import com.simple.framework.beans.Constants;
import com.simple.framework.boot.SimpleModule;
import com.simple.framework.jersey.spring.SpringListener;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;
import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


@Slf4j
@StarterModule(value = "Jersey模块 - 支持切换GuiceBean和SpringBean", order = Constants.FRAMEWORK_MODULE_ORDER_JERSEY)
public class JerseyModule extends SimpleModule {
    @Inject
    @Named("jersey.resourceClass")
    private String resourceClass;

    @Inject
    WebAppContext webAppContext;

    @Override
    protected void configure() {
        ServletHolder holder = new ServletHolder(ServletContainer.class);
        holder.setInitOrder(1);
        holder.setInitParameter("jakarta.ws.rs.Application", resourceClass);

        webAppContext.addServlet(holder, "/*");
        if (resourceClass.equals(SpringResourceConfig.class.getName())) {
            Provider<AnnotationConfigApplicationContext> provider = binder().getProvider(AnnotationConfigApplicationContext.class);
            Provider<JerseyConfig> jerseyConfigProvider = binder().getProvider(JerseyConfig.class);
            webAppContext.addEventListener(new SpringListener() {
                @Override
                protected AnnotationConfigApplicationContext getContext() {
                    return provider.get();
                }

                @Override
                protected JerseyConfig getConfig() {
                    return jerseyConfigProvider.get();
                }

            });
        }
    }
}
