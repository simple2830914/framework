package com.simple.framework.jersey;

import com.google.inject.Injector;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ResourceConfig;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;

@Slf4j
public class GuiceBridgeResourceConfig extends ResourceConfig {

    @Inject
    public GuiceBridgeResourceConfig(ServiceLocator serviceLocator, ServletContext servletContext) {
        super(config(servletContext));
        log.info("安装GuiceIntoHK2Bridge");
        installGuiceBridge(serviceLocator, servletContext);
    }

    private static ResourceConfig config(ServletContext servletContext) {
        Injector injector = getInjector(servletContext);
        JerseyConfig config = injector.getInstance(JerseyConfig.class);
        return new ResourceConfig().packages(config.getPackages().toArray(new String[]{}));
    }

    private static Injector getInjector(ServletContext servletContext) {
        return (Injector) servletContext.getAttribute(Injector.class.getName());
    }

    public static void installGuiceBridge(ServiceLocator serviceLocator, ServletContext servletContext) {
        Injector injector = getInjector(servletContext);
        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);
        GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(injector);
    }

}