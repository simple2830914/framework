package com.simple.framework.jersey;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import lombok.Data;

import java.util.List;

@Data
public class JerseyConfig {

    @Inject
    @Named("jersey.package")
    private List<String> packages;

}
