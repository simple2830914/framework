package com.simple.framework.jersey;

import jakarta.ws.rs.core.Configuration;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.server.model.Invocable;
import org.glassfish.jersey.server.model.ModelProcessor;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceModel;

import java.lang.reflect.Type;
import java.util.List;

@Provider
@Slf4j
public class JerseyModelProcessor implements ModelProcessor {

    @Override
    public ResourceModel processResourceModel(ResourceModel resourceModel, Configuration configuration) {
        for (Resource resource : resourceModel.getResources()) {
            log.info("Resource: {} - {}", resource.getPath(), resource.getName());
            resource.getChildResources().forEach(s -> {
                String method = s.getResourceMethods().get(0).getHttpMethod();
                String path = s.getParent().getPath() + s.getPath();
                List<MediaType> getProducedTypes = s.getResourceMethods().get(0).getProducedTypes();
                Type responseType = ((Invocable) s.getResourceMethods()
                        .get(0)
                        .getComponents()
                        .get(0)).getResponseType();
                log.info(" Resource: {}: - {} - {} - {}", method, path, getProducedTypes, responseType);

            });
        }
        return resourceModel;
    }

    @Override
    public ResourceModel processSubResource(ResourceModel resourceModel, Configuration configuration) {
        return resourceModel;
    }
}
