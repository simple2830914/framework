package com.simple.framework.jersey.spring;

import com.simple.framework.jersey.JerseyConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

@Slf4j
public abstract class SpringListener implements ServletContextListener {
    static final String CONTEXT_NAME = AnnotationConfigApplicationContext.class.getName();
    static final String CONFIG_NAME = JerseyConfig.class.getName();

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        AnnotationConfigApplicationContext context = getContext();
        context.refresh();
        Arrays.stream(context.getBeanFactory().getBeanDefinitionNames())
                .forEach(s -> log.debug("spring bean:{} -{}", s, context.getBeanDefinition(s).getBeanClassName()));
        servletContext.setAttribute(CONTEXT_NAME, context);
        servletContext.setAttribute(CONFIG_NAME, getConfig());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        servletContext.removeAttribute(CONTEXT_NAME);
        servletContext.removeAttribute(CONFIG_NAME);
    }

    protected abstract AnnotationConfigApplicationContext getContext();

    protected abstract JerseyConfig getConfig();

}

